# lyraSMS

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/meeths/lyrasms/master?logo=sega&style=plastic)

![Lyra](data/images/lyrasms.png)

### Sega Master System emulator under the [Lyra](https://gitlab.com/Meeths/lyra) framework

![Lyra - Sonic the Hedgehog](data/images/sonic_0.gif)


Feature set

- [x]  Complete z80 instruction System
- [x]  Mode 4 VDP emulation
- [ ]  Mode 2 VDP emulation (SG1000)
- [x]  Memory mappers
- [x]  Gamepad emulation (Keyboard and XInput)
- [x]  SN79489 emulation (Through FMOD)
- [x]  Rendering

Known issues or missing features [HERE](https://gitlab.com/Meeths/lyrasms/-/issues)
## Tests

### ZEXALL

Z80 CPU emulation test program converted to the SMS 

![Zexall 1](data/images/zexall_0.png) ![Zexall 2](data/images/zexall_1.png) ![Zexall 3](data/images/zexall_2.png)

[SMSPower.org](https://www.smspower.org/Homebrew/ZEXALL-SMS). 

### PFR Detect

Memory paging checker

![PFR Detect](data/images/PFR_Detect_0.png)

[SMSPower.org](https://www.smspower.org/Homebrew/PFRDetect-SMS)