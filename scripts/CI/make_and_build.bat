
setlocal EnableDelayedExpansion

echo Generating solution file
call generate_lyraSMS.bat
echo Building project

set COMMUNITY_MSBUILD="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe"
set BUILDTOOLS_MSBUILD="C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin\MSBuild.exe"

set BUILD_ARGUMENTS=projects\lyraSMS.sln /t:Build /p:Configuration=Release /p:Platform=Win64

if exist %BUILDTOOLS_MSBUILD% (
    echo Using BuildTools
    set BUILD_COMMAND=%BUILDTOOLS_MSBUILD% %BUILD_ARGUMENTS%
    echo !BUILD_COMMAND!
    call !BUILD_COMMAND!
) else if exist %COMMUNITY_MSBUILD% (
    echo Using Community
    set BUILD_COMMAND=%COMMUNITY_MSBUILD% %BUILD_ARGUMENTS%
    echo !BUILD_COMMAND!
    call !BUILD_COMMAND!
) else (
    echo MSBuild not found. Displaying environment variables.
    set
)

