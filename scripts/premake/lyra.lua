project "lyra"
    kind "StaticLib"
    language "C++"
    targetdir "%{BASE_DIR}bin/%{cfg.buildcfg}"

    files { 
        "%{BASE_DIR}external/lyra/packages/Lyra/**.h", 
        "%{BASE_DIR}external/lyra/packages/Lyra/**.hpp", 
        "%{BASE_DIR}external/lyra/packages/Lyra/**.inl", 
        "%{BASE_DIR}external/lyra/packages/Lyra/**.cpp", 
        "%{BASE_DIR}external/lyra/packages/Lyra/**.c" 
    }

    includedirs {
        "%{BASE_DIR}external/lyra/packages/Lyra/include"
    }

    configureFlags()

    -- External libraries
    includeVulkan()
    includeGLFW()
    includeTracy()
    includeFMOD()
    -- End external libraries

    setConfigurations()

    filter {}