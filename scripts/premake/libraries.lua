function linkXInput()
    links "xinput.lib"
end

function includeVulkan()
    includedirs "%{BASE_DIR}external/lyra/external/vulkan_1.2.141.0/Include"

end

function linkVulkan()
    filter { "platforms:Win32" }
        libdirs "%{BASE_DIR}external/lyra/external/vulkan_1.2.141.0/Lib32"
    filter { "platforms:Win64"}
        postbuildcommands { "{copy} %{wks.location}../external/lyra/external/vulkan_1.2.141.0/Bin/shaderc_shared.dll %{cfg.buildtarget.directory}" }
        libdirs "%{BASE_DIR}external/lyra/external/vulkan_1.2.141.0/Lib"

    filter "kind:not StaticLib"
        links "vulkan-1.lib"
        links "shaderc_shared.lib"

    filter "configurations:Debug"
        ignoredefaultlibraries { "msvcrt.lib" } 
    filter {}
    
    
end

function includeGLFW()
    filter { "platforms:Win32" }
        includedirs "%{BASE_DIR}external/lyra/external/glfw/glfw-3.3.2.bin.WIN32/include"
    filter { "platforms:Win64" }
        includedirs "%{BASE_DIR}external/lyra/external/glfw/glfw-3.3.2.bin.WIN64/include"
    filter {}
end

function linkGLFW()
    filter { "platforms:Win32", "action:vs2019" }
        libdirs "%{BASE_DIR}external/lyra/external/glfw/glfw-3.3.2.bin.WIN32/lib-vc2019"
    filter { "platforms:Win64", "action:vs2019" }
        libdirs "%{BASE_DIR}external/lyra/external/glfw/glfw-3.3.2.bin.WIN64/lib-vc2019"

    links "glfw3"

    filter {}

end

function includeTracy()
    filter {}
    includedirs "%{BASE_DIR}external/lyra/external/tracy"
    defines { "TRACY_ENABLE", "TRACY_ON_DEMAND" }
end

function includeFMOD()
    filter {}
        includedirs "%{BASE_DIR}external/lyra/external/fmod/api/core/inc"
end

function linkFMOD()
    filter { "platforms:Win64"}
        libdirs "%{BASE_DIR}external/lyra/external/fmod/api/core/lib/x64"
	
	filter "configurations:Release"
        postbuildcommands { "{copy} %{wks.location}../external/lyra/external/FMOD/api/core/lib/x64/fmod.dll %{cfg.buildtarget.directory}" }
		links "fmod_vc.lib"

    filter "configurations:Debug"
        postbuildcommands { "{copy} %{wks.location}../external/lyra/external/FMOD/api/core/lib/x64/fmodL.dll %{cfg.buildtarget.directory}" }
		links "fmodL_vc.lib"

    filter {}
end
    