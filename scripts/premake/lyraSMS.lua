
project "lyraSMS"
    kind "ConsoleApp"
    links { "lyra" ,"lyraEditor" }

    language "C++"
    targetdir "%{BASE_DIR}bin/lyraSMS/%{cfg.buildcfg}"

    files { 
        "%{BASE_DIR}LyraSMS/**.h", 
        "%{BASE_DIR}LyraSMS/**.hpp", 
        "%{BASE_DIR}LyraSMS/**.inl", 
        "%{BASE_DIR}LyraSMS/**.cpp", 
        "%{BASE_DIR}LyraSMS/**.c" 
    }

    includedirs {
        "%{BASE_DIR}external/lyra/packages/Lyra/include",
        "%{BASE_DIR}external/lyra/packages/LyraEditor/include",
        "%{BASE_DIR}LyraSMS"
    }

    configureFlags()

    -- External libraries
    includeVulkan()
    linkVulkan()

    includeGLFW()
    linkGLFW()

    includeTracy()

    linkXInput()

    includeFMOD()
    linkFMOD()

    -- End external libraries

    setConfigurations()

    filter {}
