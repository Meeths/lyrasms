BASE_DIR = "../../"

function configureFlags()
    flags { "FatalWarnings", "MultiProcessorCompile" }    
    staticruntime "Off"
    filter {"configurations:Release"}
        flags { "LinkTimeOptimization" }
    filter {}
    defines { "NOMINMAX" }
    cppdialect "C++17"
    editAndContinue "Off"

end

function setConfigurations()
    filter "configurations:Debug"
        defines { "DEBUG", "_DEBUG" }
        symbols "On"
        runtime "Debug"

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"
        runtime "Release"
end

workspace "lyraSMS"
    location "%{BASE_DIR}projects"
    configurations { "Debug", "Release" }
    platforms { "Win64" }

    targetdir "%{BASE_DIR}bin/lyraSMS/%{cfg.buildcfg}"

    filter { "platforms:Win64" }
        system "Windows"
        architecture "x64"

    filter {}