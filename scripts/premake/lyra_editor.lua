project "lyraEditor"
    kind "StaticLib"
    language "C++"
    targetdir "%{BASE_DIR}bin/%{cfg.buildcfg}"

    files { 
        "%{BASE_DIR}external/lyra/packages/LyraEditor/**.h", 
        "%{BASE_DIR}external/lyra/packages/LyraEditor/**.hpp", 
        "%{BASE_DIR}external/lyra/packages/LyraEditor/**.inl", 
        "%{BASE_DIR}external/lyra/packages/LyraEditor/**.cpp", 
        "%{BASE_DIR}external/lyra/packages/LyraEditor/**.c" 
    }

    includedirs {
        "%{BASE_DIR}external/lyra/packages/Lyra/include",
        "%{BASE_DIR}external/lyra/packages/LyraEditor/include"
    }

    configureFlags()

    -- External libraries
    includeVulkan()
    linkVulkan()
    includeGLFW()
    includeTracy()    
    includeFMOD()
    -- End external libraries

    setConfigurations()

    filter {}