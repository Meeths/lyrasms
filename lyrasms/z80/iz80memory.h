#pragma once
#include <System/Types.h>

class IZ80Memory
{
public:
	virtual ~IZ80Memory() = default;
	virtual void WRITE(Word address, Byte data) = 0;
	virtual Byte READ(Word address) = 0;
};