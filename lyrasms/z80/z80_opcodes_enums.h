#pragma once

enum OpcodeID
{
	NOP,				// 00
	LD_BC_NN,			// 01
	LD_xBC_A,			// 02
	INC_BC,				// 03
	INC_B,				// 04
	DEC_B,				// 05
	LD_B_N,				// 06
	RLCA,				// 07

	EX_AF_AF,			// 08
	ADD_HL_BC,			// 09
	LD_A_xBC,			// 0A
	DEC_BC,				// 0B
	INC_C,				// 0C
	DEC_C,				// 0D
	LD_C_N,				// 0E
	RRCA,				// 0F

	DJNZ,				// 10
	LD_DE_NN,			// 11
	LD_xDE_A,			// 12
	INC_DE,				// 13
	INC_D,				// 14
	DEC_D,				// 15
	LD_D_N,				// 16
	RLA,				// 17

	JR,					// 18
	ADD_HL_DE,			// 19
	LD_A_xDE,			// 1A
	DEC_DE,				// 1B
	INC_E,				// 1C
	DEC_E,				// 1D
	LD_E_N,				// 1E
	RRA,				// 1F

	JR_NZ,				// 20
	LD_HL_NN,			// 21
	LD_xNN_HL,			// 22
	INC_HL,				// 23
	INC_H,				// 24
	DEC_H,				// 25
	LD_H_N,				// 26
	DAA,				// 27

	JR_Z,				// 28
	ADD_HL_HL,			// 29
	LD_HL_xNN,			// 2A
	DEC_HL,				// 2B
	INC_L,				// 2C
	DEC_L,				// 2D
	LD_L_N,				// 2E
	CPL,				// 2F

	JR_NC,				// 30
	LD_SP_NN,			// 31
	LD_xNN_A,			// 32
	INC_SP,				// 33
	INC_mHL,			// 34
	DEC_mHL,			// 35
	LD_mHL_N,			// 36
	SCF,				// 37

	JR_C,				// 38
	ADD_HL_SP,			// 39
	LD_A_xNN,			// 3A
	DEC_SP,				// 3B
	INC_A,				// 3C
	DEC_A,				// 3D
	LD_A_N,				// 3E
	CCF,				// 3F

	LD_B_B,				// 40
	LD_B_C,				// 41
	LD_B_D,				// 42
	LD_B_E,				// 43
	LD_B_H,				// 44
	LD_B_L,				// 45
	LD_B_mHL,			// 46
	LD_B_A,				// 47

	LD_C_B,				// 48
	LD_C_C,				// 49
	LD_C_D,				// 4A
	LD_C_E,				// 4B
	LD_C_H,				// 4C
	LD_C_L,				// 4D
	LD_C_mHL,			// 4E
	LD_C_A,				// 4F

	LD_D_B,				// 50
	LD_D_C,				// 51
	LD_D_D,				// 52
	LD_D_E,				// 53
	LD_D_H,				// 54
	LD_D_L,				// 55
	LD_D_mHL,			// 56
	LD_D_A,				// 57

	LD_E_B,				// 58
	LD_E_C,				// 59
	LD_E_D,				// 5A
	LD_E_E,				// 5B
	LD_E_H,				// 5C
	LD_E_L,				// 5D
	LD_E_mHL,			// 5E
	LD_E_A,				// 5F

	LD_H_B,				// 60
	LD_H_C,				// 61
	LD_H_D,				// 62
	LD_H_E,				// 63
	LD_H_H,				// 64
	LD_H_L,				// 65
	LD_H_mHL,			// 66
	LD_H_A,				// 67

	LD_L_B,				// 68
	LD_L_C,				// 69
	LD_L_D,				// 6A
	LD_L_E,				// 6B
	LD_L_H,				// 6C
	LD_L_L,				// 6D
	LD_L_mHL,			// 6E
	LD_L_A,				// 6F

	LD_mHL_B,			// 70
	LD_mHL_C,			// 71
	LD_mHL_D,			// 72
	LD_mHL_E,			// 73
	LD_mHL_H,			// 74
	LD_mHL_L,			// 75
	HALT,				// 76
	LD_mHL_A,			// 77

	LD_A_B,				// 78
	LD_A_C,				// 79
	LD_A_D,				// 7A
	LD_A_E,				// 7B
	LD_A_H,				// 7C
	LD_A_L,				// 7D
	LD_A_mHL,			// 7E
	LD_A_A,				// 7F

	ADD_B,				// 80
	ADD_C,				// 81
	ADD_D,				// 82
	ADD_E,				// 83
	ADD_H,				// 84
	ADD_L,				// 85
	ADD_mHL,			// 86
	ADD_A,				// 87

	ADC_B,				// 88
	ADC_C,				// 89
	ADC_D,				// 8A
	ADC_E,				// 8B
	ADC_H,				// 8C
	ADC_L,				// 8D
	ADC_mHL,			// 8E
	ADC_A,				// 8F

	SUB_B,				// 90
	SUB_C,				// 91
	SUB_D,				// 92
	SUB_E,				// 93
	SUB_H,				// 94
	SUB_L,				// 95
	SUB_mHL,			// 96
	SUB_A,				// 97

	SBC_B,				// 98
	SBC_C,				// 99
	SBC_D,				// 9A
	SBC_E,				// 9B
	SBC_H,				// 9C
	SBC_L,				// 9D
	SBC_mHL,			// 9E
	SBC_A,				// 9F

	AND_B,				// A0
	AND_C,				// A1
	AND_D,				// A2
	AND_E,				// A3
	AND_H,				// A4
	AND_L,				// A5
	AND_mHL,			// A6
	AND_A,				// A7

	XOR_B,				// A8
	XOR_C,				// A9
	XOR_D,				// AA
	XOR_E,				// AB
	XOR_H,				// AC
	XOR_L,				// AD
	XOR_mHL,			// AE
	XOR_A,				// AF

	OR_B,				// B0
	OR_C,				// B1
	OR_D,				// B2
	OR_E,				// B3
	OR_H,				// B4
	OR_L,				// B5
	OR_mHL,				// B6
	OR_A,				// B7

	CP_B,				// B8
	CP_C,				// B9
	CP_D,				// BA
	CP_E,				// BB
	CP_H,				// BC
	CP_L,				// BD
	CP_mHL,				// BE
	CP_A,				// BF

	RET_NZ,				// C0
	POP_BC,				// C1
	JP_NZ,				// C2
	JP,					// C3
	CALL_NZ,			// C4
	PUSH_BC,			// C5
	ADD_N,				// C6
	RST00,				// C7

	RET_Z,				// C8
	RET,				// C9
	JP_Z,				// CA
	PFX_CB,				// CB
	CALL_Z,				// CC
	CALL,				// CD
	ADC_N,				// CE
	RST08,				// CF

	RET_NC,				// D0
	POP_DE,				// D1
	JP_NC,				// D2
	OUTA,				// D3
	CALL_NC,			// D4
	PUSH_DE,			// D5
	SUB_N,				// D6
	RST10,				// D7

	RET_C,				// D8
	EXX,				// D9
	JP_C,				// DA
	INA,				// DB
	CALL_C,				// DC
	PFX_DD,				// DD
	SBC_N,				// DE
	RST18,				// DF

	RET_PO,				// E0
	POP_HL,				// E1
	JP_PO,				// E2
	EX_HL_xSP,			// E3
	CALL_PO,			// E4
	PUSH_HL,			// E5
	AND_N,				// E6
	RST20,				// E7

	RET_PE,				// E8
	LD_PC_HL,			// E9
	JP_PE,				// EA
	EX_DE_HL,			// EB
	CALL_PE,			// EC
	PFX_ED,				// ED
	XOR_N,				// EE
	RST28,				// EF

	RET_P,				// F0
	POP_AF,				// F1
	JP_P,				// F2
	DI,					// F3
	CALL_P,				// F4
	PUSH_AF,			// F5
	OR_N,				// F6
	RST30,				// F7

	RET_M,				// F8
	LD_SP_HL,			// F9
	JP_M,				// FA
	EI,					// FB
	CALL_M,				// FC
	PFX_FD,				// FD
	CP_N,				// FE
	RST38				// FF
};

enum OpcodeCBID
{
	RLC_B,				// 00
	RLC_C,				// 01
	RLC_D,				// 02
	RLC_E,				// 03
	RLC_H,				// 04
	RLC_L,				// 05
	RLC_mHL,			// 06
	RLC_A,				// 07

	RRC_B,				// 08
	RRC_C,				// 09
	RRC_D,				// 0A
	RRC_E,				// 0B
	RRC_H,				// 0C
	RRC_L,				// 0D
	RRC_mHL,			// 0E
	RRC_A,				// 0F

	RL_B,				// 10
	RL_C,				// 11
	RL_D,				// 12
	RL_E,				// 13
	RL_H,				// 14
	RL_L,				// 15
	RL_mHL,				// 16
	RL_A,				// 17

	RR_B,				// 18
	RR_C,				// 19
	RR_D,				// 1A
	RR_E,				// 1B
	RR_H,				// 1C
	RR_L,				// 1D
	RR_mHL,				// 1E
	RR_A,				// 1F

	SLA_B,				// 20
	SLA_C,				// 21
	SLA_D,				// 22
	SLA_E,				// 23
	SLA_H,				// 24
	SLA_L,				// 25
	SLA_mHL,			// 26
	SLA_A,				// 27

	SRA_B,				// 28
	SRA_C,				// 29
	SRA_D,				// 2A
	SRA_E,				// 2B
	SRA_H,				// 2C
	SRA_L,				// 2D
	SRA_mHL,			// 2E
	SRA_A,				// 2F

	SLL_B,				// 30
	SLL_C,				// 31
	SLL_D,				// 32
	SLL_E,				// 33
	SLL_H,				// 34
	SLL_L,				// 35
	SLL_mHL,			// 36
	SLL_A,				// 37

	SRL_B,				// 38
	SRL_C,				// 39
	SRL_D,				// 3A
	SRL_E,				// 3B
	SRL_H,				// 3C
	SRL_L,				// 3D
	SRL_mHL,			// 3E
	SRL_A,				// 3F

	BIT_0_B,			// 40
	BIT_0_C,			// 41
	BIT_0_D,			// 42
	BIT_0_E,			// 43
	BIT_0_H,			// 44
	BIT_0_L,			// 45
	BIT_0_mHL,			// 46
	BIT_0_A,			// 47

	BIT_1_B,			// 48
	BIT_1_C,			// 49
	BIT_1_D,			// 4A
	BIT_1_E,			// 4B
	BIT_1_H,			// 4C
	BIT_1_L,			// 4D
	BIT_1_mHL,			// 4E
	BIT_1_A,			// 4F

	BIT_2_B,			// 50
	BIT_2_C,			// 51
	BIT_2_D,			// 52
	BIT_2_E,			// 53
	BIT_2_H,			// 54
	BIT_2_L,			// 55
	BIT_2_mHL,			// 56
	BIT_2_A,			// 57

	BIT_3_B,			// 58
	BIT_3_C,			// 59
	BIT_3_D,			// 5A
	BIT_3_E,			// 5B
	BIT_3_H,			// 5C
	BIT_3_L,			// 5D
	BIT_3_mHL,			// 5E
	BIT_3_A,			// 5F

	BIT_4_B,			// 60
	BIT_4_C,			// 61
	BIT_4_D,			// 62
	BIT_4_E,			// 63
	BIT_4_H,			// 64
	BIT_4_L,			// 65
	BIT_4_mHL,			// 66
	BIT_4_A,			// 67

	BIT_5_B,			// 68
	BIT_5_C,			// 69
	BIT_5_D,			// 6A
	BIT_5_E,			// 6B
	BIT_5_H,			// 6C
	BIT_5_L,			// 6D
	BIT_5_mHL,			// 6E
	BIT_5_A,			// 6F

	BIT_6_B,			// 70
	BIT_6_C,			// 71
	BIT_6_D,			// 72
	BIT_6_E,			// 73
	BIT_6_H,			// 74
	BIT_6_L,			// 75
	BIT_6_mHL,			// 76
	BIT_6_A,			// 77

	BIT_7_B,			// 78
	BIT_7_C,			// 79
	BIT_7_D,			// 7A
	BIT_7_E,			// 7B
	BIT_7_H,			// 7C
	BIT_7_L,			// 7D
	BIT_7_mHL,			// 7E
	BIT_7_A,			// 7F

	RES_0_B,			// 80
	RES_0_C,			// 81
	RES_0_D,			// 82
	RES_0_E,			// 83
	RES_0_H,			// 84
	RES_0_L,			// 85
	RES_0_mHL,			// 86
	RES_0_A,			// 87

	RES_1_B,			// 88
	RES_1_C,			// 89
	RES_1_D,			// 8A
	RES_1_E,			// 8B
	RES_1_H,			// 8C
	RES_1_L,			// 8D
	RES_1_mHL,			// 8E
	RES_1_A,			// 8F

	RES_2_B,			// 90
	RES_2_C,			// 91
	RES_2_D,			// 92
	RES_2_E,			// 93
	RES_2_H,			// 94
	RES_2_L,			// 95
	RES_2_mHL,			// 96
	RES_2_A,			// 97

	RES_3_B,			// 98
	RES_3_C,			// 99
	RES_3_D,			// 9A
	RES_3_E,			// 9B
	RES_3_H,			// 9C
	RES_3_L,			// 9D
	RES_3_mHL,			// 9E
	RES_3_A,			// 9F

	RES_4_B,			// A0
	RES_4_C,			// A1
	RES_4_D,			// A2
	RES_4_E,			// A3
	RES_4_H,			// A4
	RES_4_L,			// A5
	RES_4_mHL,			// A6
	RES_4_A,			// A7

	RES_5_B,			// A8
	RES_5_C,			// A9
	RES_5_D,			// AA
	RES_5_E,			// AB
	RES_5_H,			// AC
	RES_5_L,			// AD
	RES_5_mHL,			// AE
	RES_5_A,			// AF

	RES_6_B,			// B0
	RES_6_C,			// B1
	RES_6_D,			// B2
	RES_6_E,			// B3
	RES_6_H,			// B4
	RES_6_L,			// B5
	RES_6_mHL,			// B6
	RES_6_A,			// B7

	RES_7_B,			// B8
	RES_7_C,			// B9
	RES_7_D,			// BA
	RES_7_E,			// BB
	RES_7_H,			// BC
	RES_7_L,			// BD
	RES_7_mHL,			// BE
	RES_7_A,			// BF

	SET_0_B,			// C0
	SET_0_C,			// C1
	SET_0_D,			// C2
	SET_0_E,			// C3
	SET_0_H,			// C4
	SET_0_L,			// C5
	SET_0_mHL,			// C6
	SET_0_A,			// C7

	SET_1_B,			// C8
	SET_1_C,			// C9
	SET_1_D,			// CA
	SET_1_E,			// CB
	SET_1_H,			// CC
	SET_1_L,			// CD
	SET_1_mHL,			// CE
	SET_1_A,			// CF

	SET_2_B,			// D0
	SET_2_C,			// D1
	SET_2_D,			// D2
	SET_2_E,			// D3
	SET_2_H,			// D4
	SET_2_L,			// D5
	SET_2_mHL,			// D6
	SET_2_A,			// D7

	SET_3_B,			// D8
	SET_3_C,			// D9
	SET_3_D,			// DA
	SET_3_E,			// DB
	SET_3_H,			// DC
	SET_3_L,			// DD
	SET_3_mHL,			// DE
	SET_3_A,			// DF

	SET_4_B,			// E0
	SET_4_C,			// E1
	SET_4_D,			// E2
	SET_4_E,			// E3
	SET_4_H,			// E4
	SET_4_L,			// E5
	SET_4_mHL,			// E6
	SET_4_A,			// E7

	SET_5_B,			// E8
	SET_5_C,			// E9
	SET_5_D,			// EA
	SET_5_E,			// EB
	SET_5_H,			// EC
	SET_5_L,			// ED
	SET_5_mHL,			// EE
	SET_5_A,			// EF

	SET_6_B,			// F0
	SET_6_C,			// F1
	SET_6_D,			// F2
	SET_6_E,			// F3
	SET_6_H,			// F4
	SET_6_L,			// F5
	SET_6_mHL,			// F6
	SET_6_A,			// F7

	SET_7_B,			// F8
	SET_7_C,			// F9
	SET_7_D,			// FA
	SET_7_E,			// FB
	SET_7_H,			// FC
	SET_7_L,			// FD
	SET_7_mHL,			// FE
	SET_7_A				// FF
};
enum  OpcodeEDID
{
	ED_00,				// 00
	ED_01,				// 01
	ED_02,				// 02
	ED_03,				// 03
	ED_04,				// 04
	ED_05,				// 05
	ED_06,				// 06
	ED_07,				// 07

	ED_08,				// 08
	ED_09,				// 09
	ED_0A,				// 0A
	ED_0B,				// 0B
	ED_0C,				// 0C
	ED_0D,				// 0D
	ED_0E,				// 0E
	ED_0F,				// 0F

	ED_10,				// 10
	ED_11,				// 11
	ED_12,				// 12
	ED_13,				// 13
	ED_14,				// 14
	ED_15,				// 15
	ED_16,				// 16
	ED_17,				// 17

	ED_18,				// 18
	ED_19,				// 19
	ED_1A,				// 1A
	ED_1B,				// 1B
	ED_1C,				// 1C
	ED_1D,				// 1D
	ED_1E,				// 1E
	ED_1F,				// 1F

	ED_20,				// 20
	ED_21,				// 21
	ED_22,				// 22
	ED_23,				// 23
	ED_24,				// 24
	ED_25,				// 25
	ED_26,				// 26
	ED_27,				// 27

	ED_28,				// 28
	ED_29,				// 29
	ED_2A,				// 2A
	ED_2B,				// 2B
	ED_2C,				// 2C
	ED_2D,				// 2D
	ED_2E,				// 2E
	ED_2F,				// 2F

	ED_30,				// 30
	ED_31,				// 31
	ED_32,				// 32
	ED_33,				// 33
	ED_34,				// 34
	ED_35,				// 35
	ED_36,				// 36
	ED_37,				// 37

	ED_38,				// 38
	ED_39,				// 39
	ED_3A,				// 3A
	ED_3B,				// 3B
	ED_3C,				// 3C
	ED_3D,				// 3D
	ED_3E,				// 3E
	ED_3F,				// 3F

	IN_B_mC,			// 40
	OUT_mC_B,			// 41
	SBC_HL_BC,			// 42
	LD_mNN_BC,			// 43
	NEG,				// 44
	RETN,				// 45
	IM_0,				// 46
	LD_I_A,				// 47

	IN_C_mC,			// 48
	OUT_mC_C,			// 49
	ADC_HL_BC,			// 4A
	LD_BC_mNN,			// 4B
	ED_4C,				// 4C
	RETI,				// 4D
	ED_4E,				// 4E
	LD_R_A,				// 4F

	IN_D_mC,			// 50
	OUT_mC_D,			// 51
	SBC_HL_DE,			// 52
	LD_mNN_DE,			// 53
	ED_54,				// 54
	ED_55,				// 55
	IM_1,				// 56
	LD_A_I,				// 57

	IN_E_mC,			// 58
	OUT_mC_E,			// 59
	ADC_HL_DE,			// 5A
	LD_DE_mNN,			// 5B
	ED_5C,				// 5C
	ED_5D,				// 5D
	IM_2,				// 5E
	LD_A_R,				// 5F

	IN_H_mC,			// 60
	OUT_mC_H,			// 61
	SBC_HL_HL,			// 62
	LD_mNN_HL,			// 63
	ED_64,				// 64
	ED_65,				// 65
	ED_66,				// 66
	RRD,				// 67

	IN_L_mC,			// 68
	OUT_mC_L,			// 69
	ADC_HL_HL,			// 6A
	LD_HL_mNN,			// 6B
	ED_6C,				// 6C
	ED_6D,				// 6D
	ED_6E,				// 6E
	RLD,				// 6F

	IN_F_mC,			// 70
	OUT_mC_0,			// 71
	SBC_HL_SP,			// 72
	LD_mNN_SP,			// 73
	ED_74,				// 74
	ED_75,				// 75
	ED_76,				// 76
	ED_77,				// 77

	IN_A_mC,			// 78
	OUT_mC_A,			// 79
	ADC_HL_SP,			// 7A
	LD_SP_mNN,			// 7B
	ED_7C,				// 7C
	ED_7D,				// 7D
	ED_7E,				// 7E
	ED_7F,				// 7F

	ED_80,				// 80
	ED_81,				// 81
	ED_82,				// 82
	ED_83,				// 83
	ED_84,				// 84
	ED_85,				// 85
	ED_86,				// 86
	ED_87,				// 87

	ED_88,				// 88
	ED_89,				// 89
	ED_8A,				// 8A
	ED_8B,				// 8B
	ED_8C,				// 8C
	ED_8D,				// 8D
	ED_8E,				// 8E
	ED_8F,				// 8F

	ED_90,				// 90
	ED_91,				// 91
	ED_92,				// 92
	ED_93,				// 93
	ED_94,				// 94
	ED_95,				// 95
	ED_96,				// 96
	ED_97,				// 97

	ED_98,				// 98
	ED_99,				// 99
	ED_9A,				// 9A
	ED_9B,				// 9B
	ED_9C,				// 9C
	ED_9D,				// 9D
	ED_9E,				// 9E
	ED_9F,				// 9F

	LDI,				// A0
	CPI,				// A1
	INI,				// A2
	OUTI,				// A3
	ED_A4,				// A4
	ED_A5,				// A5
	ED_A6,				// A6
	ED_A7,				// A7

	LDD,				// A8
	CPD,				// A9
	IND,				// AA
	OUTD,				// AB
	ED_AC,				// AC
	ED_AD,				// AD
	ED_AE,				// AE
	ED_AF,				// AF

	LDIR,				// B0
	CPIR,				// B1
	INIR,				// B2
	OTIR,				// B3
	ED_B4,				// B4
	ED_B5,				// B5
	ED_B6,				// B6
	ED_B7,				// B7

	LDDR,				// B8
	CPDR,				// B9
	INDR,				// BA
	OTDR,				// BB
	ED_BC,				// BC
	ED_BD,				// BD
	ED_BE,				// BE
	ED_BF,				// BF

	ED_C0,				// C0
	ED_C1,				// C1
	ED_C2,				// C2
	ED_C3,				// C3
	ED_C4,				// C4
	ED_C5,				// C5
	ED_C6,				// C6
	ED_C7,				// C7

	ED_C8,				// C8
	ED_C9,				// C9
	ED_CA,				// CA
	ED_CB,				// CB
	ED_CC,				// CC
	ED_CD,				// CD
	ED_CE,				// CE
	ED_CF,				// CF

	ED_D0,				// D0
	ED_D1,				// D1
	ED_D2,				// D2
	ED_D3,				// D3
	ED_D4,				// D4
	ED_D5,				// D5
	ED_D6,				// D6
	ED_D7,				// D7

	ED_D8,				// D8
	ED_D9,				// D9
	ED_DA,				// DA
	ED_DB,				// DB
	ED_DC,				// DC
	ED_DD,				// DD
	ED_DE,				// DE
	ED_DF,				// DF

	ED_E0,				// E0
	ED_E1,				// E1
	ED_E2,				// E2
	ED_E3,				// E3
	ED_E4,				// E4
	ED_E5,				// E5
	ED_E6,				// E6
	ED_E7,				// E7

	ED_E8,				// E8
	ED_E9,				// E9
	ED_EA,				// EA
	ED_EB,				// EB
	ED_EC,				// EC
	ED_ED,				// ED
	ED_EE,				// EE
	ED_EF,				// EF

	ED_F0,				// F0
	ED_F1,				// F1
	ED_F2,				// F2
	ED_F3,				// F3
	ED_F4,				// F4
	ED_F5,				// F5
	ED_F6,				// F6
	ED_F7,				// F7

	ED_F8,				// F8
	ED_F9,				// F9
	ED_FA,				// FA
	ED_FB,				// FB
	ED_FC,				// FC
	ED_FD,				// FD
	ED_FE,				// FE
	ED_FF				// FF
};
