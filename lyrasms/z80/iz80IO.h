#pragma once
#include <System/Types.h>

class IZ80IO
{
public:
	virtual ~IZ80IO() = default;
	virtual void WRITE(Byte address, Byte data) = 0;
	virtual Byte READ(Byte address) = 0;
};