#include <z80/z80.h>
#include <z80/z80macros.h>
#include <z80/z80_opcodes_enums.h>
#include <System/Log.h>

bool z80::ExecuteOpcode(Byte opcode)
{
	switch(opcode)
	{
	case NOP: { return true; } break;
	case LD_BC_NN: { BC = ReadNextWord(); return true; } break;
	case LD_xBC_A: { WriteByte(BC, A); return true; } break;
	case INC_BC: { Inc16Bit(BC); return true; } break;
	case INC_B: { Inc8Bit(B); return true; } break;
	case DEC_B: { Dec8Bit(B); return true; } break;
	case LD_B_N: { B = ReadNextByte(); return true; } break;
	case RLCA: { Rlca8Bit(); return true; } break;

	case EX_AF_AF: { std::swap(AF, mAFShadow.reg); return true; } break;
	case ADD_HL_BC: { Add16Bit(SelectHLRegister().reg, BC); return true; } break;
	case LD_A_xBC: { A = ReadByte(BC); return true; } break;
	case DEC_BC: { Dec16Bit(BC); return true; } break;
	case INC_C: { Inc8Bit(C); return true; } break;
	case DEC_C: { Dec8Bit(C); return true; } break;
	case LD_C_N: { C = ReadNextByte(); return true; } break;
	case RRCA: { Rrca8Bit(); return true; } break;

	case DJNZ: Djnz(); return false; break;
	case LD_DE_NN: { DE = ReadNextWord(); return true; } break;
	case LD_xDE_A: { WriteByte(DE, A); return true; } break;
	case INC_DE: { Inc16Bit(DE); return true; } break;
	case INC_D: { Inc8Bit(D); return true; } break;
	case DEC_D: { Dec8Bit(D); return true; } break;
	case LD_D_N: { D = ReadNextByte(); return true; } break;
	case RLA: { Rla8Bit(); return true; } break;

	case JR: { mPC += ReadNextSByte(); return true; } break;
	case ADD_HL_DE: { Add16Bit(SelectHLRegister().reg, DE); return true; } break;
	case LD_A_xDE: { A = ReadByte(DE); return true; } break;
	case DEC_DE: { Dec16Bit(DE); return true; } break;
	case INC_E: { Inc8Bit(E); return true; } break;
	case DEC_E: { Dec8Bit(E); return true; } break;
	case LD_E_N: { E = ReadNextByte(); return true; } break;
	case RRA: { Rra8Bit(); return true; } break;

	case JR_NZ: { SByte r = ReadNextSByte(); if (TEST_NZ) { mPC += r; mInstructionCycles = 12; } else mInstructionCycles = 7; return false; } break;
	case LD_HL_NN: { SelectHLRegister().reg = ReadNextWord(); return true; } break;
	case LD_xNN_HL: { WriteWord(ReadNextWord(), SelectHLRegister().reg); return true; } break;
	case INC_HL: { Inc16Bit(SelectHLRegister().reg); return true; } break;
	case INC_H: { Inc8Bit(H_hX_hY); return true; } break;
	case DEC_H: { Dec8Bit(H_hX_hY); return true; } break;
	case LD_H_N: { H_hX_hY = ReadNextByte(); return true; } break;
	case DAA: { Daa(); return true; } break;

	case JR_Z: { SByte r = ReadNextSByte(); if (TEST_Z) { mPC += r; mInstructionCycles = 12; } else mInstructionCycles = 7; return false; } break;
	case ADD_HL_HL: { Add16Bit(SelectHLRegister().reg, SelectHLRegister().reg); return true; } break;
	case LD_HL_xNN: { SelectHLRegister().reg = ReadWord(ReadNextWord()); return true; } break;
	case DEC_HL: { Dec16Bit(SelectHLRegister().reg); return true; } break;
	case INC_L: { Inc8Bit(L_lX_lY); return true; } break;
	case DEC_L: { Dec8Bit(L_lX_lY); return true; } break;
	case LD_L_N: { L_lX_lY = ReadNextByte(); return true; } break;
	case CPL: { A = ~A; F |= HF | NF; return true; } break;

	case JR_NC: { SByte r = ReadNextSByte(); if (TEST_NC) { mPC += r; mInstructionCycles = 12; } else mInstructionCycles = 7; return false; } break;
	case LD_SP_NN: { mSP = ReadNextWord(); return true; } break;
	case LD_xNN_A: { WriteByte(ReadNextWord(), A); return true; } break;
	case INC_SP: { Inc16Bit(mSP); return true; } break;
	case INC_mHL: { DO_IN_MEMORY(HL_IX_IY, Inc8Bit); return true; } break;
	case DEC_mHL: { DO_IN_MEMORY(HL_IX_IY, Dec8Bit); return true; } break;
	case LD_mHL_N: { Word adr = HL_IX_IY; WriteByte(adr, ReadNextByte()); return true; } break;
	case SCF: { SetFlag(FlagRegisterBits::CARRY); ResetFlag(FlagRegisterBits::HALF_CARRY); ResetFlag(FlagRegisterBits::SUBTRACT); return true; } break;

	case JR_C: { SByte r = ReadNextSByte(); if (TEST_C) { mPC += r; mInstructionCycles = 12; } else mInstructionCycles = 7; return false; } break;
	case ADD_HL_SP: { Add16Bit(SelectHLRegister().reg, mSP); return true; } break;
	case LD_A_xNN: { A = ReadByte(ReadNextWord()); return true; } break;
	case DEC_SP: { Dec16Bit(mSP); return true; } break;
	case INC_A: { Inc8Bit(A); return true; } break;
	case DEC_A: { Dec8Bit(A); return true; } break;
	case LD_A_N: { A = ReadNextByte(); return true; } break;
	case CCF: { ResetFlag(FlagRegisterBits::SUBTRACT);
			if (TEST_C) { 	ResetFlag(FlagRegisterBits::CARRY); SetFlag(FlagRegisterBits::HALF_CARRY); }
			else { 			SetFlag(FlagRegisterBits::CARRY); ResetFlag(FlagRegisterBits::HALF_CARRY); }; return true; } break;

	case LD_B_B: { B = B; return true; } break;
	case LD_B_C: { B = C; return true; } break;
	case LD_B_D: { B = D; return true; } break;
	case LD_B_E: { B = E; return true; } break;
	case LD_B_H: { B = H_hX_hY; return true; } break;
	case LD_B_L: { B = L_lX_lY; return true; } break;
	case LD_B_mHL: { B = ReadByte(HL_IX_IY); return true; } break;
	case LD_B_A: { B = A; return true; } break;

	case LD_C_B: { C = B; return true; } break;
	case LD_C_C: { C = C; return true; } break;
	case LD_C_D: { C = D; return true; } break;
	case LD_C_E: { C = E; return true; } break;
	case LD_C_H: { C = H_hX_hY; return true; } break;
	case LD_C_L: { C = L_lX_lY; return true; } break;
	case LD_C_mHL: { C = ReadByte(HL_IX_IY); return true; } break;
	case LD_C_A: { C = A; return true; } break;

	case LD_D_B: { D = B; return true; } break;
	case LD_D_C: { D = C; return true; } break;
	case LD_D_D: { D = D; return true; } break;
	case LD_D_E: { D = E; return true; } break;
	case LD_D_H: { D = H_hX_hY; return true; } break;
	case LD_D_L: { D = L_lX_lY; return true; } break;
	case LD_D_mHL: { D = ReadByte(HL_IX_IY); return true; } break;
	case LD_D_A: { D = A; return true; } break;

	case LD_E_B: { E = B; return true; } break;
	case LD_E_C: { E = C; return true; } break;
	case LD_E_D: { E = D; return true; } break;
	case LD_E_E: { E = E; return true; } break;
	case LD_E_H: { E = H_hX_hY; return true; } break;
	case LD_E_L: { E = L_lX_lY; return true; } break;
	case LD_E_mHL: { E = ReadByte(HL_IX_IY); return true; } break;
	case LD_E_A: { E = A; return true; } break;

	case LD_H_B: { H_hX_hY = B; return true; } break;
	case LD_H_C: { H_hX_hY = C; return true; } break;
	case LD_H_D: { H_hX_hY = D; return true; } break;
	case LD_H_E: { H_hX_hY = E; return true; } break;
	case LD_H_H: { H_hX_hY = H_hX_hY; return true; } break;
	case LD_H_L: { H_hX_hY = L_lX_lY; return true; } break;
	case LD_H_mHL: { H = ReadByte(HL_IX_IY); return true; } break;
	case LD_H_A: { H_hX_hY = A; return true; } break;

	case LD_L_B: { L_lX_lY = B; return true; } break;
	case LD_L_C: { L_lX_lY = C; return true; } break;
	case LD_L_D: { L_lX_lY = D; return true; } break;
	case LD_L_E: { L_lX_lY = E; return true; } break;
	case LD_L_H: { L_lX_lY = H_hX_hY; return true; } break;
	case LD_L_L: { L_lX_lY = L_lX_lY; return true; } break;
	case LD_L_mHL: { L = ReadByte(HL_IX_IY); return true; } break;
	case LD_L_A: { L_lX_lY = A; return true; } break;

	case LD_mHL_B: { WriteByte(HL_IX_IY, B); return true; } break;
	case LD_mHL_C: { WriteByte(HL_IX_IY, C); return true; } break;
	case LD_mHL_D: { WriteByte(HL_IX_IY, D); return true; } break;
	case LD_mHL_E: { WriteByte(HL_IX_IY, E); return true; } break;
	case LD_mHL_H: { WriteByte(HL_IX_IY, H); return true; } break;
	case LD_mHL_L: { WriteByte(HL_IX_IY, L); return true; } break;
	case HALT: { mHalted = true; return true; } break;
	case LD_mHL_A: { WriteByte(HL_IX_IY, A); return true; } break;

	case LD_A_B: { A = B; return true; } break;
	case LD_A_C: { A = C; return true; } break;
	case LD_A_D: { A = D; return true; } break;
	case LD_A_E: { A = E; return true; } break;
	case LD_A_H: { A = H_hX_hY; return true; } break;
	case LD_A_L: { A = L_lX_lY; return true; } break;
	case LD_A_mHL: { A = ReadByte(HL_IX_IY); return true; } break;
	case LD_A_A: { A = A; return true; } break;

	case ADD_B: { Add8Bit(B); return true; } break;
	case ADD_C: { Add8Bit(C); return true; } break;
	case ADD_D: { Add8Bit(D); return true; } break;
	case ADD_E: { Add8Bit(E); return true; } break;
	case ADD_H: { Add8Bit(H_hX_hY); return true; } break;
	case ADD_L: { Add8Bit(L_lX_lY); return true; } break;
	case ADD_mHL: { Add8Bit(ReadByte(HL_IX_IY)); return true; } break;
	case ADD_A: { Add8Bit(A); return true; } break;

	case ADC_B: { Adca8Bit(B); return true; } break;
	case ADC_C: { Adca8Bit(C); return true; } break;
	case ADC_D: { Adca8Bit(D); return true; } break;
	case ADC_E: { Adca8Bit(E); return true; } break;
	case ADC_H: { Adca8Bit(H_hX_hY); return true; } break;
	case ADC_L: { Adca8Bit(L_lX_lY); return true; } break;
	case ADC_mHL: { Adca8Bit(ReadByte(HL_IX_IY)); return true; } break;
	case ADC_A: { Adca8Bit(A); return true; } break;

	case SUB_B: { Sub8Bit(B); return true; } break;
	case SUB_C: { Sub8Bit(C); return true; } break;
	case SUB_D: { Sub8Bit(D); return true; } break;
	case SUB_E: { Sub8Bit(E); return true; } break;
	case SUB_H: { Sub8Bit(H_hX_hY); return true; } break;
	case SUB_L: { Sub8Bit(L_lX_lY); return true; } break;
	case SUB_mHL: { Sub8Bit(ReadByte(HL_IX_IY)); return true; } break;
	case SUB_A: { Sub8Bit(A); return true; } break;

	case SBC_B: { Sbc8Bit(B); return true; } break;
	case SBC_C: { Sbc8Bit(C); return true; } break;
	case SBC_D: { Sbc8Bit(D); return true; } break;
	case SBC_E: { Sbc8Bit(E); return true; } break;
	case SBC_H: { Sbc8Bit(H_hX_hY); return true; } break;
	case SBC_L: { Sbc8Bit(L_lX_lY); return true; } break;
	case SBC_mHL: { Sbc8Bit(ReadByte(HL_IX_IY)); return true; } break;
	case SBC_A: { Sbc8Bit(A); return true; } break;

	case AND_B: { And8Bit(A, B); return true; } break;
	case AND_C: { And8Bit(A, C); return true; } break;
	case AND_D: { And8Bit(A, D); return true; } break;
	case AND_E: { And8Bit(A, E); return true; } break;
	case AND_H: { And8Bit(A, H_hX_hY); return true; } break;
	case AND_L: { And8Bit(A, L_lX_lY); return true; } break;
	case AND_mHL: { And8Bit(A, ReadByte(HL_IX_IY)); return true; } break;
	case AND_A: { And8Bit(A, A); return true; } break;

	case XOR_B: { Xor8Bit(A, B); return true; } break;
	case XOR_C: { Xor8Bit(A, C); return true; } break;
	case XOR_D: { Xor8Bit(A, D); return true; } break;
	case XOR_E: { Xor8Bit(A, E); return true; } break;
	case XOR_H: { Xor8Bit(A, H_hX_hY); return true; } break;
	case XOR_L: { Xor8Bit(A, L_lX_lY); return true; } break;
	case XOR_mHL: { Xor8Bit(A, ReadByte(HL_IX_IY)); return true; } break;
	case XOR_A: { Xor8Bit(A, A); return true; } break;

	case OR_B: { Or8Bit(A, B); return true; } break;
	case OR_C: { Or8Bit(A, C); return true; } break;
	case OR_D: { Or8Bit(A, D); return true; } break;
	case OR_E: { Or8Bit(A, E); return true; } break;
	case OR_H: { Or8Bit(A, H_hX_hY); return true; } break;
	case OR_L: { Or8Bit(A, L_lX_lY); return true; } break;
	case OR_mHL: { Or8Bit(A, ReadByte(HL_IX_IY)); return true; } break;
	case OR_A: { Or8Bit(A, A); return true; } break;

	case CP_B: { Cmp8Bit(A, B); return true; } break;
	case CP_C: { Cmp8Bit(A, C); return true; } break;
	case CP_D: { Cmp8Bit(A, D); return true; } break;
	case CP_E: { Cmp8Bit(A, E); return true; } break;
	case CP_H: { Cmp8Bit(A, H_hX_hY); return true; } break;
	case CP_L: { Cmp8Bit(A, L_lX_lY); return true; } break;
	case CP_mHL: { Cmp8Bit(A, ReadByte(HL_IX_IY)); return true; } break;
	case CP_A: { F = NF | ZF; return true; } break;

	case RET_NZ: { if (TEST_NZ) { mPC = Pop(); return true; } return true; } break;
	case POP_BC: { BC = Pop(); ; return true; } break;
	case JP_NZ: { Word r = ReadNextWord(); if (TEST_NZ) mPC = r; return true; } break;
	case JP: { Word r = ReadNextWord(); mPC = r; return true; } break;
	case CALL_NZ: { Word r = ReadNextWord(); if (TEST_NZ) { CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	case PUSH_BC: { Push(BC); return true; } break;
	case ADD_N: { Add8Bit(ReadNextByte()); return true; } break;
	case RST00: { CPURestart(0); return true; } break;
	case RET_Z: { if (TEST_Z) { mPC = Pop(); return true; } return true; } break;
	case RET: { mPC = Pop(); return true; } break;
	case JP_Z: { Word r = ReadNextWord(); if (TEST_Z) mPC = r; return true; } break;
	// 0xCB
	case CALL_Z: { Word r = ReadNextWord(); if (TEST_Z) { CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	case CALL: { Word r = ReadNextWord(); CALL(r); return true; } break;
	case ADC_N: { Adca8Bit(ReadNextByte()); return true; } break;
	case RST08: { CPURestart(0x8); return true; } break;

	case RET_NC: { if (TEST_NC) { mPC = Pop(); return true; } return true; } break;
	case POP_DE: { DE = Pop(); ; return true; } break;
	case JP_NC: { Word r = ReadNextWord(); if (TEST_NC) mPC = r; return true; } break;
	case OUTA: { WriteIOByte(ReadNextByte(), A); return true; } break;
	case CALL_NC: { Word r = ReadNextWord(); if (TEST_NC) { CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	case PUSH_DE: { Push(DE); return true; } break;
	case SUB_N: { Sub8Bit(ReadNextByte()); return true; } break;
	case RST10: { CPURestart(0x10); return true; } break;

	case RET_C: { if (TEST_C) { mPC = Pop(); return true; } return true; } break;
	case EXX: { std::swap(BC, mBCShadow.reg); std::swap(DE, mDEShadow.reg); std::swap(HL, mHLShadow.reg); return true; } break;
	case JP_C: { Word r = ReadNextWord(); if (TEST_C) mPC = r; return true; } break;
	case INA: { A = ReadIOByte(ReadNextByte()); return true; } break;
	case CALL_C: { Word r = ReadNextWord(); if (TEST_C) { CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	// 0xDD
	case SBC_N: { Sbc8Bit(ReadNextByte()); return true; } break;
	case RST18: { CPURestart(0x18); return true; } break;

	case RET_PO: { if (TEST_PO) { mPC = Pop(); return true; } return true; } break;
	case POP_HL: { SelectHLRegister().reg = Pop(); return true; } break;
	case JP_PO: { Word r = ReadNextWord(); if (TEST_PO) mPC = r; return true; } break;
	case EX_HL_xSP: { Word oldHL = SelectHLRegister().reg; SelectHLRegister().reg = ReadWord(mSP); WriteWord(mSP, oldHL); return true; } break;
	case CALL_PO: { Word r = ReadNextWord(); if (TEST_PO){ CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	case PUSH_HL: { Push(SelectHLRegister().reg); return true; } break;
	case AND_N: { And8Bit(A, ReadNextByte()); return true; } break;
	case RST20: { CPURestart(0x20); return true; } break;

	case RET_PE: { if (TEST_PE) { mPC = Pop(); return true; } return true; } break;
	case LD_PC_HL: { mPC = SelectHLRegister().reg; return true; } break;
	case JP_PE: { Word r = ReadNextWord(); if (TEST_PE) mPC = r; return true; } break;
	case EX_DE_HL: { std::swap(DE, HL); return true; } break;
	case CALL_PE: { Word r = ReadNextWord(); if (TEST_PE) { CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	// 0xED
	case XOR_N: { Xor8Bit(A, ReadNextByte()); return true; } break;
	case RST28: { CPURestart(0x28); return true; } break;

	case RET_P: { if (TEST_P) { mPC = Pop(); return true; } return true; } break;
	case POP_AF: { AF = Pop(); ; return true; } break;
	case JP_P: { Word r = ReadNextWord(); if (TEST_P) mPC = r; return true; } break;
	case DI: { mInterruptFlipFlop1 = false; mInterruptFlipFlop2 = false; return true; } break;
	case CALL_P: { Word r = ReadNextWord(); if (TEST_P) { CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	case PUSH_AF: { Push(AF); return true; } break;
	case OR_N: { Or8Bit(A, ReadNextByte()); return true; } break;
	case RST30: { CPURestart(0x30); return true; } break;

	case RET_M: { if (TEST_M) { mPC = Pop(); return true; } return true; } break;
	case LD_SP_HL: { mSP = SelectHLRegister().reg; return true; } break;
	case JP_M: { Word r = ReadNextWord(); if (TEST_M) mPC = r; return true; } break;
	case EI: { mEnableInterruptRequested = true; return true; } break;
	case CALL_M: { Word r = ReadNextWord(); if (TEST_M) { CALL(r); mInstructionCycles = 17; }
				 else { mInstructionCycles = 10; }	return false; } break;
	// 0xFD
	case CP_N: { Cmp8Bit(A, ReadNextByte()); return true; } break;
	case RST38: { CPURestart(0x38); return true; } break;
	}

	lyraLogError("CPU: Instruction 0x%02x not implemented", (unsigned char)opcode);
	return false;
}

bool z80::ExecuteCBOpcode(Byte opcode)
{
	switch(opcode)
	{
	case RLC_B:		Rlc8Bit(B); 	return true; break;
	case RLC_C:		Rlc8Bit(C); 	return true; break;
	case RLC_D:		Rlc8Bit(D); 	return true; break;
	case RLC_E:		Rlc8Bit(E); 	return true; break;
	case RLC_H:		Rlc8Bit(H); 	return true; break;
	case RLC_L:		Rlc8Bit(L); 	return true; break;
	case RLC_mHL:	DO_IN_MEMORY(HL, Rlc8Bit); 	return true; break;
	case RLC_A:		Rlc8Bit(A); 	return true; break;

	case RRC_B:		Rrc8Bit(B); 	return true; break;
	case RRC_C:		Rrc8Bit(C); 	return true; break;
	case RRC_D:		Rrc8Bit(D); 	return true; break;
	case RRC_E:		Rrc8Bit(E); 	return true; break;
	case RRC_H:		Rrc8Bit(H); 	return true; break;
	case RRC_L:		Rrc8Bit(L); 	return true; break;
	case RRC_mHL:	DO_IN_MEMORY(HL, Rrc8Bit); 	return true; break;
	case RRC_A:		Rrc8Bit(A); 	return true; break;

	case RL_B:		Rl8Bit(B); 		return true; break;
	case RL_C:		Rl8Bit(C); 		return true; break;
	case RL_D:		Rl8Bit(D); 		return true; break;
	case RL_E:		Rl8Bit(E); 		return true; break;
	case RL_H:		Rl8Bit(H); 		return true; break;
	case RL_L:		Rl8Bit(L); 		return true; break;
	case RL_mHL:	DO_IN_MEMORY(HL, Rl8Bit); 	return true; break;
	case RL_A:		Rl8Bit(A); 		return true; break;

	case RR_B:		Rr8Bit(B); 		return true; break;
	case RR_C:		Rr8Bit(C); 		return true; break;
	case RR_D:		Rr8Bit(D); 		return true; break;
	case RR_E:		Rr8Bit(E); 		return true; break;
	case RR_H:		Rr8Bit(H); 		return true; break;
	case RR_L:		Rr8Bit(L); 		return true; break;
	case RR_mHL:	DO_IN_MEMORY(HL, Rr8Bit); 	return true; break;
	case RR_A:		Rr8Bit(A); 		return true; break;

	case SLA_B:		Sla8Bit(B);     return true; break;
	case SLA_C:		Sla8Bit(C);     return true; break;
	case SLA_D:		Sla8Bit(D);     return true; break;
	case SLA_E:		Sla8Bit(E);     return true; break;
	case SLA_H:		Sla8Bit(H);     return true; break;
	case SLA_L:		Sla8Bit(L);     return true; break;
	case SLA_mHL:	DO_IN_MEMORY(HL, Sla8Bit);   return true; break;
	case SLA_A:		Sla8Bit(A);     return true; break;

	case SRA_B:		Sra8Bit(B);     return true; break;
	case SRA_C:		Sra8Bit(C);     return true; break;
	case SRA_D:		Sra8Bit(D);     return true; break;
	case SRA_E:		Sra8Bit(E);     return true; break;
	case SRA_H:		Sra8Bit(H);     return true; break;
	case SRA_L:		Sra8Bit(L);     return true; break;
	case SRA_mHL:	DO_IN_MEMORY(HL, Sra8Bit);   return true; break;
	case SRA_A:		Sra8Bit(A);     return true; break;

	case SLL_B:		Sll8Bit(B);     return true; break;
	case SLL_C:		Sll8Bit(C);     return true; break;
	case SLL_D:		Sll8Bit(D);     return true; break;
	case SLL_E:		Sll8Bit(E);     return true; break;
	case SLL_H:		Sll8Bit(H);     return true; break;
	case SLL_L:		Sll8Bit(L);     return true; break;
	case SLL_mHL:	DO_IN_MEMORY(HL, Sll8Bit);   return true; break;
	case SLL_A:		Sll8Bit(A);     return true; break;

	case SRL_B:		Srl8Bit(B);     return true; break;
	case SRL_C:		Srl8Bit(C);     return true; break;
	case SRL_D:		Srl8Bit(D);     return true; break;
	case SRL_E:		Srl8Bit(E);     return true; break;
	case SRL_H:		Srl8Bit(H);     return true; break;
	case SRL_L:		Srl8Bit(L);     return true; break;
	case SRL_mHL:	DO_IN_MEMORY(HL, Srl8Bit);   return true; break;
	case SRL_A:		Srl8Bit(A);     return true; break;

	case BIT_0_B:	TestBit8Bit(B, 0);return true; break;
	case BIT_0_C:	TestBit8Bit(C, 0);return true; break;
	case BIT_0_D:	TestBit8Bit(D, 0);return true; break;
	case BIT_0_E:	TestBit8Bit(E, 0);return true; break;
	case BIT_0_H:	TestBit8Bit(H, 0);return true; break;
	case BIT_0_L:	TestBit8Bit(L, 0);return true; break;
	case BIT_0_mHL:	TestBit8Bit(ReadByte(HL), 0);  return true; break;
	case BIT_0_A:	TestBit8Bit(A, 0);return true; break;

	case BIT_1_B:	TestBit8Bit(B, 1);return true; break;
	case BIT_1_C:	TestBit8Bit(C, 1);return true; break;
	case BIT_1_D:	TestBit8Bit(D, 1);return true; break;
	case BIT_1_E:	TestBit8Bit(E, 1);return true; break;
	case BIT_1_H:	TestBit8Bit(H, 1);return true; break;
	case BIT_1_L:	TestBit8Bit(L, 1);return true; break;
	case BIT_1_mHL:	TestBit8Bit(ReadByte(HL), 1);  return true; break;
	case BIT_1_A:	TestBit8Bit(A, 1);return true; break;

	case BIT_2_B:	TestBit8Bit(B, 2);return true; break;
	case BIT_2_C:	TestBit8Bit(C, 2);return true; break;
	case BIT_2_D:	TestBit8Bit(D, 2);return true; break;
	case BIT_2_E:	TestBit8Bit(E, 2);return true; break;
	case BIT_2_H:	TestBit8Bit(H, 2);return true; break;
	case BIT_2_L:	TestBit8Bit(L, 2);return true; break;
	case BIT_2_mHL:	TestBit8Bit(ReadByte(HL), 2);  return true; break;
	case BIT_2_A:	TestBit8Bit(A, 2);return true; break;

	case BIT_3_B:	TestBit8Bit(B, 3);return true; break;
	case BIT_3_C:	TestBit8Bit(C, 3);return true; break;
	case BIT_3_D:	TestBit8Bit(D, 3);return true; break;
	case BIT_3_E:	TestBit8Bit(E, 3);return true; break;
	case BIT_3_H:	TestBit8Bit(H, 3);return true; break;
	case BIT_3_L:	TestBit8Bit(L, 3);return true; break;
	case BIT_3_mHL:	TestBit8Bit(ReadByte(HL), 3);  return true; break;
	case BIT_3_A:	TestBit8Bit(A, 3);return true; break;

	case BIT_4_B:	TestBit8Bit(B, 4);return true; break;
	case BIT_4_C:	TestBit8Bit(C, 4);return true; break;
	case BIT_4_D:	TestBit8Bit(D, 4);return true; break;
	case BIT_4_E:	TestBit8Bit(E, 4);return true; break;
	case BIT_4_H:	TestBit8Bit(H, 4);return true; break;
	case BIT_4_L:	TestBit8Bit(L, 4);return true; break;
	case BIT_4_mHL:	TestBit8Bit(ReadByte(HL), 4);  return true; break;
	case BIT_4_A:	TestBit8Bit(A, 4);return true; break;

	case BIT_5_B:	TestBit8Bit(B, 5);return true; break;
	case BIT_5_C:	TestBit8Bit(C, 5);return true; break;
	case BIT_5_D:	TestBit8Bit(D, 5);return true; break;
	case BIT_5_E:	TestBit8Bit(E, 5);return true; break;
	case BIT_5_H:	TestBit8Bit(H, 5);return true; break;
	case BIT_5_L:	TestBit8Bit(L, 5);return true; break;
	case BIT_5_mHL:	TestBit8Bit(ReadByte(HL), 5);  return true; break;
	case BIT_5_A:	TestBit8Bit(A, 5);return true; break;

	case BIT_6_B:	TestBit8Bit(B, 6);return true; break;
	case BIT_6_C:	TestBit8Bit(C, 6);return true; break;
	case BIT_6_D:	TestBit8Bit(D, 6);return true; break;
	case BIT_6_E:	TestBit8Bit(E, 6);return true; break;
	case BIT_6_H:	TestBit8Bit(H, 6);return true; break;
	case BIT_6_L:	TestBit8Bit(L, 6);return true; break;
	case BIT_6_mHL:	TestBit8Bit(ReadByte(HL), 6);  return true; break;
	case BIT_6_A:	TestBit8Bit(A, 6);return true; break;

	case BIT_7_B:	TestBit8Bit(B, 7);return true; break;
	case BIT_7_C:	TestBit8Bit(C, 7);return true; break;
	case BIT_7_D:	TestBit8Bit(D, 7);return true; break;
	case BIT_7_E:	TestBit8Bit(E, 7);return true; break;
	case BIT_7_H:	TestBit8Bit(H, 7);return true; break;
	case BIT_7_L:	TestBit8Bit(L, 7);return true; break;
	case BIT_7_mHL:	TestBit8Bit(ReadByte(HL), 7);  return true; break;
	case BIT_7_A:	TestBit8Bit(A, 7);return true; break;

	case RES_0_B:	ResetBit8Bit(B, 0);return true; break;
	case RES_0_C:	ResetBit8Bit(C, 0);return true; break;
	case RES_0_D:	ResetBit8Bit(D, 0);return true; break;
	case RES_0_E:	ResetBit8Bit(E, 0);return true; break;
	case RES_0_H:	ResetBit8Bit(H, 0);return true; break;
	case RES_0_L:	ResetBit8Bit(L, 0);return true; break;
	case RES_0_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 0);     return true; break;
	case RES_0_A:	ResetBit8Bit(A, 0);return true; break;

	case RES_1_B:	ResetBit8Bit(B, 1);return true; break;
	case RES_1_C:	ResetBit8Bit(C, 1);return true; break;
	case RES_1_D:	ResetBit8Bit(D, 1);return true; break;
	case RES_1_E:	ResetBit8Bit(E, 1);return true; break;
	case RES_1_H:	ResetBit8Bit(H, 1);return true; break;
	case RES_1_L:	ResetBit8Bit(L, 1);return true; break;
	case RES_1_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 1);     return true; break;
	case RES_1_A:	ResetBit8Bit(A, 1);return true; break;

	case RES_2_B:	ResetBit8Bit(B, 2);return true; break;
	case RES_2_C:	ResetBit8Bit(C, 2);return true; break;
	case RES_2_D:	ResetBit8Bit(D, 2);return true; break;
	case RES_2_E:	ResetBit8Bit(E, 2);return true; break;
	case RES_2_H:	ResetBit8Bit(H, 2);return true; break;
	case RES_2_L:	ResetBit8Bit(L, 2);return true; break;
	case RES_2_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 2);     return true; break;
	case RES_2_A:	ResetBit8Bit(A, 2);return true; break;

	case RES_3_B:	ResetBit8Bit(B, 3);return true; break;
	case RES_3_C:	ResetBit8Bit(C, 3);return true; break;
	case RES_3_D:	ResetBit8Bit(D, 3);return true; break;
	case RES_3_E:	ResetBit8Bit(E, 3);return true; break;
	case RES_3_H:	ResetBit8Bit(H, 3);return true; break;
	case RES_3_L:	ResetBit8Bit(L, 3);return true; break;
	case RES_3_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 3);     return true; break;
	case RES_3_A:	ResetBit8Bit(A, 3);return true; break;

	case RES_4_B:	ResetBit8Bit(B, 4);return true; break;
	case RES_4_C:	ResetBit8Bit(C, 4);return true; break;
	case RES_4_D:	ResetBit8Bit(D, 4);return true; break;
	case RES_4_E:	ResetBit8Bit(E, 4);return true; break;
	case RES_4_H:	ResetBit8Bit(H, 4);return true; break;
	case RES_4_L:	ResetBit8Bit(L, 4);return true; break;
	case RES_4_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 4);     return true; break;
	case RES_4_A:	ResetBit8Bit(A, 4);return true; break;

	case RES_5_B:	ResetBit8Bit(B, 5);return true; break;
	case RES_5_C:	ResetBit8Bit(C, 5);return true; break;
	case RES_5_D:	ResetBit8Bit(D, 5);return true; break;
	case RES_5_E:	ResetBit8Bit(E, 5);return true; break;
	case RES_5_H:	ResetBit8Bit(H, 5);return true; break;
	case RES_5_L:	ResetBit8Bit(L, 5);return true; break;
	case RES_5_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 5);     return true; break;
	case RES_5_A:	ResetBit8Bit(A, 5);return true; break;

	case RES_6_B:	ResetBit8Bit(B, 6);return true; break;
	case RES_6_C:	ResetBit8Bit(C, 6);return true; break;
	case RES_6_D:	ResetBit8Bit(D, 6);return true; break;
	case RES_6_E:	ResetBit8Bit(E, 6);return true; break;
	case RES_6_H:	ResetBit8Bit(H, 6);return true; break;
	case RES_6_L:	ResetBit8Bit(L, 6);return true; break;
	case RES_6_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 6);     return true; break;
	case RES_6_A:	ResetBit8Bit(A, 6);return true; break;

	case RES_7_B:	ResetBit8Bit(B, 7);return true; break;
	case RES_7_C:	ResetBit8Bit(C, 7);return true; break;
	case RES_7_D:	ResetBit8Bit(D, 7);return true; break;
	case RES_7_E:	ResetBit8Bit(E, 7);return true; break;
	case RES_7_H:	ResetBit8Bit(H, 7);return true; break;
	case RES_7_L:	ResetBit8Bit(L, 7);return true; break;
	case RES_7_mHL:	DO_IN_MEMORY_1P(HL, ResetBit8Bit, 7);     return true; break;
	case RES_7_A:	ResetBit8Bit(A, 7);return true; break;

	case SET_0_B:	SetBit8Bit(B, 0);return true; break;
	case SET_0_C:	SetBit8Bit(C, 0);return true; break;
	case SET_0_D:	SetBit8Bit(D, 0);return true; break;
	case SET_0_E:	SetBit8Bit(E, 0);return true; break;
	case SET_0_H:	SetBit8Bit(H, 0);return true; break;
	case SET_0_L:	SetBit8Bit(L, 0);return true; break;
	case SET_0_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 0);return true; break;
	case SET_0_A:	SetBit8Bit(A, 0);return true; break;

	case SET_1_B:	SetBit8Bit(B, 1);return true; break;
	case SET_1_C:	SetBit8Bit(C, 1);return true; break;
	case SET_1_D:	SetBit8Bit(D, 1);return true; break;
	case SET_1_E:	SetBit8Bit(E, 1);return true; break;
	case SET_1_H:	SetBit8Bit(H, 1);return true; break;
	case SET_1_L:	SetBit8Bit(L, 1);return true; break;
	case SET_1_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 1);return true; break;
	case SET_1_A:	SetBit8Bit(A, 1);return true; break;

	case SET_2_B:	SetBit8Bit(B, 2);return true; break;
	case SET_2_C:	SetBit8Bit(C, 2);return true; break;
	case SET_2_D:	SetBit8Bit(D, 2);return true; break;
	case SET_2_E:	SetBit8Bit(E, 2);return true; break;
	case SET_2_H:	SetBit8Bit(H, 2);return true; break;
	case SET_2_L:	SetBit8Bit(L, 2);return true; break;
	case SET_2_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 2);return true; break;
	case SET_2_A:	SetBit8Bit(A, 2);return true; break;

	case SET_3_B:	SetBit8Bit(B, 3);return true; break;
	case SET_3_C:	SetBit8Bit(C, 3);return true; break;
	case SET_3_D:	SetBit8Bit(D, 3);return true; break;
	case SET_3_E:	SetBit8Bit(E, 3);return true; break;
	case SET_3_H:	SetBit8Bit(H, 3);return true; break;
	case SET_3_L:	SetBit8Bit(L, 3);return true; break;
	case SET_3_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 3);return true; break;
	case SET_3_A:	SetBit8Bit(A, 3);return true; break;

	case SET_4_B:	SetBit8Bit(B, 4);return true; break;
	case SET_4_C:	SetBit8Bit(C, 4);return true; break;
	case SET_4_D:	SetBit8Bit(D, 4);return true; break;
	case SET_4_E:	SetBit8Bit(E, 4);return true; break;
	case SET_4_H:	SetBit8Bit(H, 4);return true; break;
	case SET_4_L:	SetBit8Bit(L, 4);return true; break;
	case SET_4_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 4);return true; break;
	case SET_4_A:	SetBit8Bit(A, 4);return true; break;

	case SET_5_B:	SetBit8Bit(B, 5);return true; break;
	case SET_5_C:	SetBit8Bit(C, 5);return true; break;
	case SET_5_D:	SetBit8Bit(D, 5);return true; break;
	case SET_5_E:	SetBit8Bit(E, 5);return true; break;
	case SET_5_H:	SetBit8Bit(H, 5);return true; break;
	case SET_5_L:	SetBit8Bit(L, 5);return true; break;
	case SET_5_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 5);return true; break;
	case SET_5_A:	SetBit8Bit(A, 5);return true; break;

	case SET_6_B:	SetBit8Bit(B, 6);return true; break;
	case SET_6_C:	SetBit8Bit(C, 6);return true; break;
	case SET_6_D:	SetBit8Bit(D, 6);return true; break;
	case SET_6_E:	SetBit8Bit(E, 6);return true; break;
	case SET_6_H:	SetBit8Bit(H, 6);return true; break;
	case SET_6_L:	SetBit8Bit(L, 6);return true; break;
	case SET_6_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 6);return true; break;
	case SET_6_A:	SetBit8Bit(A, 6);return true; break;

	case SET_7_B:	SetBit8Bit(B, 7);return true; break;
	case SET_7_C:	SetBit8Bit(C, 7);return true; break;
	case SET_7_D:	SetBit8Bit(D, 7);return true; break;
	case SET_7_E:	SetBit8Bit(E, 7);return true; break;
	case SET_7_H:	SetBit8Bit(H, 7);return true; break;
	case SET_7_L:	SetBit8Bit(L, 7);return true; break;
	case SET_7_mHL:	DO_IN_MEMORY_1P(HL, SetBit8Bit, 7);return true; break;
	case SET_7_A:	SetBit8Bit(A, 7);return true; break;
	}

	lyraLogError("CPU: Instruction 0xCB%02x not implemented", (unsigned char)opcode);
	return false;
}

bool z80::ExecuteEDOpcode(Byte opcode)
{
	switch(opcode)
	{
// 	case ED_00:
// 	case ED_01:
// 	case ED_02:
// 	case ED_03:
// 	case ED_04:
// 	case ED_05:
// 	case ED_06:
// 	case ED_07:
// 
// 	case ED_08:
// 	case ED_09:
// 	case ED_0A:
// 	case ED_0B:
// 	case ED_0C:
// 	case ED_0D:
// 	case ED_0E:
// 	case ED_0F:
// 
// 	case ED_10:
// 	case ED_11:
// 	case ED_12:
// 	case ED_13:
// 	case ED_14:
// 	case ED_15:
// 	case ED_16:
// 	case ED_17:
// 
// 	case ED_18:
// 	case ED_19:
// 	case ED_1A:
// 	case ED_1B:
// 	case ED_1C:
// 	case ED_1D:
// 	case ED_1E:
// 	case ED_1F:
// 
// 	case ED_20:
// 	case ED_21:
// 	case ED_22:
// 	case ED_23:
// 	case ED_24:
// 	case ED_25:
// 	case ED_26:
// 	case ED_27:
// 
// 	case ED_28:
// 	case ED_29:
// 	case ED_2A:
// 	case ED_2B:
// 	case ED_2C:
// 	case ED_2D:
// 	case ED_2E:
// 	case ED_2F:
// 
// 	case ED_30:
// 	case ED_31:
// 	case ED_32:
// 	case ED_33:
// 	case ED_34:
// 	case ED_35:
// 	case ED_36:
// 	case ED_37:
// 
// 	case ED_38:
// 	case ED_39:
// 	case ED_3A:
// 	case ED_3B:
// 	case ED_3C:
// 	case ED_3D:
// 	case ED_3E:
// 	case ED_3F:

	case IN_B_mC:		B = In(C); return true; break;
	case OUT_mC_B:		WriteIOByte(C, B); return true; break;
	case SBC_HL_BC:		SbcHL16Bit(BC);  return true; break;
	case LD_mNN_BC:		WriteWord(ReadNextWord(), BC);  return true; break;
	case NEG:			{Byte oldA = A; A = 0; Sub8Bit(oldA);}					return true; break;
// 	case RETN:
// 	case IM_0:
	case LD_I_A:		mI = A; 		return true; break;

	case IN_C_mC:		C = In(C); return true; break;
	case OUT_mC_C:		WriteIOByte(C, C); return true; break;
	case ADC_HL_BC:		AdcHL16Bit(BC);  return true; break;
	case LD_BC_mNN:		BC = ReadWord(ReadNextWord()); return true; break;
//	case ED_4C:
	case RETI:			mPC = Pop(); mInterruptFlipFlop1 = mInterruptFlipFlop2; return true; break;
// 	case ED_4E:
// 	case LD_R_A:

	case IN_D_mC:		D = In(C); return true; break;
	case OUT_mC_D:		WriteIOByte(C, D); return true; break;
	case SBC_HL_DE:		SbcHL16Bit(DE);  return true; break;
	case LD_mNN_DE:		WriteWord(ReadNextWord(), DE);  return true; break;
// 	case ED_54:
// 	case ED_55:
	case IM_1:			mIM = 1; 		return true; break;
	case LD_A_I:		Ldai(); 			return true; break;

	case IN_E_mC:		E = In(C); return true; break;
	case OUT_mC_E:		WriteIOByte(C, E); return true; break;
	case ADC_HL_DE:		AdcHL16Bit(DE);  return true; break;
	case LD_DE_mNN:		DE = ReadWord(ReadNextWord()); return true; break;
// 	case ED_5C:
// 	case ED_5D:
	case IM_2:			mIM = 2; 		return true; break;
	case LD_A_R:		Ldar(); 		return true; break;

	case IN_H_mC:		H = In(C); return true; break;
	case OUT_mC_H:		WriteIOByte(C, H); return true; break;
	case SBC_HL_HL:		SbcHL16Bit(HL);  return true; break;
// 	case LD_mNN_HL:
// 	case ED_64:
// 	case ED_65:
// 	case ED_66:
	case RRD:			Rrd(); 			return true; break;

	case IN_L_mC:		L = In(C); return true; break;
	case OUT_mC_L:		WriteIOByte(C, L); return true; break;
	case ADC_HL_HL:		AdcHL16Bit(HL);  return true; break;
// 	case LD_HL_mNN:
// 	case ED_6C:
// 	case ED_6D:
// 	case ED_6E:
 	case RLD:			Rld(); 			return true; break;
// 
// 	case IN_F_mC:
// 	case OUT_mC_0:
	case SBC_HL_SP:		SbcHL16Bit(mSP); return true; break;
	case LD_mNN_SP:		WriteWord(ReadNextWord(), mSP);return true; break;
// 	case ED_74:
// 	case ED_75:
// 	case ED_76:
// 	case ED_77:

	case IN_A_mC:		A = In(C); return true; break;
	case OUT_mC_A:		WriteIOByte(C, A); return true; break;
	case ADC_HL_SP:		AdcHL16Bit(mSP); return true; break;
	case LD_SP_mNN:		mSP = ReadWord(ReadNextWord()); return true; break;
// 	case ED_7C:
// 	case ED_7D:
// 	case ED_7E:
// 	case ED_7F:
// 
// 	case ED_80:
// 	case ED_81:
// 	case ED_82:
// 	case ED_83:
// 	case ED_84:
// 	case ED_85:
// 	case ED_86:
// 	case ED_87:
// 
// 	case ED_88:
// 	case ED_89:
// 	case ED_8A:
// 	case ED_8B:
// 	case ED_8C:
// 	case ED_8D:
// 	case ED_8E:
// 	case ED_8F:
// 
// 	case ED_90:
// 	case ED_91:
// 	case ED_92:
// 	case ED_93:
// 	case ED_94:
// 	case ED_95:
// 	case ED_96:
// 	case ED_97:
// 
// 	case ED_98:
// 	case ED_99:
// 	case ED_9A:
// 	case ED_9B:
// 	case ED_9C:
// 	case ED_9D:
// 	case ED_9E:
// 	case ED_9F:

	case LDI:			Ldi(); 			return true; break;
	case CPI:			Cpi(); 			return true; break;
	case INI:			Ini(); 			return true; break;
	case OUTI:			Outi(); 			return true; break;
// 	case ED_A4:
// 	case ED_A5:
// 	case ED_A6:
// 	case ED_A7:

	case LDD:			Ldd(); 			return true; break;
 	case CPD:			Cpd(); 			return true; break;
// 	case IND:
 	case OUTD:			Outd(); 			return true; break;
// 	case ED_AC:
// 	case ED_AD:
// 	case ED_AE:
// 	case ED_AF:

	case LDIR:			Ldir();	return false; break;
	case CPIR:			Cpir();	return false; break;
//	case INIR:
	case OTIR:			Otir();	return false; break;
// 	case ED_B4:
// 	case ED_B5:
// 	case ED_B6:
// 	case ED_B7:

	case LDDR:			Lddr(); return false; break;
 	case CPDR:			Cpdr(); return false; break;
// 	case INDR:
// 	case OTDR:
// 	case ED_BC:
// 	case ED_BD:
// 	case ED_BE:
// 	case ED_BF:
// 
// 	case ED_C0:
// 	case ED_C1:
// 	case ED_C2:
// 	case ED_C3:
// 	case ED_C4:
// 	case ED_C5:
// 	case ED_C6:
// 	case ED_C7:
// 
// 	case ED_C8:
// 	case ED_C9:
// 	case ED_CA:
// 	case ED_CB:
// 	case ED_CC:
// 	case ED_CD:
// 	case ED_CE:
// 	case ED_CF:
// 
// 	case ED_D0:
// 	case ED_D1:
// 	case ED_D2:
// 	case ED_D3:
// 	case ED_D4:
// 	case ED_D5:
// 	case ED_D6:
// 	case ED_D7:
// 
// 	case ED_D8:
// 	case ED_D9:
// 	case ED_DA:
// 	case ED_DB:
// 	case ED_DC:
// 	case ED_DD:
// 	case ED_DE:
// 	case ED_DF:
// 
// 	case ED_E0:
// 	case ED_E1:
// 	case ED_E2:
// 	case ED_E3:
// 	case ED_E4:
// 	case ED_E5:
// 	case ED_E6:
// 	case ED_E7:
// 
// 	case ED_E8:
// 	case ED_E9:
// 	case ED_EA:
// 	case ED_EB:
// 	case ED_EC:
// 	case ED_ED:
// 	case ED_EE:
// 	case ED_EF:
// 
// 	case ED_F0:
// 	case ED_F1:
// 	case ED_F2:
// 	case ED_F3:
// 	case ED_F4:
// 	case ED_F5:
// 	case ED_F6:
// 	case ED_F7:
// 
// 	case ED_F8:
// 	case ED_F9:
// 	case ED_FA:
// 	case ED_FB:
// 	case ED_FC:
// 	case ED_FD:
// 	case ED_FE:
// 	case ED_FF:

	}

	lyraLogError("CPU: Instruction 0xED%02x not implemented", (unsigned char)opcode);
	return false;
}

bool z80::ExecuteFDOpcode(Byte opcode)
{
	bool res = ExecuteOpcode(opcode); 

	if (!res) 
		lyraLogError("CPU: Instruction 0xFD%02x not implemented", (unsigned char)opcode);

	return res;
}

bool z80::ExecuteDDOpcode(Byte opcode)
{
	bool res = ExecuteOpcode(opcode);

	if (!res) 
		lyraLogError("CPU: Instruction 0xDD%02x not implemented", (unsigned char)opcode);
	
	return res;
}

bool z80::ExecuteDDFDBCOpcode(Byte opcode, SByte data)
{
	if (!(mDDInstruction || mFDInstruction))
	{
		lyraLogError("CPU: Calling DDFDCB Opcode without DD or FD");
		return false;
	}

	auto& reg = SelectHLRegister();
	const char* regName = mDDInstruction ? "IX" : "IY";

	switch (opcode)
	{
	case BIT_0_mHL: TestBit8Bit(ReadByte(reg.reg + data), 0); return true; break;
	case BIT_1_mHL: TestBit8Bit(ReadByte(reg.reg + data), 1); return true; break;
	case BIT_2_mHL: TestBit8Bit(ReadByte(reg.reg + data), 2); return true; break;
	case BIT_3_mHL: TestBit8Bit(ReadByte(reg.reg + data), 3); return true; break;
	case BIT_4_mHL: TestBit8Bit(ReadByte(reg.reg + data), 4); return true; break;
	case BIT_5_mHL: TestBit8Bit(ReadByte(reg.reg + data), 5); return true; break;
	case BIT_6_mHL: TestBit8Bit(ReadByte(reg.reg + data), 6); return true; break;
	case BIT_7_mHL: TestBit8Bit(ReadByte(reg.reg + data), 7); return true; break;

	case SET_0_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 0); return true; break;
	case SET_1_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 1); return true; break;
	case SET_2_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 2); return true; break;
	case SET_3_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 3); return true; break;
	case SET_4_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 4); return true; break;
	case SET_5_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 5); return true; break;
	case SET_6_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 6); return true; break;
	case SET_7_mHL: DO_IN_MEMORY_1P(reg.reg + data, SetBit8Bit, 7); return true; break;


	case RES_0_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 0); return true; break;
	case RES_1_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 1); return true; break;
	case RES_2_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 2); return true; break;
	case RES_3_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 3); return true; break;
	case RES_4_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 4); return true; break;
	case RES_5_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 5); return true; break;
	case RES_6_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 6); return true; break;
	case RES_7_mHL: DO_IN_MEMORY_1P(reg.reg + data, ResetBit8Bit, 7); return true; break;

	case RL_mHL: DO_IN_MEMORY(reg.reg + data, Rl8Bit); return true; break;
	case RLC_mHL: DO_IN_MEMORY(reg.reg + data, Rlc8Bit); return true; break;
	case RR_mHL: DO_IN_MEMORY(reg.reg + data, Rr8Bit); return true; break;
	case RRC_mHL: DO_IN_MEMORY(reg.reg + data, Rrc8Bit); return true; break;
	case SLA_mHL: DO_IN_MEMORY(reg.reg + data, Sla8Bit); return true; break;
	case SRA_mHL: DO_IN_MEMORY(reg.reg + data, Sra8Bit); return true; break;
	case SLL_mHL: DO_IN_MEMORY(reg.reg + data, Sll8Bit); return true; break;
	case SRL_mHL: DO_IN_MEMORY(reg.reg + data, Srl8Bit); return true; break;

	}
	return false;
}