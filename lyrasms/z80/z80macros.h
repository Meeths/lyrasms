#pragma once


#define HI_BYTE(x) ((Byte*)&x)[0] 
#define LO_BYTE(x) ((Byte*)&x)[1]

#define A mAF.hi
#define B mBC.hi
#define C mBC.lo
#define D mDE.hi
#define E mDE.lo
#define F mAF.lo
#define H mHL.hi
#define L mHL.lo

#define AF mAF.reg
#define BC mBC.reg
#define DE mDE.reg
#define HL mHL.reg

#define SF ((Byte)z80::FlagRegisterMasks::SIGN_MASK)
#define ZF ((Byte)z80::FlagRegisterMasks::ZERO_MASK)
#define HF ((Byte)z80::FlagRegisterMasks::HALF_CARRY_MASK)
#define CF ((Byte)z80::FlagRegisterMasks::CARRY_MASK)
#define PF ((Byte)z80::FlagRegisterMasks::PARITY_OVERFLOW_MASK)
#define VF ((Byte)z80::FlagRegisterMasks::PARITY_OVERFLOW_MASK)
#define NF ((Byte)z80::FlagRegisterMasks::SUBTRACT_MASK)

#define TEST_NZ (!(F & ZF))
#define TEST_Z (F & ZF)
#define TEST_NC (!(F & CF))
#define TEST_C (F & CF)
#define TEST_PO (!(F & PF))
#define TEST_PE (F & PF)
#define TEST_P (!(F & SF))
#define TEST_M (F & SF)

#define HL_IX_IY (SelectHLRegister().reg + (IsDDFDInstruction() ? ReadNextSByte() : 0))
#define H_hX_hY SelectHLRegister().hi
#define L_lX_lY SelectHLRegister().lo
#define DO_IN_MEMORY(address, what) { Word adr = address; auto r = ReadByte(adr); what##(r); WriteByte(adr, r); }
#define DO_IN_MEMORY_1P(address, what, p1) { Word adr = address; auto r = ReadByte(adr); what##(r, p1); WriteByte(adr, r); }
#define CALL(x) { Push(mPC) ; mPC = x; }



