#include <z80/z80.h>
#include <z80/z80macros.h>
#include <z80/iz80memory.h>
#include <z80/iz80IO.h>
#include <System/StringUtils.h>

#include <random>
#include <chrono>
#include <System/Log.h>

namespace
{
const Byte opCycles[256] =
{
   4,10, 7, 6, 4, 4, 7, 4, 4,11, 7, 6, 4, 4, 7, 4,
   8,10, 7, 6, 4, 4, 7, 4,12,11, 7, 6, 4, 4, 7, 4,
   7,10,16, 6, 4, 4, 7, 4, 7,11,16, 6, 4, 4, 7, 4,
   7,10,13, 6,11,11,10, 4, 7,11,13, 6, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   7, 7, 7, 7, 7, 7, 4, 7, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4,
   5,10,10,10,10,11, 7,11, 5,10,10, 0,10,17, 7,11,
   5,10,10,11,10,11, 7,11, 5, 4,10,11,10, 0, 7,11,
   5,10,10,19,10,11, 7,11, 5, 4,10, 4,10, 0, 7,11,
   5,10,10, 4,10,11, 7,11, 5, 6,10, 4,10, 0, 7,11
};

const Byte opCyclesCB[256] =
{
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,12, 8, 8, 8, 8, 8, 8, 8,12, 8,
   8, 8, 8, 8, 8, 8,12, 8, 8, 8, 8, 8, 8, 8,12, 8,
   8, 8, 8, 8, 8, 8,12, 8, 8, 8, 8, 8, 8, 8,12, 8,
   8, 8, 8, 8, 8, 8,12, 8, 8, 8, 8, 8, 8, 8,12, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8,
   8, 8, 8, 8, 8, 8,15, 8, 8, 8, 8, 8, 8, 8,15, 8
};

const Byte opCyclesED[256] =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  12,12,15,20, 8,14, 8, 9,12,12,15,20, 0,14, 0, 9,
  12,12,15,20, 0, 0, 8, 9,12,12,15,20, 0, 0, 8, 9,
  12,12,15,20, 0, 0, 0,18,12,12,15,20, 0, 0, 0,18,
  12, 0,15,20, 0, 0, 0, 0,12,12,15,20, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  16,16,16,16, 0, 0, 0, 0,16,16,16,16, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const Byte opCyclesDDFD[256] =
{
   0, 0, 0, 0, 0, 0, 0, 0, 0,15, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0,15, 0, 0, 0, 0, 0, 0,
   0,14,20,10, 9, 9, 9, 0, 0,15,20,10, 9, 9, 9, 0,
   0, 0, 0, 0,23,23,19, 0, 0,15, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 9, 9,19, 0, 0, 0, 0, 0, 9, 9,19, 0,
   0, 0, 0, 0, 9, 9,19, 0, 0, 0, 0, 0, 9, 9,19, 0,
   9, 9, 9, 9, 9, 9,19, 9, 9, 9, 9, 9, 9, 9,19, 9,
  19,19,19,19,19,19,19,19, 0, 0, 0, 0, 9, 9,19, 0,
   0, 0, 0, 0, 9, 9,19, 0, 0, 0, 0, 0, 9, 9,19, 0,
   0, 0, 0, 0, 9, 9,19, 0, 0, 0, 0, 0, 9, 9,19, 0,
   0, 0, 0, 0, 9, 9,19, 0, 0, 0, 0, 0, 9, 9,19, 0,
   0, 0, 0, 0, 9, 9,19, 0, 0, 0, 0, 0, 9, 9,19, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0,14, 0,23, 0,15, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0,
   0, 0, 0, 0, 0, 0, 0, 0, 0,10, 0, 0, 0, 0, 0, 0
};

const Byte opCyclesDDFDCB[256] =
{
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
  20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,
  20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,
  20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,
  20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0,
   0, 0, 0, 0, 0, 0,23, 0, 0, 0, 0, 0, 0, 0,23, 0
};

static Word DAATable[2048] =
{
  0x0044, 0x0100, 0x0200, 0x0304, 0x0400, 0x0504, 0x0604, 0x0700,
  0x0808, 0x090C, 0x1010, 0x1114, 0x1214, 0x1310, 0x1414, 0x1510,
  0x1000, 0x1104, 0x1204, 0x1300, 0x1404, 0x1500, 0x1600, 0x1704,
  0x180C, 0x1908, 0x2030, 0x2134, 0x2234, 0x2330, 0x2434, 0x2530,
  0x2020, 0x2124, 0x2224, 0x2320, 0x2424, 0x2520, 0x2620, 0x2724,
  0x282C, 0x2928, 0x3034, 0x3130, 0x3230, 0x3334, 0x3430, 0x3534,
  0x3024, 0x3120, 0x3220, 0x3324, 0x3420, 0x3524, 0x3624, 0x3720,
  0x3828, 0x392C, 0x4010, 0x4114, 0x4214, 0x4310, 0x4414, 0x4510,
  0x4000, 0x4104, 0x4204, 0x4300, 0x4404, 0x4500, 0x4600, 0x4704,
  0x480C, 0x4908, 0x5014, 0x5110, 0x5210, 0x5314, 0x5410, 0x5514,
  0x5004, 0x5100, 0x5200, 0x5304, 0x5400, 0x5504, 0x5604, 0x5700,
  0x5808, 0x590C, 0x6034, 0x6130, 0x6230, 0x6334, 0x6430, 0x6534,
  0x6024, 0x6120, 0x6220, 0x6324, 0x6420, 0x6524, 0x6624, 0x6720,
  0x6828, 0x692C, 0x7030, 0x7134, 0x7234, 0x7330, 0x7434, 0x7530,
  0x7020, 0x7124, 0x7224, 0x7320, 0x7424, 0x7520, 0x7620, 0x7724,
  0x782C, 0x7928, 0x8090, 0x8194, 0x8294, 0x8390, 0x8494, 0x8590,
  0x8080, 0x8184, 0x8284, 0x8380, 0x8484, 0x8580, 0x8680, 0x8784,
  0x888C, 0x8988, 0x9094, 0x9190, 0x9290, 0x9394, 0x9490, 0x9594,
  0x9084, 0x9180, 0x9280, 0x9384, 0x9480, 0x9584, 0x9684, 0x9780,
  0x9888, 0x998C, 0x0055, 0x0111, 0x0211, 0x0315, 0x0411, 0x0515,
  0x0045, 0x0101, 0x0201, 0x0305, 0x0401, 0x0505, 0x0605, 0x0701,
  0x0809, 0x090D, 0x1011, 0x1115, 0x1215, 0x1311, 0x1415, 0x1511,
  0x1001, 0x1105, 0x1205, 0x1301, 0x1405, 0x1501, 0x1601, 0x1705,
  0x180D, 0x1909, 0x2031, 0x2135, 0x2235, 0x2331, 0x2435, 0x2531,
  0x2021, 0x2125, 0x2225, 0x2321, 0x2425, 0x2521, 0x2621, 0x2725,
  0x282D, 0x2929, 0x3035, 0x3131, 0x3231, 0x3335, 0x3431, 0x3535,
  0x3025, 0x3121, 0x3221, 0x3325, 0x3421, 0x3525, 0x3625, 0x3721,
  0x3829, 0x392D, 0x4011, 0x4115, 0x4215, 0x4311, 0x4415, 0x4511,
  0x4001, 0x4105, 0x4205, 0x4301, 0x4405, 0x4501, 0x4601, 0x4705,
  0x480D, 0x4909, 0x5015, 0x5111, 0x5211, 0x5315, 0x5411, 0x5515,
  0x5005, 0x5101, 0x5201, 0x5305, 0x5401, 0x5505, 0x5605, 0x5701,
  0x5809, 0x590D, 0x6035, 0x6131, 0x6231, 0x6335, 0x6431, 0x6535,
  0x6025, 0x6121, 0x6221, 0x6325, 0x6421, 0x6525, 0x6625, 0x6721,
  0x6829, 0x692D, 0x7031, 0x7135, 0x7235, 0x7331, 0x7435, 0x7531,
  0x7021, 0x7125, 0x7225, 0x7321, 0x7425, 0x7521, 0x7621, 0x7725,
  0x782D, 0x7929, 0x8091, 0x8195, 0x8295, 0x8391, 0x8495, 0x8591,
  0x8081, 0x8185, 0x8285, 0x8381, 0x8485, 0x8581, 0x8681, 0x8785,
  0x888D, 0x8989, 0x9095, 0x9191, 0x9291, 0x9395, 0x9491, 0x9595,
  0x9085, 0x9181, 0x9281, 0x9385, 0x9481, 0x9585, 0x9685, 0x9781,
  0x9889, 0x998D, 0xA0B5, 0xA1B1, 0xA2B1, 0xA3B5, 0xA4B1, 0xA5B5,
  0xA0A5, 0xA1A1, 0xA2A1, 0xA3A5, 0xA4A1, 0xA5A5, 0xA6A5, 0xA7A1,
  0xA8A9, 0xA9AD, 0xB0B1, 0xB1B5, 0xB2B5, 0xB3B1, 0xB4B5, 0xB5B1,
  0xB0A1, 0xB1A5, 0xB2A5, 0xB3A1, 0xB4A5, 0xB5A1, 0xB6A1, 0xB7A5,
  0xB8AD, 0xB9A9, 0xC095, 0xC191, 0xC291, 0xC395, 0xC491, 0xC595,
  0xC085, 0xC181, 0xC281, 0xC385, 0xC481, 0xC585, 0xC685, 0xC781,
  0xC889, 0xC98D, 0xD091, 0xD195, 0xD295, 0xD391, 0xD495, 0xD591,
  0xD081, 0xD185, 0xD285, 0xD381, 0xD485, 0xD581, 0xD681, 0xD785,
  0xD88D, 0xD989, 0xE0B1, 0xE1B5, 0xE2B5, 0xE3B1, 0xE4B5, 0xE5B1,
  0xE0A1, 0xE1A5, 0xE2A5, 0xE3A1, 0xE4A5, 0xE5A1, 0xE6A1, 0xE7A5,
  0xE8AD, 0xE9A9, 0xF0B5, 0xF1B1, 0xF2B1, 0xF3B5, 0xF4B1, 0xF5B5,
  0xF0A5, 0xF1A1, 0xF2A1, 0xF3A5, 0xF4A1, 0xF5A5, 0xF6A5, 0xF7A1,
  0xF8A9, 0xF9AD, 0x0055, 0x0111, 0x0211, 0x0315, 0x0411, 0x0515,
  0x0045, 0x0101, 0x0201, 0x0305, 0x0401, 0x0505, 0x0605, 0x0701,
  0x0809, 0x090D, 0x1011, 0x1115, 0x1215, 0x1311, 0x1415, 0x1511,
  0x1001, 0x1105, 0x1205, 0x1301, 0x1405, 0x1501, 0x1601, 0x1705,
  0x180D, 0x1909, 0x2031, 0x2135, 0x2235, 0x2331, 0x2435, 0x2531,
  0x2021, 0x2125, 0x2225, 0x2321, 0x2425, 0x2521, 0x2621, 0x2725,
  0x282D, 0x2929, 0x3035, 0x3131, 0x3231, 0x3335, 0x3431, 0x3535,
  0x3025, 0x3121, 0x3221, 0x3325, 0x3421, 0x3525, 0x3625, 0x3721,
  0x3829, 0x392D, 0x4011, 0x4115, 0x4215, 0x4311, 0x4415, 0x4511,
  0x4001, 0x4105, 0x4205, 0x4301, 0x4405, 0x4501, 0x4601, 0x4705,
  0x480D, 0x4909, 0x5015, 0x5111, 0x5211, 0x5315, 0x5411, 0x5515,
  0x5005, 0x5101, 0x5201, 0x5305, 0x5401, 0x5505, 0x5605, 0x5701,
  0x5809, 0x590D, 0x6035, 0x6131, 0x6231, 0x6335, 0x6431, 0x6535,
  0x0604, 0x0700, 0x0808, 0x090C, 0x0A0C, 0x0B08, 0x0C0C, 0x0D08,
  0x0E08, 0x0F0C, 0x1010, 0x1114, 0x1214, 0x1310, 0x1414, 0x1510,
  0x1600, 0x1704, 0x180C, 0x1908, 0x1A08, 0x1B0C, 0x1C08, 0x1D0C,
  0x1E0C, 0x1F08, 0x2030, 0x2134, 0x2234, 0x2330, 0x2434, 0x2530,
  0x2620, 0x2724, 0x282C, 0x2928, 0x2A28, 0x2B2C, 0x2C28, 0x2D2C,
  0x2E2C, 0x2F28, 0x3034, 0x3130, 0x3230, 0x3334, 0x3430, 0x3534,
  0x3624, 0x3720, 0x3828, 0x392C, 0x3A2C, 0x3B28, 0x3C2C, 0x3D28,
  0x3E28, 0x3F2C, 0x4010, 0x4114, 0x4214, 0x4310, 0x4414, 0x4510,
  0x4600, 0x4704, 0x480C, 0x4908, 0x4A08, 0x4B0C, 0x4C08, 0x4D0C,
  0x4E0C, 0x4F08, 0x5014, 0x5110, 0x5210, 0x5314, 0x5410, 0x5514,
  0x5604, 0x5700, 0x5808, 0x590C, 0x5A0C, 0x5B08, 0x5C0C, 0x5D08,
  0x5E08, 0x5F0C, 0x6034, 0x6130, 0x6230, 0x6334, 0x6430, 0x6534,
  0x6624, 0x6720, 0x6828, 0x692C, 0x6A2C, 0x6B28, 0x6C2C, 0x6D28,
  0x6E28, 0x6F2C, 0x7030, 0x7134, 0x7234, 0x7330, 0x7434, 0x7530,
  0x7620, 0x7724, 0x782C, 0x7928, 0x7A28, 0x7B2C, 0x7C28, 0x7D2C,
  0x7E2C, 0x7F28, 0x8090, 0x8194, 0x8294, 0x8390, 0x8494, 0x8590,
  0x8680, 0x8784, 0x888C, 0x8988, 0x8A88, 0x8B8C, 0x8C88, 0x8D8C,
  0x8E8C, 0x8F88, 0x9094, 0x9190, 0x9290, 0x9394, 0x9490, 0x9594,
  0x9684, 0x9780, 0x9888, 0x998C, 0x9A8C, 0x9B88, 0x9C8C, 0x9D88,
  0x9E88, 0x9F8C, 0x0055, 0x0111, 0x0211, 0x0315, 0x0411, 0x0515,
  0x0605, 0x0701, 0x0809, 0x090D, 0x0A0D, 0x0B09, 0x0C0D, 0x0D09,
  0x0E09, 0x0F0D, 0x1011, 0x1115, 0x1215, 0x1311, 0x1415, 0x1511,
  0x1601, 0x1705, 0x180D, 0x1909, 0x1A09, 0x1B0D, 0x1C09, 0x1D0D,
  0x1E0D, 0x1F09, 0x2031, 0x2135, 0x2235, 0x2331, 0x2435, 0x2531,
  0x2621, 0x2725, 0x282D, 0x2929, 0x2A29, 0x2B2D, 0x2C29, 0x2D2D,
  0x2E2D, 0x2F29, 0x3035, 0x3131, 0x3231, 0x3335, 0x3431, 0x3535,
  0x3625, 0x3721, 0x3829, 0x392D, 0x3A2D, 0x3B29, 0x3C2D, 0x3D29,
  0x3E29, 0x3F2D, 0x4011, 0x4115, 0x4215, 0x4311, 0x4415, 0x4511,
  0x4601, 0x4705, 0x480D, 0x4909, 0x4A09, 0x4B0D, 0x4C09, 0x4D0D,
  0x4E0D, 0x4F09, 0x5015, 0x5111, 0x5211, 0x5315, 0x5411, 0x5515,
  0x5605, 0x5701, 0x5809, 0x590D, 0x5A0D, 0x5B09, 0x5C0D, 0x5D09,
  0x5E09, 0x5F0D, 0x6035, 0x6131, 0x6231, 0x6335, 0x6431, 0x6535,
  0x6625, 0x6721, 0x6829, 0x692D, 0x6A2D, 0x6B29, 0x6C2D, 0x6D29,
  0x6E29, 0x6F2D, 0x7031, 0x7135, 0x7235, 0x7331, 0x7435, 0x7531,
  0x7621, 0x7725, 0x782D, 0x7929, 0x7A29, 0x7B2D, 0x7C29, 0x7D2D,
  0x7E2D, 0x7F29, 0x8091, 0x8195, 0x8295, 0x8391, 0x8495, 0x8591,
  0x8681, 0x8785, 0x888D, 0x8989, 0x8A89, 0x8B8D, 0x8C89, 0x8D8D,
  0x8E8D, 0x8F89, 0x9095, 0x9191, 0x9291, 0x9395, 0x9491, 0x9595,
  0x9685, 0x9781, 0x9889, 0x998D, 0x9A8D, 0x9B89, 0x9C8D, 0x9D89,
  0x9E89, 0x9F8D, 0xA0B5, 0xA1B1, 0xA2B1, 0xA3B5, 0xA4B1, 0xA5B5,
  0xA6A5, 0xA7A1, 0xA8A9, 0xA9AD, 0xAAAD, 0xABA9, 0xACAD, 0xADA9,
  0xAEA9, 0xAFAD, 0xB0B1, 0xB1B5, 0xB2B5, 0xB3B1, 0xB4B5, 0xB5B1,
  0xB6A1, 0xB7A5, 0xB8AD, 0xB9A9, 0xBAA9, 0xBBAD, 0xBCA9, 0xBDAD,
  0xBEAD, 0xBFA9, 0xC095, 0xC191, 0xC291, 0xC395, 0xC491, 0xC595,
  0xC685, 0xC781, 0xC889, 0xC98D, 0xCA8D, 0xCB89, 0xCC8D, 0xCD89,
  0xCE89, 0xCF8D, 0xD091, 0xD195, 0xD295, 0xD391, 0xD495, 0xD591,
  0xD681, 0xD785, 0xD88D, 0xD989, 0xDA89, 0xDB8D, 0xDC89, 0xDD8D,
  0xDE8D, 0xDF89, 0xE0B1, 0xE1B5, 0xE2B5, 0xE3B1, 0xE4B5, 0xE5B1,
  0xE6A1, 0xE7A5, 0xE8AD, 0xE9A9, 0xEAA9, 0xEBAD, 0xECA9, 0xEDAD,
  0xEEAD, 0xEFA9, 0xF0B5, 0xF1B1, 0xF2B1, 0xF3B5, 0xF4B1, 0xF5B5,
  0xF6A5, 0xF7A1, 0xF8A9, 0xF9AD, 0xFAAD, 0xFBA9, 0xFCAD, 0xFDA9,
  0xFEA9, 0xFFAD, 0x0055, 0x0111, 0x0211, 0x0315, 0x0411, 0x0515,
  0x0605, 0x0701, 0x0809, 0x090D, 0x0A0D, 0x0B09, 0x0C0D, 0x0D09,
  0x0E09, 0x0F0D, 0x1011, 0x1115, 0x1215, 0x1311, 0x1415, 0x1511,
  0x1601, 0x1705, 0x180D, 0x1909, 0x1A09, 0x1B0D, 0x1C09, 0x1D0D,
  0x1E0D, 0x1F09, 0x2031, 0x2135, 0x2235, 0x2331, 0x2435, 0x2531,
  0x2621, 0x2725, 0x282D, 0x2929, 0x2A29, 0x2B2D, 0x2C29, 0x2D2D,
  0x2E2D, 0x2F29, 0x3035, 0x3131, 0x3231, 0x3335, 0x3431, 0x3535,
  0x3625, 0x3721, 0x3829, 0x392D, 0x3A2D, 0x3B29, 0x3C2D, 0x3D29,
  0x3E29, 0x3F2D, 0x4011, 0x4115, 0x4215, 0x4311, 0x4415, 0x4511,
  0x4601, 0x4705, 0x480D, 0x4909, 0x4A09, 0x4B0D, 0x4C09, 0x4D0D,
  0x4E0D, 0x4F09, 0x5015, 0x5111, 0x5211, 0x5315, 0x5411, 0x5515,
  0x5605, 0x5701, 0x5809, 0x590D, 0x5A0D, 0x5B09, 0x5C0D, 0x5D09,
  0x5E09, 0x5F0D, 0x6035, 0x6131, 0x6231, 0x6335, 0x6431, 0x6535,
  0x0046, 0x0102, 0x0202, 0x0306, 0x0402, 0x0506, 0x0606, 0x0702,
  0x080A, 0x090E, 0x0402, 0x0506, 0x0606, 0x0702, 0x080A, 0x090E,
  0x1002, 0x1106, 0x1206, 0x1302, 0x1406, 0x1502, 0x1602, 0x1706,
  0x180E, 0x190A, 0x1406, 0x1502, 0x1602, 0x1706, 0x180E, 0x190A,
  0x2022, 0x2126, 0x2226, 0x2322, 0x2426, 0x2522, 0x2622, 0x2726,
  0x282E, 0x292A, 0x2426, 0x2522, 0x2622, 0x2726, 0x282E, 0x292A,
  0x3026, 0x3122, 0x3222, 0x3326, 0x3422, 0x3526, 0x3626, 0x3722,
  0x382A, 0x392E, 0x3422, 0x3526, 0x3626, 0x3722, 0x382A, 0x392E,
  0x4002, 0x4106, 0x4206, 0x4302, 0x4406, 0x4502, 0x4602, 0x4706,
  0x480E, 0x490A, 0x4406, 0x4502, 0x4602, 0x4706, 0x480E, 0x490A,
  0x5006, 0x5102, 0x5202, 0x5306, 0x5402, 0x5506, 0x5606, 0x5702,
  0x580A, 0x590E, 0x5402, 0x5506, 0x5606, 0x5702, 0x580A, 0x590E,
  0x6026, 0x6122, 0x6222, 0x6326, 0x6422, 0x6526, 0x6626, 0x6722,
  0x682A, 0x692E, 0x6422, 0x6526, 0x6626, 0x6722, 0x682A, 0x692E,
  0x7022, 0x7126, 0x7226, 0x7322, 0x7426, 0x7522, 0x7622, 0x7726,
  0x782E, 0x792A, 0x7426, 0x7522, 0x7622, 0x7726, 0x782E, 0x792A,
  0x8082, 0x8186, 0x8286, 0x8382, 0x8486, 0x8582, 0x8682, 0x8786,
  0x888E, 0x898A, 0x8486, 0x8582, 0x8682, 0x8786, 0x888E, 0x898A,
  0x9086, 0x9182, 0x9282, 0x9386, 0x9482, 0x9586, 0x9686, 0x9782,
  0x988A, 0x998E, 0x3423, 0x3527, 0x3627, 0x3723, 0x382B, 0x392F,
  0x4003, 0x4107, 0x4207, 0x4303, 0x4407, 0x4503, 0x4603, 0x4707,
  0x480F, 0x490B, 0x4407, 0x4503, 0x4603, 0x4707, 0x480F, 0x490B,
  0x5007, 0x5103, 0x5203, 0x5307, 0x5403, 0x5507, 0x5607, 0x5703,
  0x580B, 0x590F, 0x5403, 0x5507, 0x5607, 0x5703, 0x580B, 0x590F,
  0x6027, 0x6123, 0x6223, 0x6327, 0x6423, 0x6527, 0x6627, 0x6723,
  0x682B, 0x692F, 0x6423, 0x6527, 0x6627, 0x6723, 0x682B, 0x692F,
  0x7023, 0x7127, 0x7227, 0x7323, 0x7427, 0x7523, 0x7623, 0x7727,
  0x782F, 0x792B, 0x7427, 0x7523, 0x7623, 0x7727, 0x782F, 0x792B,
  0x8083, 0x8187, 0x8287, 0x8383, 0x8487, 0x8583, 0x8683, 0x8787,
  0x888F, 0x898B, 0x8487, 0x8583, 0x8683, 0x8787, 0x888F, 0x898B,
  0x9087, 0x9183, 0x9283, 0x9387, 0x9483, 0x9587, 0x9687, 0x9783,
  0x988B, 0x998F, 0x9483, 0x9587, 0x9687, 0x9783, 0x988B, 0x998F,
  0xA0A7, 0xA1A3, 0xA2A3, 0xA3A7, 0xA4A3, 0xA5A7, 0xA6A7, 0xA7A3,
  0xA8AB, 0xA9AF, 0xA4A3, 0xA5A7, 0xA6A7, 0xA7A3, 0xA8AB, 0xA9AF,
  0xB0A3, 0xB1A7, 0xB2A7, 0xB3A3, 0xB4A7, 0xB5A3, 0xB6A3, 0xB7A7,
  0xB8AF, 0xB9AB, 0xB4A7, 0xB5A3, 0xB6A3, 0xB7A7, 0xB8AF, 0xB9AB,
  0xC087, 0xC183, 0xC283, 0xC387, 0xC483, 0xC587, 0xC687, 0xC783,
  0xC88B, 0xC98F, 0xC483, 0xC587, 0xC687, 0xC783, 0xC88B, 0xC98F,
  0xD083, 0xD187, 0xD287, 0xD383, 0xD487, 0xD583, 0xD683, 0xD787,
  0xD88F, 0xD98B, 0xD487, 0xD583, 0xD683, 0xD787, 0xD88F, 0xD98B,
  0xE0A3, 0xE1A7, 0xE2A7, 0xE3A3, 0xE4A7, 0xE5A3, 0xE6A3, 0xE7A7,
  0xE8AF, 0xE9AB, 0xE4A7, 0xE5A3, 0xE6A3, 0xE7A7, 0xE8AF, 0xE9AB,
  0xF0A7, 0xF1A3, 0xF2A3, 0xF3A7, 0xF4A3, 0xF5A7, 0xF6A7, 0xF7A3,
  0xF8AB, 0xF9AF, 0xF4A3, 0xF5A7, 0xF6A7, 0xF7A3, 0xF8AB, 0xF9AF,
  0x0047, 0x0103, 0x0203, 0x0307, 0x0403, 0x0507, 0x0607, 0x0703,
  0x080B, 0x090F, 0x0403, 0x0507, 0x0607, 0x0703, 0x080B, 0x090F,
  0x1003, 0x1107, 0x1207, 0x1303, 0x1407, 0x1503, 0x1603, 0x1707,
  0x180F, 0x190B, 0x1407, 0x1503, 0x1603, 0x1707, 0x180F, 0x190B,
  0x2023, 0x2127, 0x2227, 0x2323, 0x2427, 0x2523, 0x2623, 0x2727,
  0x282F, 0x292B, 0x2427, 0x2523, 0x2623, 0x2727, 0x282F, 0x292B,
  0x3027, 0x3123, 0x3223, 0x3327, 0x3423, 0x3527, 0x3627, 0x3723,
  0x382B, 0x392F, 0x3423, 0x3527, 0x3627, 0x3723, 0x382B, 0x392F,
  0x4003, 0x4107, 0x4207, 0x4303, 0x4407, 0x4503, 0x4603, 0x4707,
  0x480F, 0x490B, 0x4407, 0x4503, 0x4603, 0x4707, 0x480F, 0x490B,
  0x5007, 0x5103, 0x5203, 0x5307, 0x5403, 0x5507, 0x5607, 0x5703,
  0x580B, 0x590F, 0x5403, 0x5507, 0x5607, 0x5703, 0x580B, 0x590F,
  0x6027, 0x6123, 0x6223, 0x6327, 0x6423, 0x6527, 0x6627, 0x6723,
  0x682B, 0x692F, 0x6423, 0x6527, 0x6627, 0x6723, 0x682B, 0x692F,
  0x7023, 0x7127, 0x7227, 0x7323, 0x7427, 0x7523, 0x7623, 0x7727,
  0x782F, 0x792B, 0x7427, 0x7523, 0x7623, 0x7727, 0x782F, 0x792B,
  0x8083, 0x8187, 0x8287, 0x8383, 0x8487, 0x8583, 0x8683, 0x8787,
  0x888F, 0x898B, 0x8487, 0x8583, 0x8683, 0x8787, 0x888F, 0x898B,
  0x9087, 0x9183, 0x9283, 0x9387, 0x9483, 0x9587, 0x9687, 0x9783,
  0x988B, 0x998F, 0x9483, 0x9587, 0x9687, 0x9783, 0x988B, 0x998F,
  0xFABE, 0xFBBA, 0xFCBE, 0xFDBA, 0xFEBA, 0xFFBE, 0x0046, 0x0102,
  0x0202, 0x0306, 0x0402, 0x0506, 0x0606, 0x0702, 0x080A, 0x090E,
  0x0A1E, 0x0B1A, 0x0C1E, 0x0D1A, 0x0E1A, 0x0F1E, 0x1002, 0x1106,
  0x1206, 0x1302, 0x1406, 0x1502, 0x1602, 0x1706, 0x180E, 0x190A,
  0x1A1A, 0x1B1E, 0x1C1A, 0x1D1E, 0x1E1E, 0x1F1A, 0x2022, 0x2126,
  0x2226, 0x2322, 0x2426, 0x2522, 0x2622, 0x2726, 0x282E, 0x292A,
  0x2A3A, 0x2B3E, 0x2C3A, 0x2D3E, 0x2E3E, 0x2F3A, 0x3026, 0x3122,
  0x3222, 0x3326, 0x3422, 0x3526, 0x3626, 0x3722, 0x382A, 0x392E,
  0x3A3E, 0x3B3A, 0x3C3E, 0x3D3A, 0x3E3A, 0x3F3E, 0x4002, 0x4106,
  0x4206, 0x4302, 0x4406, 0x4502, 0x4602, 0x4706, 0x480E, 0x490A,
  0x4A1A, 0x4B1E, 0x4C1A, 0x4D1E, 0x4E1E, 0x4F1A, 0x5006, 0x5102,
  0x5202, 0x5306, 0x5402, 0x5506, 0x5606, 0x5702, 0x580A, 0x590E,
  0x5A1E, 0x5B1A, 0x5C1E, 0x5D1A, 0x5E1A, 0x5F1E, 0x6026, 0x6122,
  0x6222, 0x6326, 0x6422, 0x6526, 0x6626, 0x6722, 0x682A, 0x692E,
  0x6A3E, 0x6B3A, 0x6C3E, 0x6D3A, 0x6E3A, 0x6F3E, 0x7022, 0x7126,
  0x7226, 0x7322, 0x7426, 0x7522, 0x7622, 0x7726, 0x782E, 0x792A,
  0x7A3A, 0x7B3E, 0x7C3A, 0x7D3E, 0x7E3E, 0x7F3A, 0x8082, 0x8186,
  0x8286, 0x8382, 0x8486, 0x8582, 0x8682, 0x8786, 0x888E, 0x898A,
  0x8A9A, 0x8B9E, 0x8C9A, 0x8D9E, 0x8E9E, 0x8F9A, 0x9086, 0x9182,
  0x9282, 0x9386, 0x3423, 0x3527, 0x3627, 0x3723, 0x382B, 0x392F,
  0x3A3F, 0x3B3B, 0x3C3F, 0x3D3B, 0x3E3B, 0x3F3F, 0x4003, 0x4107,
  0x4207, 0x4303, 0x4407, 0x4503, 0x4603, 0x4707, 0x480F, 0x490B,
  0x4A1B, 0x4B1F, 0x4C1B, 0x4D1F, 0x4E1F, 0x4F1B, 0x5007, 0x5103,
  0x5203, 0x5307, 0x5403, 0x5507, 0x5607, 0x5703, 0x580B, 0x590F,
  0x5A1F, 0x5B1B, 0x5C1F, 0x5D1B, 0x5E1B, 0x5F1F, 0x6027, 0x6123,
  0x6223, 0x6327, 0x6423, 0x6527, 0x6627, 0x6723, 0x682B, 0x692F,
  0x6A3F, 0x6B3B, 0x6C3F, 0x6D3B, 0x6E3B, 0x6F3F, 0x7023, 0x7127,
  0x7227, 0x7323, 0x7427, 0x7523, 0x7623, 0x7727, 0x782F, 0x792B,
  0x7A3B, 0x7B3F, 0x7C3B, 0x7D3F, 0x7E3F, 0x7F3B, 0x8083, 0x8187,
  0x8287, 0x8383, 0x8487, 0x8583, 0x8683, 0x8787, 0x888F, 0x898B,
  0x8A9B, 0x8B9F, 0x8C9B, 0x8D9F, 0x8E9F, 0x8F9B, 0x9087, 0x9183,
  0x9283, 0x9387, 0x9483, 0x9587, 0x9687, 0x9783, 0x988B, 0x998F,
  0x9A9F, 0x9B9B, 0x9C9F, 0x9D9B, 0x9E9B, 0x9F9F, 0xA0A7, 0xA1A3,
  0xA2A3, 0xA3A7, 0xA4A3, 0xA5A7, 0xA6A7, 0xA7A3, 0xA8AB, 0xA9AF,
  0xAABF, 0xABBB, 0xACBF, 0xADBB, 0xAEBB, 0xAFBF, 0xB0A3, 0xB1A7,
  0xB2A7, 0xB3A3, 0xB4A7, 0xB5A3, 0xB6A3, 0xB7A7, 0xB8AF, 0xB9AB,
  0xBABB, 0xBBBF, 0xBCBB, 0xBDBF, 0xBEBF, 0xBFBB, 0xC087, 0xC183,
  0xC283, 0xC387, 0xC483, 0xC587, 0xC687, 0xC783, 0xC88B, 0xC98F,
  0xCA9F, 0xCB9B, 0xCC9F, 0xCD9B, 0xCE9B, 0xCF9F, 0xD083, 0xD187,
  0xD287, 0xD383, 0xD487, 0xD583, 0xD683, 0xD787, 0xD88F, 0xD98B,
  0xDA9B, 0xDB9F, 0xDC9B, 0xDD9F, 0xDE9F, 0xDF9B, 0xE0A3, 0xE1A7,
  0xE2A7, 0xE3A3, 0xE4A7, 0xE5A3, 0xE6A3, 0xE7A7, 0xE8AF, 0xE9AB,
  0xEABB, 0xEBBF, 0xECBB, 0xEDBF, 0xEEBF, 0xEFBB, 0xF0A7, 0xF1A3,
  0xF2A3, 0xF3A7, 0xF4A3, 0xF5A7, 0xF6A7, 0xF7A3, 0xF8AB, 0xF9AF,
  0xFABF, 0xFBBB, 0xFCBF, 0xFDBB, 0xFEBB, 0xFFBF, 0x0047, 0x0103,
  0x0203, 0x0307, 0x0403, 0x0507, 0x0607, 0x0703, 0x080B, 0x090F,
  0x0A1F, 0x0B1B, 0x0C1F, 0x0D1B, 0x0E1B, 0x0F1F, 0x1003, 0x1107,
  0x1207, 0x1303, 0x1407, 0x1503, 0x1603, 0x1707, 0x180F, 0x190B,
  0x1A1B, 0x1B1F, 0x1C1B, 0x1D1F, 0x1E1F, 0x1F1B, 0x2023, 0x2127,
  0x2227, 0x2323, 0x2427, 0x2523, 0x2623, 0x2727, 0x282F, 0x292B,
  0x2A3B, 0x2B3F, 0x2C3B, 0x2D3F, 0x2E3F, 0x2F3B, 0x3027, 0x3123,
  0x3223, 0x3327, 0x3423, 0x3527, 0x3627, 0x3723, 0x382B, 0x392F,
  0x3A3F, 0x3B3B, 0x3C3F, 0x3D3B, 0x3E3B, 0x3F3F, 0x4003, 0x4107,
  0x4207, 0x4303, 0x4407, 0x4503, 0x4603, 0x4707, 0x480F, 0x490B,
  0x4A1B, 0x4B1F, 0x4C1B, 0x4D1F, 0x4E1F, 0x4F1B, 0x5007, 0x5103,
  0x5203, 0x5307, 0x5403, 0x5507, 0x5607, 0x5703, 0x580B, 0x590F,
  0x5A1F, 0x5B1B, 0x5C1F, 0x5D1B, 0x5E1B, 0x5F1F, 0x6027, 0x6123,
  0x6223, 0x6327, 0x6423, 0x6527, 0x6627, 0x6723, 0x682B, 0x692F,
  0x6A3F, 0x6B3B, 0x6C3F, 0x6D3B, 0x6E3B, 0x6F3F, 0x7023, 0x7127,
  0x7227, 0x7323, 0x7427, 0x7523, 0x7623, 0x7727, 0x782F, 0x792B,
  0x7A3B, 0x7B3F, 0x7C3B, 0x7D3F, 0x7E3F, 0x7F3B, 0x8083, 0x8187,
  0x8287, 0x8383, 0x8487, 0x8583, 0x8683, 0x8787, 0x888F, 0x898B,
  0x8A9B, 0x8B9F, 0x8C9B, 0x8D9F, 0x8E9F, 0x8F9B, 0x9087, 0x9183,
  0x9283, 0x9387, 0x9483, 0x9587, 0x9687, 0x9783, 0x988B, 0x998F
};

}


void z80::Reset()
{
    lyraLogInfo("CPU: z80 Reset");

    mPC = 0x0000;
    mSP = 0xDFF0;
    mInterruptFlipFlop1 = false;
    mInterruptFlipFlop2 = false;
    mIM = 0;

    mIX.reg = 0x0;
    mIY.reg = 0x0;

    AF = 0;
    BC = 0;
    DE = 0;
    HL = 0;

    mAFShadow.reg = 0;
    mBCShadow.reg = 0;
    mDEShadow.reg = 0;
    mHLShadow.reg = 0;

    mR = 0;	
    mI = 0;

    mIM = 0;


    mHalted = false;

    
}

Byte z80::FetchAndExecuteInstruction()
{
    mInstructionCycles = 0;

    if (mHalted)
        return 4;

    IncreaseR();

    bool opExecuted = false;
    Byte opcode = 0;

    if (!mHalted)
    {
        opcode = mMemory->READ(mPC);

        mPC++;
        switch (opcode)
        {
		    case 0xCB: FetchAndExecuteCBInstruction(); break;
		    case 0xDD: FetchAndExecuteDDInstruction(); break;
		    case 0xED: FetchAndExecuteEDInstruction(); break;
		    case 0xFD: FetchAndExecuteFDInstruction(); break;
			default: 
                opExecuted = ExecuteOpcode(opcode); 		
                if (opExecuted) mInstructionCycles = opCycles[opcode];
				   else if (mInstructionCycles != 0) opExecuted = true;
				   else {
				        // Function not implemented
					    lyraLogError("CPU: Instruction 0x%02x cycle count not implemented", (unsigned char)opcode);
			       }
				   break;
        }

        
    }

    if (mEnableInterruptRequested && opcode != 0xFB)
    {
        mEnableInterruptRequested = false;
        mInterruptFlipFlop1 = true;
        mInterruptFlipFlop2 = true;
    }

    return mInstructionCycles;
}

void z80::RequestInterrupt(Word routineAddress)
{
	if (mInterruptFlipFlop1 && mIM == 1)
	{
		if (mHalted)
		{
			mHalted = false;
			mPC++;
		}
		mInterruptFlipFlop1 = false;
		mInterruptFlipFlop2 = false;
		CALL(routineAddress);
	}
}

bool z80::FetchAndExecuteCBInstruction()
{
    IncreaseR();
    // PC points to next instruction
    Byte opcode = mMemory->READ(mPC);

    mPC++;

    bool opExecuted = ExecuteCBOpcode(opcode);
	if (opExecuted) mInstructionCycles = opCyclesCB[opcode];
	else if (mInstructionCycles != 0) opExecuted = true;
    else
    {
        // Function not implemented
        lyraLogError("CPU: Instruction 0xCB%02x cycle count not implemented", (unsigned char)opcode);
    }


	return opExecuted;
}

bool z80::FetchAndExecuteDDInstruction()
{
    IncreaseR();
    // PC points to next instruction
    Byte opcode = ReadNextByte();

    // Special after special?
    if (opcode == 0xFD || opcode == 0xDD || opcode == 0xED)
        __debugbreak();

    mDDInstruction = true;
    bool opExecuted = false;
    switch (opcode)
    {
	    case 0xCB: opExecuted = FetchAndExecuteDDFDCBInstruction(); break;
        default: 
        {
			opExecuted = ExecuteDDOpcode(opcode);

			if (opExecuted) mInstructionCycles = opCyclesDDFD[opcode];
			else if (mInstructionCycles != 0) opExecuted = true;
			else
			{
			    
				// Function not implemented
			    lyraLogError("CPU: Instruction 0xDD%02x cycle count not implemented", (unsigned char)opcode);
			}
            if(opExecuted && mInstructionCycles == 0)
            {
                mInstructionCycles = opCycles[opcode];
            }
        }
    }

    mDDInstruction = false;

	return opExecuted;
}

bool z80::FetchAndExecuteEDInstruction()
{
    IncreaseR();
    // PC points to next instruction
    Byte opcode = ReadNextByte();

    // Special after special?
    if (opcode == 0xFD || opcode == 0xDD || opcode == 0xED || opcode == 0xCB)
        __debugbreak();

    bool opExecuted = ExecuteEDOpcode(opcode);

    if (opExecuted) mInstructionCycles = opCyclesED[opcode];
    else if (mInstructionCycles != 0) opExecuted = true;
    else
	{
		// Function not implemented
		lyraLogError("CPU: Instruction 0xED%02x cycle count not implemented", (unsigned char)opcode);
	}

    return opExecuted;
}

bool z80::FetchAndExecuteFDInstruction()
{
    IncreaseR();
    // PC points to next instruction
    Byte opcode = ReadNextByte();

    // Special after special?
    if (opcode == 0xFD || opcode == 0xDD || opcode == 0xED)
        __debugbreak();

    mFDInstruction = true;
    bool opExecuted = false;
    switch (opcode)
    {
        case 0xCB: opExecuted = FetchAndExecuteDDFDCBInstruction(); break;
        default:
        {
			opExecuted = ExecuteFDOpcode(opcode);

			if (opExecuted) mInstructionCycles = opCyclesDDFD[opcode];
			else if (mInstructionCycles != 0) opExecuted = true;
			else
			{
				// Function not implemented
    			lyraLogError("CPU: Instruction 0xFD%02x cycle count not implemented", (unsigned char)opcode);
			}
            if (opExecuted && mInstructionCycles == 0)
            {
                mInstructionCycles = opCycles[opcode];
            }
        }
        break;
    }

	mFDInstruction = false;

    return opExecuted;
}

bool z80::FetchAndExecuteDDFDCBInstruction()
{
    SByte data = ReadNextSByte();
    Byte opcode = ReadNextByte();

    bool opExecuted = ExecuteDDFDBCOpcode(opcode, data);
	if (opExecuted) mInstructionCycles = opCyclesDDFDCB[opcode];
	else if (mInstructionCycles != 0) opExecuted = true;
    else
    {
        // Function not implemented
        if (mDDInstruction)
            lyraLogError("CPU: Instruction DD CB XX %02X cycle count not implemented", (unsigned char)opcode);
        if (mFDInstruction)
            lyraLogError("CPU: Instruction FD CB XX %02X cycle count not implemented", (unsigned char)opcode);
    }

	return opExecuted;
}


void z80::WriteByte(Word address, Byte byte)
{
    mMemory->WRITE(address, byte);
}

void z80::WriteWord(Word address, Word word)
{
    Byte hi = word >> 8;
    Byte lo = word & 0xFF;
    mMemory->WRITE(address + 1, hi);
    mMemory->WRITE(address, lo);
}

Byte z80::ReadByte(Word address)
{
    return mMemory->READ(address);
}

SByte z80::ReadSByte(Word address)
{
    auto byte = ReadByte(address);
    return static_cast<SByte>(byte);
}


void z80::WriteIOByte(Byte address, Byte byte)
{
    mIO->WRITE(address, byte);
}

Byte z80::ReadIOByte(Byte address)
{
    Byte a = mIO->READ(address);
    return a;
}

Word z80::ReadWord(Word address)
{

    Word result = ReadByte(address) & 0x00ff;
    result |= ReadByte(address + 1) << 8;

    return result;
}

Byte z80::ReadNextByte()
{
    return ReadByte(mPC++);
}

SByte z80::ReadNextSByte()
{
    return ReadSByte(mPC++);
}

Word z80::ReadNextWord()
{

    Word result = ReadWord(mPC);
    mPC += 2;
    return result;
}

void z80::Push(Word val)
{
    Byte hi = val >> 8;
    Byte lo = val & 0xFF;
    mSP--;
    mMemory->WRITE(mSP, hi);
    mSP--;
    mMemory->WRITE(mSP, lo);

}

Word z80::Pop()
{
    Word val = ReadWord(mSP);

    mSP += 2;

    return val;
}

void z80::IncreaseR()
{
    if ((mR & 127) == 127)
        mR = mR & 128;
    else
        mR++;
}

void z80::CPURestart(Byte newPC)
{
    Push(mPC);
    mPC = newPC;
}

void z80::CalculateTables()
{
    // Mame tables
    for (int i = 0; i < 256; i++)
    {
        int p = 0;
        if (i & 0x01) ++p;
        if (i & 0x02) ++p;
        if (i & 0x04) ++p;
        if (i & 0x08) ++p;
        if (i & 0x10) ++p;
        if (i & 0x20) ++p;
        if (i & 0x40) ++p;
        if (i & 0x80) ++p;
        mBitsSignZero[i] = i ? i & SF : ZF;
        mBitsSignParityZero_BIT[i] = i ? i & SF : ZF | PF;

        mBitsSignParityZero[i] = mBitsSignZero[i] | ((p & 1) ? 0 : PF);

        mBitsSignHalfOverflowZero_INC[i] = mBitsSignZero[i];
        if (i == 0x80) mBitsSignHalfOverflowZero_INC[i] |= VF;
        if (!(i & 0x0f)) mBitsSignHalfOverflowZero_INC[i] |= HF;
        mBitsSignHalfOverflowZero_DEC[i] = mBitsSignZero[i] | NF;
        if (i == 0x7f) mBitsSignHalfOverflowZero_DEC[i] |= VF;
        if ((i & 0x0f) == 0x0f) mBitsSignHalfOverflowZero_DEC[i] |= HF;
    }

    Byte *padd = &mBitsSignHalfOverflowZero_ADD[0 * 256];
    Byte *padc = &mBitsSignHalfOverflowZero_ADD[256 * 256];
    Byte *psub = &mBitsSignHalfOverflowZero_SUB[0 * 256];
    Byte *psbc = &mBitsSignHalfOverflowZero_SUB[256 * 256];
    for (int oldval = 0; oldval < 256; oldval++)
    {
        for (int newval = 0; newval < 256; newval++)
        {
            /* add or adc w/o carry set */
            int val = newval - oldval;
            *padd = (newval) ? ((newval & 0x80) ? SF : 0) : ZF;
            if ((newval & 0x0f) < (oldval & 0x0f)) *padd |= HF;
            if (newval < oldval) *padd |= CF;
            if ((val^oldval ^ 0x80) & (val^newval) & 0x80) *padd |= VF;
            padd++;

            /* adc with carry set */
            val = newval - oldval - 1;
            *padc = (newval) ? ((newval & 0x80) ? SF : 0) : ZF;
            if ((newval & 0x0f) <= (oldval & 0x0f)) *padc |= HF;
            if (newval <= oldval) *padc |= CF;
            if ((val^oldval ^ 0x80) & (val^newval) & 0x80) *padc |= VF;
            padc++;

            /* cp, sub or sbc w/o carry set */
            val = oldval - newval;
            *psub = NF | ((newval) ? ((newval & 0x80) ? SF : 0) : ZF);
            if ((newval & 0x0f) > (oldval & 0x0f)) *psub |= HF;
            if (newval > oldval) *psub |= CF;
            if ((val^oldval) & (oldval^newval) & 0x80) *psub |= VF;
            psub++;

            /* sbc with carry set */
            val = oldval - newval - 1;
            *psbc = NF | ((newval) ? ((newval & 0x80) ? SF : 0) : ZF);
            if ((newval & 0x0f) >= (oldval & 0x0f)) *psbc |= HF;
            if (newval >= oldval) *psbc |= CF;
            if ((val^oldval) & (oldval^newval) & 0x80) *psbc |= VF;
            psbc++;
        }
    }
}

void z80::TestBit8Bit(Byte byte, Byte bit)
{
    F = (F & CF) | HF | mBitsSignParityZero[byte & (1 << bit)];
}

void z80::ResetBit8Bit(Byte & byte, Byte bit)
{
    byte = ResetBit(byte, bit);
}

void z80::SetBit8Bit(Byte & byte, Byte bit)
{
    byte = SetBit(byte, bit);
}

void z80::Srl8Bit(Byte & byte)
{
    F = byte & 0x01; 
    byte >>= 1; 
    F |= mBitsSignParityZero[byte];
}

void z80::Sra8Bit(Byte & byte)
{
    F = byte & CF;
    byte = (byte >> 1) | (byte & 0x80); 
    F |= mBitsSignParityZero[byte];
}

void z80::Sll8Bit(Byte & byte)
{
    F = byte >> 7; 
    byte = (byte << 1) | 0x01; 
    F |= mBitsSignParityZero[byte];
}

void z80::Sla8Bit(Byte & byte)
{
    F = byte >> 7;
    byte <<= 1;
    F |= mBitsSignParityZero[byte];

}

void z80::Rr8Bit(Byte & byte)
{
	if (byte & 0x01)          
	{                    
		byte = (byte >> 1) | (F << 7);     
		F = mBitsSignParityZero[byte] | CF; 
	}                    
	else                 
	{                    
		byte = (byte >> 1) | (F << 7);     
		F = mBitsSignParityZero[byte];        
	}
}

void z80::Rrc8Bit(Byte & byte)
{
    F = byte & 0x01;
    byte = (byte >> 1) | (F << 7); 
    F |= mBitsSignParityZero[byte];
}

void z80::Rl8Bit(Byte & byte)
{
	if (byte & 0x80)
	{                    
		byte = (byte << 1) | (F & CF); 
		F = mBitsSignParityZero[byte] | CF; 
	}                    
	else                 
	{                    
		byte = (byte << 1) | (F & CF); 
		F = mBitsSignParityZero[byte];        
	}
}

void z80::Rlc8Bit(Byte & byte)
{
    F = byte >> 7; 
    byte = (byte << 1) | F; 
    F |= mBitsSignParityZero[byte];
}

void z80::Rlca8Bit()
{
	Byte c = (A & 0x80) ? CF : 0;
    A = (A << 1) | c;
    F = (F & ~(CF | NF | HF)) | c;
}

void z80::Rla8Bit()
{
	Byte c = (A & 0x80) ? CF : 0;
	A = (A << 1) | (F & CF);
	F = (F & ~(CF | NF | HF)) | c;
}

void z80::Rrca8Bit()
{
    Byte c = A & 0x01;
	A = (A >> 1) | (c ? 0x80 : 0);
	F = (F & ~(CF | NF | HF)) | c;
}

void z80::Rra8Bit()
{
	Byte c = A & 0x01;
	A = (A >> 1) | (F & CF ? 0x80 : 0);
	F = (F & ~(CF | NF | HF)) | c;
}

void z80::Djnz()
{
    B--; 
    if (B == 0)
	{
        mInstructionCycles = 8;
        mPC++;
		return;
	}
	SByte r = ReadNextSByte();
    mInstructionCycles = 13;
    mPC += r;
}

void z80::Daa()
{
	int i = A;

	if (F & CF)
		i |= 0x100;
	if (F & HF)
		i |= 0x200;
	if (F & NF)
		i |= 0x400;

    AF = DAATable[i];
}

Byte z80::Cpi()
{

    Byte val = ReadByte(HL);
    Byte res = A - val;
    HL++; BC--;
	F =
		NF | (F & CF) | mBitsSignZero[res] |
		((A ^ val ^ res) & HF) | (BC ? PF : 0);

    return res;
}

void z80::Cpir()
{
	Byte res = Cpi(); 
    if (BC != 0 && res) 
    { 
        mPC -= 2; 
        mInstructionCycles = 21; 
    }
	else 
    { 
        mInstructionCycles = 16; 
    }
}

Byte z80::Cpd()
{
    Byte val = ReadByte(HL);
    Byte res = A - val;
    HL--; BC--;
	F =
		NF | (F & CF) | mBitsSignZero[res] |
		((A ^ val ^ res) & HF) | (BC ? PF : 0);
    return res;
}

void z80::Ldi()
{
    Byte io = ReadByte(HL);
    WriteByte(DE, io);

    Inc16Bit(DE);
    Inc16Bit(HL);
    Dec16Bit(BC);

    F = (F & ~(NF | HF | PF)) | (BC ? PF : 0);
}

void z80::Ldir()
{
	Ldi();	
    if (BC != 0) 
    { 
        //F |= NF;
        mPC -= 2; 
        mInstructionCycles = 21; 
    }
	else 
    { 
        F &= ~(PF); 
        mInstructionCycles = 16; 
    }
}

void z80::Ldd()
{
    Byte io = ReadByte(HL);
    WriteByte(DE, io);

	Dec16Bit(DE);
	Dec16Bit(HL);
	Dec16Bit(BC);

	F = (F & ~(NF | HF | PF)) | (BC ? PF : 0);
}

void z80::Lddr()
{
	Ldd(); 
    if (BC != 0) { mPC -= 2; mInstructionCycles = 21; }
	else { mInstructionCycles = 16; F &= ~(NF | HF | PF); }
}

void z80::Cpdr()
{
    Byte res = Cpd(); 
    if (BC != 0 && res) { mPC -= 2; mInstructionCycles = 21; }
    else { mInstructionCycles = 16; }
}

void z80::Rrd()
{
    Byte n = ReadByte(HL);
    WriteByte(HL, (n >> 4) | (A << 4));
    A = (A & 0xf0) | (n & 0x0f);
    F = (F & CF) | mBitsSignParityZero[A];
}

void z80::Rld()
{
    Byte n = ReadByte(HL);
    WriteByte(HL, (n << 4) | (A & 0x0f));
    A = (A & 0xf0) | (n >> 4);
    F = (F & CF) | mBitsSignParityZero[A];
}

void z80::Outi()
{
/*
    unsigned t;
    Byte io = ReadByte(HL);
    B--;
    WriteIOByte(C, io);
    HL++;
    F = mBitsSignZero[B];
    t = (unsigned)L + (unsigned)io;
    if (io & SF) F |= NF;
    if (t & 0x100) F |= HF | CF;
    F |= mBitsSignParityZero[(Byte)(t & 0x07) ^ B] & PF;*/

	Byte io = ReadByte(HL);
	WriteIOByte(C, io);

    HL++;
	B--;
	F = NF | (B ? 0 : ZF) | ((int)L + io > 255 ? (CF | HF) : 0);
}

void z80::Otir()
{
	Outi();
	if (B != 0) 
    { 
        mPC -= 2; 
        mInstructionCycles = 21; 
    }
	else 
    { 
        ResetFlag(FlagRegisterBits::SIGN);
        SetFlag(FlagRegisterBits::ZERO);
        mInstructionCycles = 16;
    }
}


Byte z80::In(Byte address)
{
    Byte reg = ReadIOByte(address);
    F = mBitsSignParityZero[reg] | (F & CF);
    return reg;
}

void z80::Ini()
{
	uint8_t io = ReadIOByte(C);
	WriteByte(HL, io);

    HL++;
    B--;
    F = NF | (B ? 0 : ZF);
}

void z80::Outd()
{
    Byte out = ReadByte(HL);
	WriteIOByte(C, out);
	Dec16Bit(HL);
    B--;

    F = NF | (B ? 0 : ZF) | ((int)L + out > 255 ? (CF | HF) : 0);
}

void z80::Ldai()
{
    A = mI;

    F = mBitsSignZero[A] | (F & CF);
    PutFlag(FlagRegisterBits::PARITY_OVERFLOW, mInterruptFlipFlop2);
}

void z80::Ldar()
{
    A = mR;

    F = mBitsSignZero[A] | (F & CF);
    PutFlag(FlagRegisterBits::PARITY_OVERFLOW, mInterruptFlipFlop2);
}

void z80::Inc8Bit(Byte& reg)
{
	reg++;

	F &= CF;
	F |= mBitsSignHalfOverflowZero_INC[reg];
}

void z80::Dec8Bit(Byte & reg)
{
    reg--;

    F &= CF;
    F |= mBitsSignHalfOverflowZero_DEC[reg];
}

void z80::Inc16Bit(Word & reg)
{
    reg++;
}

void z80::Dec16Bit(Word & reg)
{
    reg--;
}

void z80::Add8Bit(Byte value)
{
    uint32_t ah = AF & 0xff00;
    uint32_t res = (Byte)((ah >> 8) + value);
    F = mBitsSignHalfOverflowZero_ADD[ah | res];
    A = res;
}

void z80::Adca8Bit(Byte value)
{
    uint32_t ah = AF & 0xff00, c = AF & 1;
    uint32_t res = (Byte)((ah >> 8) + value + c);
    F = mBitsSignHalfOverflowZero_ADD[(c << 16) | ah | res];
    A = res;
}

void z80::Add16Bit(Word & dst, Word value)
{
    uint32_t res = dst + value;
    F = (F & (SF | ZF | VF)) |
        (((dst ^ res ^ value) >> 8) & HF) |
        ((res >> 16) & CF);
    dst = (uint16_t)res;
}

void z80::AdcHL16Bit(Word value)
{
    uint32_t res = HL + value + (F & CF);
    F = (((HL ^ res ^ value) >> 8) & HF) |
        ((res >> 16) & CF) |
        ((res >> 8) & SF) |
        ((res & 0xffff) ? 0 : ZF) |
        (((value ^ HL ^ 0x8000) & (value ^ res) & 0x8000) >> 13);
    HL = (uint16_t)res;
}

void z80::Sub8Bit(Byte value)
{
    uint32_t ah = AF & 0xff00;
    uint32_t res = (Byte)(A - value);
    F = mBitsSignHalfOverflowZero_SUB[ah | res];
    A = res;
}

void z80::Sbc8Bit(Byte value)
{

    uint32_t ah = AF & 0xff00;
    uint32_t c = F & CF;
    uint32_t res = (Byte)((A) - value - c);
    Byte newF = mBitsSignHalfOverflowZero_SUB[(c << 16) | ah | res];
    Byte newA = res;

    Register j;
	j.reg = A - value - (F & CF); 
	F = 
		((A ^ value) & (A ^ j.lo) & 0x80 ? VF : 0) | 
		NF | -j.hi | mBitsSignZero[j.lo] | 
		((A ^ value ^ j.lo) & HF);     
    A = j.lo;

    if (F != newF || A != newA)
    {
        __debugbreak();
    }
}

void z80::SbcHL16Bit(Word value)
{
    uint32_t res = HL - value - (F & CF);
    F = (((HL ^ res ^ value) >> 8) & HF) | NF |
        ((res >> 16) & CF) |
        ((res >> 8) & SF) |
        ((res & 0xffff) ? 0 : ZF) |
        (((value ^ HL) & (HL ^ res) & 0x8000) >> 13);
    HL = (uint16_t)res;
}

void z80::Cmp8Bit(Byte reg, Byte value)
{
    unsigned val = value;
    uint32_t ah = AF & 0xff00;
    uint32_t res = (Byte)(A - val);
    F = (mBitsSignHalfOverflowZero_SUB[ah | res]);
}

void z80::Or8Bit(Byte & dst, Byte value)
{
    dst |= value;

    F = mBitsSignParityZero[dst];
}

void z80::And8Bit(Byte & dst, Byte value)
{
    dst &= value;

    F = mBitsSignParityZero[dst];
    F |= HF;

}

void z80::Xor8Bit(Byte & dst, Byte value)
{
    dst ^= value;
    mAF.lo = mBitsSignParityZero[dst];
}
