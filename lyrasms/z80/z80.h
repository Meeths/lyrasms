#pragma once
#include <memory>
#include <System/Types.h>
#include <common/BitOperations.h>

class IZ80Memory;
class IZ80IO;

class z80
{
public:

union Register
{
	Word reg;
	struct
	{
		Byte lo;
		Byte hi;
	};
};

enum class FlagRegisterBits
{
	CARRY = 0,
	SUBTRACT = 1,
	PARITY_OVERFLOW = 2,
	HALF_CARRY = 4,
	ZERO = 6,
	SIGN = 7
};

enum class FlagRegisterMasks
{
	CARRY_MASK = 1 << static_cast<char>(FlagRegisterBits::CARRY),
	SUBTRACT_MASK = 1 << static_cast<char>(FlagRegisterBits::SUBTRACT),
	PARITY_OVERFLOW_MASK = 1 << static_cast<char>(FlagRegisterBits::PARITY_OVERFLOW),
	HALF_CARRY_MASK = 1 << static_cast<char>(FlagRegisterBits::HALF_CARRY),
	ZERO_MASK = 1 << static_cast<char>(FlagRegisterBits::ZERO),
	SIGN_MASK = 1 << static_cast<char>(FlagRegisterBits::SIGN)
};

	z80(lyra::SharedPointer<IZ80Memory> memory, lyra::SharedPointer<IZ80IO> io) :
		mMemory(std::move(memory)), mIO(std::move(io))
	{
		CalculateTables();
	};

	void Reset();
	Byte FetchAndExecuteInstruction();


	Register mAF;
	Register mBC;
	Register mDE;
	Register mHL;

	Register mAFShadow;
	Register mBCShadow;
	Register mDEShadow;
	Register mHLShadow;

	Register mIX;
	Register mIY;

	Word mPC;			// Program counter
	Word mSP;			// Stack pointer

	Byte mR;			// Refresh
	Byte mI;			// Interrupt

	Word mIM;

	bool mInterruptFlipFlop1 = false;
	bool mInterruptFlipFlop2 = false;
	bool mEnableInterruptRequested = false;

	bool mHalted = false;

	Byte mInstructionCycles = 0;

	lyra::SharedPointer<IZ80Memory> mMemory;
	lyra::SharedPointer<IZ80IO> mIO;

	bool mEnableInstructionLogging = true;

	void RequestInterrupt(Word routineAddress);

private:
	bool FetchAndExecuteCBInstruction();
	bool FetchAndExecuteDDInstruction();
	bool FetchAndExecuteEDInstruction();
	bool FetchAndExecuteFDInstruction();
	bool FetchAndExecuteDDFDCBInstruction();

	bool ExecuteOpcode(Byte opcode);
	bool ExecuteCBOpcode(Byte opcode);
	bool ExecuteDDOpcode(Byte opcode);
	bool ExecuteEDOpcode(Byte opcode);
	bool ExecuteFDOpcode(Byte opcode);
	bool ExecuteDDFDBCOpcode(Byte opcode, SByte data);

	void WriteByte(Word address, Byte byte);
	void WriteWord(Word address, Word word);

	Byte ReadByte(Word address);
	SByte ReadSByte(Word address);
	Word ReadWord(Word address);
	Byte ReadNextByte();

	SByte ReadNextSByte();
	Word ReadNextWord();

	void WriteIOByte(Byte address, Byte byte);
	Byte ReadIOByte(Byte address);

	void Push(Word val);
	Word Pop();

	bool IsDDFDInstruction() const { return mDDInstruction || mFDInstruction; };
	Register& SelectHLRegister() { return mFDInstruction ? mIY : (mDDInstruction ? mIX : mHL); };

	void SetFlag(FlagRegisterBits flag) { mAF.lo = SetBit(mAF.lo, static_cast<Byte>(flag)); };
	void ResetFlag(FlagRegisterBits flag) { mAF.lo = ResetBit(mAF.lo, static_cast<Byte>(flag)); };
	void PutFlag(FlagRegisterBits flag, bool value) { if (value) SetFlag(flag); else ResetFlag(flag); }
	bool TestFlag(FlagRegisterBits flag) const { return TEST_BIT(mAF.lo, static_cast<Byte>(flag)); }

	bool mFDInstruction = false;
	bool mDDInstruction = false;

	void Inc8Bit(Byte& reg);
	void Dec8Bit(Byte& reg);
	void Inc16Bit(Word& reg);
	void Dec16Bit(Word& reg);

	void Add8Bit(Byte value);
	void Adca8Bit(Byte value);
	void Add16Bit(Word& dst, Word value);
    void AdcHL16Bit(Word value);
	void Sub8Bit(Byte value);
	void Sbc8Bit(Byte value);
	void SbcHL16Bit(Word value);

	void Cmp8Bit(Byte reg, Byte ref);

	void Or8Bit(Byte& dst, Byte value);
	void And8Bit(Byte& dst, Byte value);
	void Xor8Bit(Byte& dst, Byte value);

	void TestBit8Bit(Byte byte, Byte bit);
	void ResetBit8Bit(Byte& byte, Byte bit);
	void SetBit8Bit(Byte& byte, Byte bit);

	void Srl8Bit(Byte& byte);
	void Sra8Bit(Byte& byte);
	void Sll8Bit(Byte& byte);
	void Sla8Bit(Byte& byte);
	void Rr8Bit(Byte& byte);
	void Rra8Bit();
	void Rrc8Bit(Byte& byte);
	void Rrca8Bit();
	void Rl8Bit(Byte& byte);
	void Rla8Bit();
	void Rlc8Bit(Byte& byte);
	void Rlca8Bit();

	void Djnz();
    void Daa();
	Byte Cpi();
	void Cpir();
	Byte Cpd();
	void Cpdr();
	void Ldi();
	void Ldir();
	void Ldd();
	void Lddr();

	void Rrd();
	void Rld();

	Byte In(Byte address);
	void Ini();
	void Outi();
	void Otir();
	void Outd();


	void Ldai();
	void Ldar();

	void IncreaseR();
	void CPURestart(Byte newPC);

	void CalculateTables();


    Byte mBitsSignZero[256] = {};
	Byte mBitsSignParityZero[256] = {};
    Byte mBitsSignHalfOverflowZero_ADD[256 * 256 * 2] = {};
    Byte mBitsSignHalfOverflowZero_SUB[256 * 256 * 2] = {};
    Byte mBitsSignHalfOverflowZero_INC[256] = {};
	Byte mBitsSignHalfOverflowZero_DEC[256] = {};
	Byte mBitsSignParityZero_BIT[256] = {};
};