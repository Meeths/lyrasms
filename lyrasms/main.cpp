#include <Application/Windows/WindowsVulkanApplication.h>
#include <System/Types.h>


#include <sms/SMSSystem.h>
#include <System/Log.h>
#include <System/LoggerStdout.h>

#include <Renderer/Vulkan/VulkanBasicPrimitives.h>
#include <Renderer/Vulkan/VulkanConstants.h>
#include <Renderer/Vulkan/VulkanDefaultShaders.h>

#include <Filesystem/FileSystem.h>
#include <Time/FrameLimiter.h>

#include "Sound/ISoundSystem.h"


lyra::VulkanBuffer mFullScreenVertexBuffer;
lyra::VulkanBuffer mFullScreenIndexBuffer;


void Render(lyra::WindowsVulkanApplication& _application)
{
    ProfileScoped;

    const auto commandBuffer = _application.GetVulkanSystem()->GetVulkanCommandBufferManager().GetCommandBuffer(lyra::VulkanConstants::GraphicsCommandBufferName()).GetVkCommandBuffer();
    _application.GetVulkanSystem()->GetResolveRenderPass().BeginRenderPass(commandBuffer);

    VkViewport viewport;
    viewport.x = 0;
    viewport.y = 0;
    viewport.width = static_cast<float>(_application.GetVulkanSystem()->GetVulkanSwapchain().GetExtents().width);
    viewport.height = static_cast<float>(_application.GetVulkanSystem()->GetVulkanSwapchain().GetExtents().height);
    viewport.minDepth = 0;
    viewport.maxDepth = 1.0f;
    VkRect2D scissorRect;
    scissorRect.offset = { static_cast<int>(viewport.x), static_cast<int>(viewport.y) };
    scissorRect.extent = { static_cast<unsigned int>(viewport.width), static_cast<unsigned int>(viewport.height) };

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(commandBuffer, 0, 1, &scissorRect);
        
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _application.GetVulkanSystem()->GetPipelineManager().GetPipeline("Default", _application.GetVulkanSystem()->GetResolveRenderPass()));

    VkDescriptorSet descriptorSet = _application.GetVulkanSystem()->GetVulkanDescriptorManager().GetVkDescriptorSet("Default");
    unsigned int dynOffsets[] = {0, 0};
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, _application.GetVulkanSystem()->GetVulkanDescriptorManager().GetVkPipelineLayout(lyra::VulkanDescriptorManager::LayoutType::Ubo_Sampler2D_Ubo_SamplerCube),
        0, 1, &descriptorSet, 2, dynOffsets);

    auto vertexBuffer = mFullScreenVertexBuffer.GetVkBuffer();
    VkDeviceSize offsets = 0;
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertexBuffer, &offsets);
    vkCmdBindIndexBuffer(commandBuffer, mFullScreenIndexBuffer.GetVkBuffer(), 0, sizeof(lyra::VertexIndexType) == 2 ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32);

    vkCmdDrawIndexed(commandBuffer, static_cast<uint32_t>(lyra::VulkanBasicPrimitives::FullScreenQuadIndices.size()), 1, 0, 0, 0);

}

void ConfigureApplication(lyra::WindowsVulkanApplication& _application)
{
        _application.GetVulkanSystem()->GetVulkanDescriptorManager().CreateDescriptorSet(
            "Default", "Default", lyra::VulkanDescriptorManager::LayoutType::Ubo_Sampler2D_Ubo_SamplerCube);


        mFullScreenVertexBuffer = _application.GetVulkanSystem()->GetVulkanDevice().CreateVulkanBuffer(
            (void*)lyra::VulkanBasicPrimitives::FullScreenQuadVertices.data(),
            sizeof(lyra::VertexAttributes::VertexPosColUV) * lyra::VulkanBasicPrimitives::FullScreenQuadVertices.size(),
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);

        mFullScreenIndexBuffer = _application.GetVulkanSystem()->GetVulkanDevice().CreateVulkanBuffer(
            (void*)lyra::VulkanBasicPrimitives::FullScreenQuadIndices.data(),
            sizeof(lyra::VertexIndexType) * lyra::VulkanBasicPrimitives::FullScreenQuadIndices.size(),
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT);

        lyra::VulkanShaderModuleManager::ShaderModuleCreationInfo vsInfo {lyra::VulkanDefaultShaders::kDefaultVertexShader, lyra::ShaderConstants::ShaderType::VertexShader};
        lyra::VulkanShaderModuleManager::ShaderModuleCreationInfo fsInfo {lyra::VulkanDefaultShaders::kDefaultFragmentShader, lyra::ShaderConstants::ShaderType::FragmentShader};
        _application.GetVulkanSystem()->GetShaderModuleManager().CreateShaderModule(lyra::ShaderConstants::DefaultVertexShaderName(), vsInfo);
        _application.GetVulkanSystem()->GetShaderModuleManager().CreateShaderModule(lyra::ShaderConstants::DefaultFragmentShaderName(), fsInfo);


        lyra::VulkanPipelineTemplate defaultTemplate;
        defaultTemplate.vertexShader = lyra::ShaderConstants::DefaultVertexShaderName();
        defaultTemplate.fragmentShader = lyra::ShaderConstants::DefaultFragmentShaderName();
        defaultTemplate.colorBlend = true;
        defaultTemplate.attributeDescription = lyra::VertexAttributes::VertexPosColUVAttributes;
        defaultTemplate.vertexStride = sizeof(lyra::VertexAttributes::VertexPosColUV);
        defaultTemplate.cullMode = VK_CULL_MODE_NONE;
        defaultTemplate.frontFace = VK_FRONT_FACE_CLOCKWISE;
        defaultTemplate.polygonMode = VK_POLYGON_MODE_FILL;
        defaultTemplate.depthWrite = false;
        defaultTemplate.depthTest = false;
        defaultTemplate.pipelineLayout = _application.GetVulkanSystem()->GetVulkanDescriptorManager().GetVkPipelineLayout(lyra::VulkanDescriptorManager::LayoutType::Ubo_Sampler2D_Ubo_SamplerCube);

        _application.GetVulkanSystem()->GetPipelineManager().RegisterPipelineTemplate("Default", defaultTemplate);
        const lyra::VulkanPipelineManager::PipelineCreationInfo creationInfo
        {
            _application.GetVulkanSystem()->GetVulkanSwapchain(),
            _application.GetVulkanSystem()->GetResolveRenderPass(),
            _application.GetVulkanSystem()->GetVulkanDevice(),
            _application.GetVulkanSystem()->GetShaderModuleManager(),
            "Default" 
        };
        _application.GetVulkanSystem()->GetPipelineManager().CreatePipeline("Default", creationInfo);
}

int main(int, char[])
{
    lyra::Log::Instance().RegisterLogger(MakeSharedPointer<lyra::LoggerStdout>());
    
    lyra::WindowsVulkanApplication::CreationInfo creationInfo;
    creationInfo.mFullScreen = false;
    creationInfo.mMainWindowSize = {256, 192};
    creationInfo.mCreateSoundSystem = true;
    creationInfo.mMaxAudioChannels = 32;
    
    auto application = MakeSharedPointer<lyra::WindowsVulkanApplication>(creationInfo);
    lyra::SharedPointer<SMSSystem> mSMS = MakeSharedPointer<SMSSystem>();
    
    application->Initialize();
    mSMS->Initialize(application->GetVulkanSystem()->GetVulkanTextureManager(),*application->GetInputManager());

    auto sound = application->GetSoundSystem()->CreateSound(&mSMS->mIO->GetSound());
    sound->Play();
    
//    const auto fileContents = lyra::FileSystem::ReadFile("../data/roms/zexall.sms");
//    const auto fileContents = lyra::FileSystem::ReadFile("../data/roms/VDPTEST.sms");
//    const auto fileContents = lyra::FileSystem::ReadFile("../data/roms/PFR_Detect100.sms");
//    const auto fileContents = lyra::FileSystem::ReadFile("../data/roms/pause.sms");
    const auto fileContents = lyra::FileSystem::ReadFile("../data/roms/Sonic The Hedgehog (USA, Europe).sms");
        
    mSMS->LoadRom(fileContents);

    lyra::FrameLimiter fpsLimiter(60);
    
    application->GetApplicationLoop().AddExecutionUnit(lyra::ApplicationLoop::Phase::StartFrame,
                                                           lyra::ExecutionUnit([&fpsLimiter](float){ fpsLimiter.StartFrame();} ));
    application->GetApplicationLoop().AddExecutionUnit(lyra::ApplicationLoop::Phase::EndFrame,
                                                           lyra::ExecutionUnit([&fpsLimiter](float){ fpsLimiter.EndFrame();} ));
    
    application->GetApplicationLoop().AddExecutionUnit(lyra::ApplicationLoop::Phase::Render,
                                                       lyra::ExecutionUnit([application](float){ Render(*application);} ));

    auto sms = mSMS;
    application->GetApplicationLoop().AddExecutionUnit(lyra::ApplicationLoop::Phase::Update,
                                                   lyra::ExecutionUnit([sms](float _dt){ sms->UpdateSystem(_dt);} ));

    ConfigureApplication(*application);


    application->Run();

    application->GetSoundSystem()->DestroySound(sound);
    
    application->GetVulkanSystem()->GetVulkanDevice().WaitForDevice();
    mFullScreenIndexBuffer.Destroy();
    mFullScreenVertexBuffer.Destroy();

    application->Shutdown();
    return 0;
}
