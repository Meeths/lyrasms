#pragma once

#include <string>
#include <System/Types.h>

class SMSROM
{
public:
    enum class RomOffsets
    {
        TMR_SEGA = 0x7ff0,
        RESERVED_SPACE = 0x7ff8,
        CHECKSUM = 0x7ffa,
        PRODUCT_CODE = 0x7ffc,
        VERSION = 0x7ffe,
        REGION_CODE = 0x7fff,
        ROM_SIZE = 0x7fff
    };

    enum class Region : char
    {
        SMSJapan = 0x03,
        SMSExport = 0x04,
        GGJapan = 0x05,
        GGExport = 0x06,
        GGInternational = 0x07,
        Undefined = 0x0
    };

    enum class RomSize : char
    {
        Size8KB = 0x0a,
        Size16KB = 0x0b,
        Size32KB = 0x0c,
        Size48KB = 0x0d,
        Size64KB = 0x0e,
        Size128KB = 0x0f,
        Size256KB = 0x00,
        Size512KB = 0x01,
        Size1MB = 0x02,
        Undefined = 0x0
    };

    struct RomHeader
    {
        std::string mTMRSega;
        int mChecksum = 0;
        int mProductCode = 0;
        int mVersion = 0;
        Region mRegionCode = Region::Undefined;
        RomSize mROMSize = RomSize::Undefined;
    };

    void LoadRom(const lyra::Vector<char>& romdata);
    Byte Read(unsigned int  _address) const { return mMemory[_address];}
    const RomHeader& GetHeader() const { return mHeader; };
private:
    static RomHeader LoadHeader(char* romdata);

    static unsigned const int kROMSize = 0x100000;
    lyra::Array<char, kROMSize> mMemory = {};
    RomHeader mHeader = {};
};
