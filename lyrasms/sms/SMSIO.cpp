#include <sms/SMSIO.h>
#include <Input/InputManager.h>

namespace
{
    void SetBit(Byte& byte, Byte bit)
    {
        byte = byte | (1 << bit);
    }

    void ResetBit(Byte& byte, Byte bit)
    {
        byte =  byte & ~(1 << bit);
    }
    void PutBit(Byte& byte, Byte bit, bool value) { if (value) SetBit(byte, bit); else ResetBit(byte, bit); }

}

void SMSIO::Update(unsigned int cpuCycles)
{
    // Input port 1
    const auto& pad1 = mInputManager.GetGamepadState(0);
    const auto& keys = mInputManager.GetKeyboardState();

    const auto& pad2 = mInputManager.GetGamepadState(1);
#define KEY_SPACE 0x20
#define KEY_CONTROL 0x11
#define KEY_UP 0x26
#define KEY_DOWN 0x28
#define KEY_LEFT 0x25
#define KEY_RIGHT 0x27


    PutBit(mInputPort1, 0, !pad1.mUp.pressed && !keys[KEY_UP].pressed);
    PutBit(mInputPort1, 1, !pad1.mDown.pressed && !keys[KEY_DOWN].pressed);
    PutBit(mInputPort1, 2, !pad1.mLeft.pressed && !keys[KEY_LEFT].pressed);
    PutBit(mInputPort1, 3, !pad1.mRight.pressed && !keys[KEY_RIGHT].pressed);
    PutBit(mInputPort1, 4, !pad1.mA.pressed && !keys[KEY_SPACE].pressed);
    PutBit(mInputPort1, 5, !pad1.mB.pressed && !keys[KEY_CONTROL].pressed);
    PutBit(mInputPort1, 6, !pad2.mUp.pressed);
    PutBit(mInputPort1, 7, !pad2.mDown.pressed);

    PutBit(mInputPort2, 0, !pad2.mLeft.pressed);
    PutBit(mInputPort2, 1, !pad2.mRight.pressed);
    PutBit(mInputPort2, 2, !pad2.mA.pressed);
    PutBit(mInputPort2, 3, !pad2.mB.pressed);
    PutBit(mInputPort2, 4, true); // Reset not pressed
    PutBit(mInputPort2, 5, true); // unused
    PutBit(mInputPort2, 6, true);
    PutBit(mInputPort2, 7, true);

}

void SMSIO::WRITE(Byte address, Byte data)
{
    const DeviceAddresses deviceAddress = static_cast<DeviceAddresses>(address);
    switch (deviceAddress)
    {
    case DeviceAddresses::GRAPHICS_DATA_PORT:
        mVDP.WriteDataPort(data);
        break;

    case DeviceAddresses::GRAPHICS_VDP_ADDRESS_A:
    case DeviceAddresses::GRAPHICS_VDP_ADDRESS_B:
        mVDP.WriteControlPort(data);
        break;

    case DeviceAddresses::SOUND_WRITE_PORT_A:
    case DeviceAddresses::SOUND_WRITE_PORT_B:
		mSound.WriteData(data);
        break;
    case DeviceAddresses::FM_MODULE:
    case DeviceAddresses::FM_DATA:
    case DeviceAddresses::FM_REGISTER:
        break;
    default:
        //__debugbreak();
        break;
    }
}

Byte SMSIO::READ(Byte address)
{
    const DeviceAddresses deviceAddress = static_cast<DeviceAddresses>(address);
    switch (deviceAddress)
    {
    case DeviceAddresses::GRAPHICS_DATA_PORT:
        return mVDP.ReadDataPort();
        break;
    case DeviceAddresses::GRAPHICS_VDP_ADDRESS_A:
    case DeviceAddresses::GRAPHICS_VDP_ADDRESS_B:
        return mVDP.ReadControlPort();
        break;
    case DeviceAddresses::GRAPHICS_VDP_READ_HCOUNTER:
        return mVDP.GetHCounter();
        break;
    case DeviceAddresses::GRAPHICS_VDP_READ_VCOUNTER:
        return mVDP.GetVCounter();
        break;
    case DeviceAddresses::INPUT_PORT_1:
    case DeviceAddresses::INPUT_PORT_1_MIRROR:
        return mInputPort1;
        break;
    case DeviceAddresses::INPUT_PORT_2:
    case DeviceAddresses::INPUT_PORT_2_MIRROR:
        return mInputPort2;
        break;
    case DeviceAddresses::FM_MODULE:
    case DeviceAddresses::FM_DATA:
        return 0;
        
    case DeviceAddresses::INPUT_PORT_KEYBOARD:
        return 0;
        break;
    case DeviceAddresses::SOUND_CHIP:
        return 0;
        break;
    case DeviceAddresses::FM_REGISTER:
        return 0;
        break;
    default: ;
    }

    if (address >= 0x40 && address <= 0x7F)
    {
        if ((address % 2) == 0)
        {
            return mVDP.GetVCounter();
        }
        return mVDP.GetHCounter();
    }

    if (address >= 0x80 && address <= 0xBF)
    {
        if ((address % 2) == 0)
        {
            return mVDP.ReadDataPort();
        }
        return mVDP.ReadControlPort();
    }

    return 0xFF;
}
