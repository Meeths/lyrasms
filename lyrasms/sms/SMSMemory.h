#pragma once

#include <sms/SMSROM.h>
#include <z80/iz80memory.h>

class SMSMemory : public IZ80Memory
{
public:

    enum class MemoryMapRanges
    {
        ROM_NOT_PAGED = 0x0400,
        ROM_SLOT_1 = 0x0000,
        ROM_SLOT_2 = 0x4000,
        ROM_SLOT_3 = 0x8000,
        RAM = 0xC000,
        RAM_MIRRORED = 0xE000
    };

    enum class MemoryOffsets
    {
        MEMORY_CONTROL_REGISTER = 0xFFFC,
        MEMORY_CONTROL_PAGE_SLOT1 = 0xFFFD,
        MEMORY_CONTROL_PAGE_SLOT2 = 0xFFFE,
        MEMORY_CONTROL_PAGE_SLOT3 = 0xFFFF
    };

    enum class MemoryControlBits
    {
        RAM_BANKING = 3,
        SECOND_RAM_BANK = 2,
        RAM_BANKING_EXT = 4
    };

    enum class MemorySizes
    {
        MEMORY_ROM_PAGE_SIZE = 0x4000,
        MEMORY_RAM_MIRROR_SIZE = 0x2000,
        MEMORY_RAM_BANK_SIZE = 0x4000,
        MEMORY_RAM_BANKS = 2
    };

    void LoadRom(const lyra::Vector<char>& data);

    void WRITE(Word address, Byte data) override;
    Byte READ(Word address) override;
private:

    void OnMemoryControlWrite(Word address, Byte data);

    static unsigned const int kMemorySize = 0x10000;

    Byte mMemory[kMemorySize] = {};
    Byte mRAMBanks	[static_cast<Word>(MemorySizes::MEMORY_RAM_BANKS)]
                    [static_cast<Word>(MemorySizes::MEMORY_RAM_BANK_SIZE)] = {};

    SMSROM mROM;

    Byte mMemoryPageSlot1 = 0;
    Byte mMemoryPageSlot2 = 1;
    Byte mMemoryPageSlot3 = 2;
    SByte mMemoryRAMBank = -1;

    bool mBigCartridge = false;
};
