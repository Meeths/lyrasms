#pragma once

#include <common/IColorOutput.h>
#include <System/Types.h>

class VDP
{
public:
	static const int kHorizontalResolution = 256;
	static const int kVerticalResolution = 192;
	static const int kScanlinesCountNTSC = 262;
	static const int kScanlinesCountPAL = 313;
	static const int kVerticalResolutionMedium = 224;
	static const int kVerticalResolutionHigh = 240;
	static const int kUnwrittenPixelValue = 1;

	static const int kVRAMSize = 0x4000;
	static const int kCRAMSize = 0x20;
	static const int kVDPRegisterCount = 0xF;

	static const Word kAddressRegisterMask = 0x3FFF;


	VDP();

	void WriteControlPort(uint8_t data);
	void WriteDataPort(uint8_t data);
	uint8_t ReadControlPort();
	uint8_t ReadDataPort();

	void ProcessLine(Word _line);
	bool IsRequestingInterupt() const { return mFrameInterruptPendingFlag; }

	uint8_t GetHCounter() const;

	uint8_t GetVCounter() const
	{
		return mLineCounter;
	}

	unsigned long GetClock() const { return 228 * (m_IsPAL ? (kScanlinesCountNTSC * 60) : (kScanlinesCountPAL * 50)); }
	Word GetHeight() const { return mHeight; }
	Word GetWidth() const { return mWidth; }

    bool IsScreenEnabled() const;
	void SetColorOutput(lyra::SharedPointer<IColorOutput> colorOutput) { mColorOutput = std::move(colorOutput); }
	void ResetScreen();

private:
	Byte GetVDPMode() const;

	bool m_IsPAL = false;

	Byte mVRAM[kVRAMSize]{0};
	Byte mCRAM[kCRAMSize]{0};
	Byte mVDPRegisters[kVDPRegisterCount]{0};

	bool mCommandWordSecondWrite = false;

	Word mAddressRegister = 0;
	Word mCodeRegister = 0;

	Byte mReadBuffer = 0;
	Byte mLineCounter = 0;
    Word mHCounter = 0;

	Byte mVScroll = 0;

	bool mFrameInterruptPendingFlag = false;
	bool mLineInterruptPendingFlag = false;

	bool mSpriteOverflowFlag = false;
	bool mSpriteCollisionFlag = false;

    // Work areas before writing final values
	uint32_t spriteTempRow[kHorizontalResolution];          // For sprites
	uint32_t tileTempRow[8];                                // For backgounrd tiles

	uint32_t mRGBAColorLUT[0x40];
    
	uint8_t ConvertColor(uint8_t color); // convert from 2 bit to 8 bit color
	inline void RasterizeMode4Background(int line, int col);
	inline void RasterizeMode4Sprites(int line);
	Word GetBaseAddress();

	bool m_RequestInterupt = false;
	lyra::SharedPointer<IColorOutput> mColorOutput;

	Word mHeight;
	Word mWidth;
};
