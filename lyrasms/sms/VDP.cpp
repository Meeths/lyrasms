#include <sms/VDP.h>
#include <common/BitOperations.h>
#include <System/SystemDefines.h>

VDP::VDP() :
 mHeight(kVerticalResolution)
, mWidth(kHorizontalResolution)
{
    // fill out color conversion table
    for (int i=0; i<64; i++)
    {
        
        Byte blue = ConvertColor((i & 0x30) >> 4);
        Byte green = ConvertColor((i & 0xC) >> 2);
        Byte red = ConvertColor(i & 0x3);        
        mRGBAColorLUT[i] = (red) | (green << 8) | (blue << 16);
    }

    mLineCounter = 0xFF;
    
}

Byte VDP::ConvertColor(Byte color)
{
    // convert 2 bit to 8 bit color
    color = color & 0x3;
    return (color << 6) | (color << 4) | (color << 2) | (color);
}

void VDP::WriteControlPort(Byte data)
{
    /*
    MSB                         LSB
    CD1 CD0 A13 A12 A11 A10 A09 A08    Second byte written
    A07 A06 A05 A04 A03 A02 A01 A00    First byte written
    */
    
    if (mCommandWordSecondWrite)
    {
        mCodeRegister = data >> 6;
        mAddressRegister = ((data & 0x3F) << 8 )| (mAddressRegister & 0x00FF);

        switch(mCodeRegister)
        {
            case 0:
                mReadBuffer = mVRAM[mAddressRegister++];
                mAddressRegister &= kAddressRegisterMask; //Address register is 14 bits 
                break;
            case 2:
                {
                    const Byte selectedVDPRegister = data & 0xF; // First 4 bits select the register
                    const Byte VDPData = mAddressRegister & 0x00FF; // First written command word is VDP data
                    mVDPRegisters[selectedVDPRegister] = VDPData;
                }
                break;
        }
    }
    else
    {
        mAddressRegister = (mAddressRegister & 0x3F00) | data;
    }
    mCommandWordSecondWrite = !mCommandWordSecondWrite;
}

Byte VDP::ReadControlPort()
{
    mCommandWordSecondWrite = false;

    /*
    MSB                         LSB
    INT OVR COL --- --- --- --- ---
     */
    const Byte ret =
        (mFrameInterruptPendingFlag << 7) |
        (mSpriteOverflowFlag << 6) |
        (mSpriteCollisionFlag << 5);

    mLineInterruptPendingFlag = false;
    mFrameInterruptPendingFlag = false;
    mSpriteOverflowFlag = false;
    mSpriteCollisionFlag = false;
    
    m_RequestInterupt = false;

    return ret;
}

void VDP::WriteDataPort(Byte data)
{
    mCommandWordSecondWrite = false;

    switch (mCodeRegister)
    {
        case 0:
        case 1:
        case 2:
            mVRAM[mAddressRegister] = data;
            break;
        case 3:
            mCRAM[mAddressRegister & 0x1F] = data;
        break;
    }

    mAddressRegister++;
    mAddressRegister &= kAddressRegisterMask;

    /*
    http://www.techno-junk.org/txt/msvdp.txt
     
    An additional quirk is that writing to the
    data port will also load the buffer with the value written.
     */
    mReadBuffer = data;
}

Byte VDP::ReadDataPort()
{
    mCommandWordSecondWrite = false;

    const auto ret = mReadBuffer;

    mReadBuffer = mVRAM[mAddressRegister++];
    mAddressRegister &= kAddressRegisterMask;

    return ret;    
}

inline void VDP::RasterizeMode4Background(int line, int col)
{
    const Byte row = line / 8;

    const auto baseAddress = GetBaseAddress();

    const int bytesPerTile = 2;
    const int tilesPerRow = 32;

    
    const Word vramAddress = baseAddress
                                + ((row & 0x1f) * tilesPerRow * bytesPerTile)
                                + ((col & 0x1F) * bytesPerTile);

    const Byte highVRAMByte = mVRAM[vramAddress+1];
    const Byte lowVRAMByte = mVRAM[vramAddress];
    Word patternIndex = ((highVRAMByte & 1) << 8) | lowVRAMByte;
    patternIndex *= 32;
    
    bool priority = TEST_BIT(highVRAMByte, 4);
    const bool usePalette = TEST_BIT(highVRAMByte, 3);
    const bool vFlip = TEST_BIT(highVRAMByte, 2);
    const bool hFlip = TEST_BIT(highVRAMByte, 1);

    int offset = (line % 8) * 4;    
    if (vFlip)
    {
        offset = (8 - (line % 8)) * 4;
    }
    
    Byte b0 = mVRAM[(patternIndex) + offset];
    Byte b1 = mVRAM[(patternIndex) + offset + 1];
    Byte b2 = mVRAM[(patternIndex) + offset + 2];
    Byte b3 = mVRAM[(patternIndex) + offset + 3];

    for (int i=0; i<8; i++)
    {
        Byte raw_color = hFlip ?
            ((b0 & 1) << 0) | ((b1 & 1) << 1) | ((b2 & 1) << 2) | ((b3 & 1) << 3) :
            ((b0 & 128) >> 7) | ((b1 & 128) >> 6) | ((b2 & 128) >> 5) | ((b3 & 128) >> 4);

        if (usePalette)
        {
            raw_color = mCRAM[raw_color + 16];
        }
        else
        {
            raw_color = mCRAM[raw_color];
        }
        
        tileTempRow[i] = mRGBAColorLUT[raw_color];

        if(hFlip)
        {
            b0 >>= 1;
            b1 >>= 1;
            b2 >>= 1;
            b3 >>= 1;
        }
        else
        {
            b0 <<= 1;
            b1 <<= 1;
            b2 <<= 1;
            b3 <<= 1;
        }
    }
}

void VDP::RasterizeMode4Sprites(int line)
{
    std::fill(spriteTempRow, spriteTempRow + kHorizontalResolution, kUnwrittenPixelValue);
    /*
    Register $05 - Sprite Attribute Table Base Address
    D7 - No effect
    D6 - Bit 13 of the table base address
    D5 - Bit 12 of the table base address
    D4 - Bit 11 of the table base address
    D3 - Bit 10 of the table base address
    D2 - Bit  9 of the table base address
    D1 - Bit  8 of the table base address
    D0 - No effect
     */
    Word spriteAttributeTableBase = mVDPRegisters[5] & 0x7E;
    spriteAttributeTableBase <<= 7;


    const bool doubleSizeSprite = TEST_BIT(mVDPRegisters[1], 0);
    const Byte spriteHeight = TEST_BIT(mVDPRegisters[1], 1) || doubleSizeSprite ? 16 : 8;
    const bool useAlternativePattern = TEST_BIT(mVDPRegisters[6], 2);;
    const bool shiftX = TEST_BIT(mVDPRegisters[0], 3);
    const int maxSprites = 64;
    int spriteCountThisLine = 0;
    const int maxSpritesPerLine = 8;
    
    for (int currentSprite = 0; currentSprite < maxSprites; ++currentSprite)
    {
        const int spriteAddress = spriteAttributeTableBase + currentSprite;
        int yCoord = mVRAM[spriteAddress];

        if(yCoord == 0xD0 && mHeight == kVerticalResolution)
        {
            // Do not render any more sprites
            break;    
        }

        if (yCoord > 0xD0)
        {
            yCoord -= 0x100 ;
        }
        
        ++yCoord;

        if (line >= yCoord && line < (yCoord + spriteHeight))
        {
            spriteCountThisLine++;
            if(spriteCountThisLine > maxSpritesPerLine)
            {
                mSpriteOverflowFlag = true;
                break;
            }
            
            int xCoord = mVRAM[spriteAttributeTableBase + 128 + currentSprite * 2];
            int patternIndex = mVRAM[spriteAttributeTableBase + 128 + currentSprite * 2 + 1];

            if (shiftX)
                xCoord -= 8 ;

            if(useAlternativePattern)
                patternIndex += 256;

            if (spriteHeight == 16 && !doubleSizeSprite)
            {
                if (yCoord < (line + 9))
                    patternIndex &= ~1;
            }
            

            int startAddress = patternIndex * 32 ;
            startAddress += (4 * (line - yCoord)) ;

            Byte data1 = mVRAM[startAddress] ;
            Byte data2 = mVRAM[startAddress+1] ;
            Byte data3 = mVRAM[startAddress+2] ;
            Byte data4 = mVRAM[startAddress+3] ;

            for (int i = 0; i < 8; ++i)
            {
                if (xCoord + i >= kHorizontalResolution)
                {
                    break; ;
                }
                
                if(spriteTempRow[xCoord + i] != kUnwrittenPixelValue)
                {
                    mSpriteCollisionFlag = true;
                    continue;
                }
                
                int col = 7 - i;
                    
                Byte palette = 0 ;
                palette |= TEST_BIT(data4,col) ? 1 << 3: 0 ;
                palette |= TEST_BIT(data3,col) ? 1 << 2: 0 ;
                palette |= TEST_BIT(data2,col) ? 1 << 1: 0 ;
                palette |= TEST_BIT(data1, col) ? 1 : 0 ;

                // 0 is transparent
                if (palette == 0)
                    continue;
                

                Byte raw_color = mCRAM[palette + 16];
                spriteTempRow[xCoord + i] = mRGBAColorLUT[raw_color];
            }
        }  
    }
}

Word VDP::GetBaseAddress()
{
    if(mHeight == kVerticalResolution)
    {
        /*
        Register $02 - Name Table Base Address
        D7 - No effect
        D6 - No effect
        D5 - No effect
        D4 - No effect
        D3 - Bit 13 of the table address
        D2 - Bit 12 of the table address
        D1 - Bit 11 of the table address
        D0 - No effect
        */
        return ((mVDPRegisters[2] >> 1) & 0x7) << 11;
    }
    else
    {
        /*
        If the 224 or 240-line displays are being used, only bits 3 and 2 select
        the table address like so:

        D3 D2  Address
        -- --  -------
        0  0  $0700
        0  1  $1700
        1  0  $2700
        1  1  $3700
        */
        Word baseAddressTable[4] = { 0x700, 0x1700, 0x2700, 0x3700};
        const int baseAddressIndex =  TEST_BIT(mVDPRegisters[2], 2) ? 1 : 0 |
                                      TEST_BIT(mVDPRegisters[2], 3) ? 1 << 1 : 0;
        return baseAddressTable[baseAddressIndex];
    }
}

void VDP::ProcessLine(Word _line)
{
    // Frame Interrupts
    if (_line == (mHeight + 1) && TEST_BIT(mVDPRegisters[1], 5))
    {        
        mFrameInterruptPendingFlag = true;
        return;
    }

    // Line counter value is in $10 (full value)
    if (_line > (mHeight + 1))
    {
        mLineCounter = mVDPRegisters[10];
        return;
    }

    if (_line <= mHeight)
    {
        // Line is going to underscan, assert line interrupt
        if (mLineCounter == 0 && TEST_BIT(mVDPRegisters[0], 4))        
            mLineInterruptPendingFlag = true;
        
        mLineCounter--;
        if (mLineCounter == 0xFF)
            mLineCounter = mVDPRegisters[10];
    }

    // 4 first bits of $07 are the background color
    const auto overscanColor = mCRAM[(mVDPRegisters[7] & 0xF)];


    
    // backrgound X scroll is stored in $08
    Byte hScrollValue = mVDPRegisters[8];
    if (_line < 16 && TEST_BIT(mVDPRegisters[0], 6))
        hScrollValue = 0; // No horizontal scroll for lines 0-15
    
    Byte hScrollColumn = hScrollValue >> 3;
    Byte hScrollFine = hScrollValue & 0x7;
    
    Byte row = _line / 8;

    
    if (_line == 0)
    {
        // background Y scroll is stored in $09
        // VScroll can only change outside active display period (ie. once per frame)
        const Byte vScrollWrap = (mHeight == kVerticalResolution) ? 223 : 255;
        const Byte vScrollValue = mVDPRegisters[9] % vScrollWrap;
        const Byte vScrollRow = vScrollValue >> 3;       // Last 5 bits are the row
        const Byte vScrollFine = vScrollValue & 0x7;     // First 3 bits are the fine scrolling
        mVScroll = vScrollFine + 8 * vScrollRow;

        // Height can change after active display period
        Byte mode = GetVDPMode();
        if (mode == 11)
        {
            mHeight = kVerticalResolutionMedium;
            __debugbreak();
        }
        else if (mode == 14)
        {
            mHeight = kVerticalResolutionHigh;
            __debugbreak();
        }
        else
            mHeight = kVerticalResolution; 
    }

    RasterizeMode4Sprites(_line);

    // Raster tiles
    for (Byte col = 0; col < 32; col++)
    {
        auto row = _line + mVScroll;
        // Disable V-Scroll for last 8 cols if requested 
        if(TEST_BIT(mVDPRegisters[0], 7) && col >= 24)
            row = _line;

        switch (GetVDPMode())
        {
            case 2:
                lyraAssert(0);
                break;
            default:
                break;
        }

        RasterizeMode4Background((row) % (mHeight + 32), (col - hScrollColumn) % 32);
        for (int i = 0; i < 8; i++)
        {
            if (i + col*8 + hScrollFine < mWidth)
            {
                const auto xCoord = ((i + col*8 + hScrollFine) % mWidth);
                mColorOutput->WriteColor(reinterpret_cast<Byte*>(&tileTempRow[i]),xCoord, _line);
            }
        }
    }

    for (Word i = 0; i < kHorizontalResolution; ++i)
    {
        if(spriteTempRow[i] != kUnwrittenPixelValue)
            mColorOutput->WriteColor(reinterpret_cast<Byte*>(&spriteTempRow[i]),i, _line);            
    }
    
    // Mask first column with overscan color if requested (or sprite pattern?)
    if (TEST_BIT(mVDPRegisters[0], 5))
    {
        for (int i = 0; i < 8; i++)
        {
            mColorOutput->WriteColor(reinterpret_cast<Byte*>(&mRGBAColorLUT[overscanColor]),i, _line);
        }
    }

}

uint8_t VDP::GetHCounter() const
{
    lyraAssert(0);
    return (mHCounter & 0x1FF >> 1) & 0xFFFF;
}

bool VDP::IsScreenEnabled() const
{
    return TEST_BIT(mVDPRegisters[0x1], 6);
}

void VDP::ResetScreen()
{
    for (int i = 0; i < mHeight; ++i)
    {
        for (int j = 0; j < mWidth; ++j)
        {
            static const Byte color[] = { kUnwrittenPixelValue, kUnwrittenPixelValue, kUnwrittenPixelValue };
            mColorOutput->WriteColor(color, j, i);
        }

    }

}

Byte VDP::GetVDPMode() const
{
    Byte res = 0;
    res |= TEST_BIT(mVDPRegisters[0x1], 4) ? 1 : 0;
    res |= TEST_BIT(mVDPRegisters[0x0], 1) ? 1 << 1 : 0;
    res |= TEST_BIT(mVDPRegisters[0x1], 3) ? 1 << 2 : 0;
    res |= TEST_BIT(mVDPRegisters[0x0], 2) ? 1 << 3 : 0;
    return res;
}
