#include <sms/SMSROM.h>
#include <sms/SMSMemory.h>

#include "common/BitOperations.h"

namespace 
{
    const Byte kPageMask = 0b11111;
    const Byte kPageMask1MB = 0b111111;
}

void SMSMemory::LoadRom(const lyra::Vector<char>& data)
{
    mROM.LoadRom(data);

    if (mROM.GetHeader().mROMSize == SMSROM::RomSize::Size1MB)
        mBigCartridge = true;

    memset(mMemory, 0, kMemorySize);

    mMemory[static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_PAGE_SLOT1)] = 0x0;
    mMemory[static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_PAGE_SLOT2)] = 0x1;
    mMemory[static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_PAGE_SLOT3)] = 0x2;

    mMemoryPageSlot1 = 0;
    mMemoryPageSlot2 = 1;
    mMemoryPageSlot3 = 2;
    mMemoryRAMBank = -1;

    for (Byte i = 0; i < static_cast<Byte>(MemorySizes::MEMORY_RAM_BANKS); ++i)
    {
        memset(mRAMBanks[i], 0xF0, static_cast<Word>(MemorySizes::MEMORY_RAM_BANK_SIZE));
    }


}

void SMSMemory::WRITE(Word address, Byte data)
{
    if (address < static_cast<Word>(MemoryMapRanges::ROM_SLOT_3))
    {
        // Trying to write in ROM
        return;
    }

    if (address < static_cast<Word>(MemoryMapRanges::RAM))
    {
        // Might be RAM or ROM depending on memory control register
        if (mMemoryRAMBank > -1)
        {
            mRAMBanks[mMemoryRAMBank][address - static_cast<Word>(MemoryMapRanges::ROM_SLOT_3)] = data;
            return;
        }
        
        // Trying to write in ROM		
        return;
    }
    // Write
    mMemory[address] = data;

    // Writing in control registers
    if (address >= static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_REGISTER))
    {
        // Paging code goes here etc
        OnMemoryControlWrite(address, data);
    }

    // Mirror ranges
    
    if (address >= static_cast<Word>(MemoryMapRanges::RAM) && address < static_cast<Word>(MemoryMapRanges::RAM_MIRRORED) &&
        (address + static_cast<Word>(MemorySizes::MEMORY_RAM_MIRROR_SIZE)) < static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_REGISTER))
    {
        mMemory[address + static_cast<Word>(MemorySizes::MEMORY_RAM_MIRROR_SIZE)] = data;
    }


    if (address >= static_cast<Word>(MemoryMapRanges::RAM_MIRRORED))
        mMemory[address - static_cast<Word>(MemorySizes::MEMORY_RAM_MIRROR_SIZE)] = data;
}

Byte SMSMemory::READ(Word address)
{
    // Reading the registers from program is not allowed
    // use mirrored block
    if (address >= static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_REGISTER))
    {
        address -= static_cast<Word>(MemorySizes::MEMORY_RAM_MIRROR_SIZE);
    }

    // Read fixed ROM space
    if (address < static_cast<Word>(MemoryMapRanges::ROM_NOT_PAGED))
    {
        return mROM.Read(address);
    }

    if (address < static_cast<Word>(MemoryMapRanges::ROM_SLOT_1) + static_cast<Word>(MemorySizes::MEMORY_ROM_PAGE_SIZE))
    {
        const unsigned int bankAddress = address + (static_cast<Word>(MemorySizes::MEMORY_ROM_PAGE_SIZE) * mMemoryPageSlot1);
        return mROM.Read(bankAddress);
    }

    if (address < static_cast<Word>(MemoryMapRanges::ROM_SLOT_2) + static_cast<Word>(MemorySizes::MEMORY_ROM_PAGE_SIZE))
    {
        const unsigned int bankAddress = address + (static_cast<int>(MemorySizes::MEMORY_ROM_PAGE_SIZE) * (mMemoryPageSlot2 - 1));
        return mROM.Read(bankAddress);
    }

    if (address < static_cast<Word>(MemoryMapRanges::ROM_SLOT_3) + static_cast<Word>(MemorySizes::MEMORY_ROM_PAGE_SIZE))
    {
        // RAM bank in use
        if (mMemoryRAMBank >= 0)
        {
            return mRAMBanks[mMemoryRAMBank][address - static_cast<Word>(MemoryMapRanges::ROM_SLOT_3)];
        }

        // Map to ROM slot 3 otherwise
        const unsigned int bankAddress = address + (static_cast<int>(MemorySizes::MEMORY_ROM_PAGE_SIZE) * (mMemoryPageSlot3 - 2));
        return mROM.Read(bankAddress);
    }


    return mMemory[address];
}

void SMSMemory::OnMemoryControlWrite(Word address, Byte data)
{
    if (address < static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_REGISTER))
        return;

    mMemory[address - static_cast<Word>(MemorySizes::MEMORY_RAM_MIRROR_SIZE)] = data;

    const Byte pageValue = mBigCartridge ? data & kPageMask1MB : data & kPageMask;

    if (address == static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_REGISTER))
    {
        // RAM paging
        if (TEST_BIT(data, static_cast<Word>(MemoryControlBits::RAM_BANKING)))
        {
            mMemoryRAMBank = TEST_BIT(data, static_cast<Word>(MemoryControlBits::SECOND_RAM_BANK)) ? 1 : 0;
        }
        else
        {
            mMemoryRAMBank = -1;
        }
    }

    if (address == static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_PAGE_SLOT1))
    {
        mMemoryPageSlot1 = pageValue;
    }
    if (address == static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_PAGE_SLOT2))
    {
        mMemoryPageSlot2 = pageValue;
    }
    if (address == static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_PAGE_SLOT3))
    {
        // ROM slot 3 only available if RAM bank is not set
        if(!(mMemory[static_cast<Word>(MemoryOffsets::MEMORY_CONTROL_REGISTER)] & (1 << static_cast<Word>(MemoryControlBits::RAM_BANKING))))
        {
            mMemoryPageSlot3 = pageValue;
        }
    }

}
