#include <sms/SMSROM.h>
#include <stdexcept>

namespace
{
    struct TwoHalfBytes
    {
        char lowBits : 4, highBits : 4;
    };

    unsigned int DecodeBCD(unsigned int numBytes, const char * data)
    {
        unsigned int result = 0;

        for (unsigned int i = 0; i<numBytes; i++)
        {
            result = (result * 100) + ((data[i] >> 4) * 10) + (data[i] & 0x0F);
        }

        return result;
    }

    lyra::Vector<char> ReadRomBytes(char* romdata, int offset, int numBytes, bool littleEndian = false)
    {
        lyra::Vector<char> readBuffer(&romdata[offset], &romdata[offset + numBytes]);
        if(littleEndian)
            std::reverse(readBuffer.begin(), readBuffer.end());
        return readBuffer;
    }
}
//////////////////////////////////////////////////////////////////////////
SMSROM::RomHeader SMSROM::LoadHeader(char* romdata)
{
    RomHeader header;
    auto tmrBuffer = ReadRomBytes(romdata, static_cast<size_t>(RomOffsets::TMR_SEGA), 8);
    header.mTMRSega = std::string(tmrBuffer.data(), 8);
    header.mChecksum = static_cast<int>(*reinterpret_cast<uint16_t*>(ReadRomBytes(romdata, static_cast<size_t>(RomOffsets::CHECKSUM), 2, true).data()));

    header.mProductCode = DecodeBCD(2, ReadRomBytes(romdata, static_cast<size_t>(RomOffsets::PRODUCT_CODE), 2, true).data());
    const TwoHalfBytes productCodeLastDigitAndVersion = *reinterpret_cast<TwoHalfBytes*>(ReadRomBytes(romdata, static_cast<size_t>(RomOffsets::VERSION), 1).data());
    header.mProductCode += productCodeLastDigitAndVersion.highBits * 10000;
    header.mVersion = productCodeLastDigitAndVersion.lowBits;
    const TwoHalfBytes regionAndRomSize = *reinterpret_cast<TwoHalfBytes*>(ReadRomBytes(romdata, static_cast<size_t>(RomOffsets::REGION_CODE), 1).data());
    header.mRegionCode = static_cast<Region>(regionAndRomSize.highBits);
    header.mROMSize = static_cast<RomSize>(regionAndRomSize.lowBits);

    return header;
}
//////////////////////////////////////////////////////////////////////////
void SMSROM::LoadRom(const lyra::Vector<char>& romdata)
{
    const int kExtraHeaderSize = 512;
    const bool hasExtraHeader = romdata.size() % 0x4000 == kExtraHeaderSize;

    if (romdata.size() > kROMSize)
        throw std::runtime_error("The ROM size is too big for this system");

    std::memset(mMemory.data(), 0, kROMSize);
    if(!hasExtraHeader)
    {
        std::memcpy(mMemory.data(), romdata.data(), romdata.size());		
    }
    else
    {
        std::memcpy(mMemory.data(), romdata.data() + kExtraHeaderSize, romdata.size() - kExtraHeaderSize);
    }

    mHeader = LoadHeader(mMemory.data());

}
