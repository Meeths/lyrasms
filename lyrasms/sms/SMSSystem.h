#pragma once

#include <sms/SMSMemory.h>
#include <sms/SMSIO.h>
#include <z80/z80.h>
#include <common/ColorOutputVulkanTexture.h>
#include <Renderer/Vulkan/VulkanTextureManager.h>

namespace lyra 
{
    class InputManager;
    class ISound;
}

class ILogger;

class SMSSystem
{
public:
    void Initialize(lyra::VulkanTextureManager& textureManager, lyra::InputManager& inputManager);
    void Shutdown();
    void LoadRom(const lyra::Vector<char>& data);
    void UpdateSystem(float _deltaTime);

    bool mPaused = false;
    bool mHalted = false;

    lyra::SharedPointer<SMSMemory> mMemory;
    lyra::SharedPointer<SMSIO> mIO;
    lyra::SharedPointer<z80> mZ80;
    lyra::SharedPointer<ColorOutputVulkanTexture> mTVOut;

    const lyra::Vector<unsigned int>& GetLastFrameInstructions() const { return mLastFrameInstructions; }
private:
    void ManageInterrupts();

    lyra::Vector<unsigned int> mLastFrameInstructions;
	lyra::ISound* mSound = nullptr;
};
