#pragma once

#include <Sound/IStreamProvider.h>
#include <System/Types.h>

class SN79489 : public lyra::IStreamProvider
{
public:
    SN79489(unsigned long _clock);
    void WriteData(Byte _data);

private:
    const int kFrequency = 44100;
    const int kBitsPerSample = 16;
    const int kChannels = 1;
    const int kSamples = 1024;
    const int kNoiseShiftRegisterDefault = 0x4000;

    void UpdateStream(char* _data, unsigned int _size) override;
    void GetStreamProperties(unsigned int& frequency, unsigned int& bits, unsigned int& samples, unsigned int& channels) override
    {
        frequency = kFrequency;
        bits = kBitsPerSample;
        samples = kSamples;
        channels = kChannels;
    }

    int StepMono();

    typedef struct Channel
    {
        SWord mToneFrequencyValue = 0;
        SByte mToneFrequencyPosition = 0;
        long mIntermediatePosition = 0;
        Word mVolume = 0;
    } Channel;

    lyra::Array<Channel, 4> mChannels = {};
    lyra::Array<Word, 8>  mRegisters = {};
    int mLatchedRegister;
    Word mNoiseShiftRegister = kNoiseShiftRegisterDefault;
    SWord mNoiseFreq;
    float mTickAccumulator;
    float mFixedTick;
};
