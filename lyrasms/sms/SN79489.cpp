#include <sms/SN79489.h>
#include <common/BitOperations.h>
#include <limits.h>
#include <Math/Functions.h>

#include <System/SystemDefines.h>

namespace
{
    const lyra::Array<Word, 16> mVolumeValues =
    {
        892, 892, 892, 760, 623, 497, 404, 323,
        257, 198, 159, 123, 96, 75, 60, 0
    };
}

int SN79489::StepMono()
{
    int monoChannel = 0;

    SWord channelValues[4];
    auto& noiseChannel = mChannels[3];
    
    for (int i = 0; i <= 2; i++)
    {
        const auto& channel = mChannels[i];
        auto& channelValue = channelValues[i];
        
        if (channel.mIntermediatePosition != LONG_MIN)
            channelValue = (channel.mVolume * channel.mIntermediatePosition) >> 16;
        else
            channelValue = channel.mVolume * channel.mToneFrequencyPosition;
    }
    channelValues[3] = static_cast<SWord>(2 * noiseChannel.mVolume * (mNoiseShiftRegister & 0x1));

    monoChannel = channelValues[0] + channelValues[1] + channelValues[2] + channelValues[3];

    // Simulate next clock tick
    mTickAccumulator += mFixedTick;

    const auto tickInteger = static_cast<int>(mTickAccumulator);
    mTickAccumulator -= tickInteger;    // Accumulate fractional part

    // Decrement "remaining lifetime"
    for (int i = 0; i < 4; i++)
    {
        // For all channels except noise if it needs to match channel 2 
        if(i != 3 || mNoiseFreq != 0x80)
        {
            mChannels[i].mToneFrequencyValue -= tickInteger;
        }
        else
        {
            // Noise tone freq needs to go with channel 2 if noisefreq == 0x80
            mChannels[i].mToneFrequencyValue = mChannels[2].mToneFrequencyValue;
        }
    }
  
    // Tone ChannelsValues:
    for (int i = 0; i < 3; i++)
    {
        auto& channel = mChannels[i];
        
        if (channel.mToneFrequencyValue <= 0)
        {
            // Tone "lifetime" ended
            const int toneFrequency = mRegisters[i * 2];

            if (toneFrequency > 0x6)
            {
                // From MEKA emulator, accurate tone with fractional tick part
                // This makes it sound super good, less "artificial" than in the typical SN documentation

                channel.mIntermediatePosition = static_cast<long>(
                    (tickInteger - mTickAccumulator + 2 * channel.mToneFrequencyValue)
                    * channel.mToneFrequencyPosition / (tickInteger + mTickAccumulator)
                    * 0x10000);

                // Square signal
                channel.mToneFrequencyPosition = -channel.mToneFrequencyPosition;
            }
            else
            {
                // Reset channel
                channel.mToneFrequencyPosition = 1;
                channel.mIntermediatePosition = LONG_MIN;
            };
            
            if (toneFrequency != 0)
                channel.mToneFrequencyValue += toneFrequency * (tickInteger / toneFrequency + 1);
        }
        else
        {
            channel.mIntermediatePosition = LONG_MIN;
        }
    };

    
    // Noise channel
    if (noiseChannel.mToneFrequencyValue < 0) // Noise "lifetime" ended
    {
        //Square signal
        noiseChannel.mToneFrequencyPosition = -noiseChannel.mToneFrequencyPosition;
        
        if (mNoiseFreq != 0x80)
            noiseChannel.mToneFrequencyValue += mNoiseFreq * (tickInteger / mNoiseFreq + 1);
        
        if (noiseChannel.mToneFrequencyPosition == 1)
        {
            int noise = 0;
            if (mRegisters[6] & 0x04)
            {
                // White Noise
                // This is like a "seed" for the white noise. Docs state that the SMS is 0x9
                const int tappedBits = 0x9;
                // Fast parity check
                noise = ((mNoiseShiftRegister & tappedBits) && ((mNoiseShiftRegister & tappedBits) ^ tappedBits));
            }
            else
            {
                // "Periodic noise"  
                noise = mNoiseShiftRegister & 1;
            }
            mNoiseShiftRegister = (mNoiseShiftRegister >> 1) | (noise << 15);
        }
    };

    return monoChannel;
};

SN79489::SN79489(unsigned long _clock)
{
    // Updates are run by the sound stream buffer fill function
    mTickAccumulator = 0.0f;
    mFixedTick = static_cast<float>(_clock) / (kBitsPerSample * kFrequency);

    mLatchedRegister = 0;

    for (auto& channel : mChannels)
    {
        channel.mVolume = 0;
        channel.mToneFrequencyValue = 0;
        channel.mToneFrequencyPosition = 1;
        channel.mIntermediatePosition = LONG_MIN;
    }

    int registerIndex = 0;
    for (auto& sndRegister : mRegisters)
    {
        if (registerIndex++ % 2 == 0)
        {
            // Odd registers are frequency
            sndRegister = 1;
        }
        else
        {
            // Even registers are volume (0xF is mute)
            sndRegister = 0xF;
        }
    }

    mNoiseFreq = 0x10;
    mNoiseShiftRegister = kNoiseShiftRegisterDefault;
}

void SN79489::WriteData(Byte _data)
{

    if (_data & 0x80) 
    {
        // Bit 7 means LATCH/DATA byte
        /*
        %1cctdddd
        |||````-- Data
        ||`------ Type
        ``------- Channel
         */
        mLatchedRegister = ((_data >> 4) & 0x07);
        mRegisters[mLatchedRegister] = (mRegisters[mLatchedRegister] & 0x3F0) | (_data & 0x0F);
        if (mLatchedRegister & 1)
            mChannels[mLatchedRegister >> 1].mVolume = mVolumeValues[_data & 0x0F];
    }
    else
    {
        /*
        %0-DDDDDD
        |``````-- Data
        `-------- Unused
         */

        if(mLatchedRegister %2 == 0)
        {
            // Odd registers are frequency (except 3rd pair, for the noise channel)
            if(mLatchedRegister != 6)
            {
                mRegisters[mLatchedRegister] = (mRegisters[mLatchedRegister] & 0x00F) | ((_data & 0x3F) << 4);
            }
            else
            {
                mRegisters[mLatchedRegister] = _data & 0x0F;
            }
        }
        else
        {
            // Even registers for volume
            mRegisters[mLatchedRegister] = _data & 0x0F;
            mChannels[mLatchedRegister >> 1].mVolume = mVolumeValues[_data & 0x0F];
        }
    }

    if(mLatchedRegister == 6)
    {
        // Reset noise signal generator
        mNoiseShiftRegister = kNoiseShiftRegisterDefault;
        mNoiseFreq = 0x10 << (mRegisters[6] & 0x3);
    }
}

void SN79489::UpdateStream(char* _data, unsigned int _size)
{
    const unsigned int nbelements = _size / sizeof(short);
    auto buf = reinterpret_cast<short*>(_data);

    for (unsigned int element = 0; element < nbelements; ++element)
    {
        auto monoChannelValue = StepMono();
        *buf++ = lyra::Math::Max(lyra::Math::Min(monoChannelValue, 0x7fff), -0x8000);
    }
}
