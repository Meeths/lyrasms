#pragma once

#include <z80/iz80io.h>
#include <sms/VDP.h>
#include <sms/SN79489.h>

namespace lyra
{
    class InputManager;
}

class SMSIO : public IZ80IO
{
public:

    enum class DeviceAddresses
    {
        SOUND_CHIP = 0x40,
        GRAPHICS_DATA_PORT = 0xBE,
        GRAPHICS_VDP_ADDRESS_A = 0xBF,
        GRAPHICS_VDP_ADDRESS_B = 0xBD,
        GRAPHICS_VDP_READ_VCOUNTER = 0x7E,
        GRAPHICS_VDP_READ_HCOUNTER = 0x7F,
        SOUND_WRITE_PORT_A = 0x7E,
        SOUND_WRITE_PORT_B = 0x7F,
        INPUT_PORT_1 = 0xDC,
        INPUT_PORT_2 = 0xDD,
        INPUT_PORT_1_MIRROR = 0xC0,
        INPUT_PORT_2_MIRROR = 0xC1,
        INPUT_PORT_KEYBOARD = 0xDE,
        FM_MODULE = 0xF2,
        FM_REGISTER = 0xF0,
        FM_DATA = 0xF1
    };

    SMSIO(lyra::InputManager& inputManager) : mSound(mVDP.GetClock()), mInputManager(inputManager) {};

    void Update(unsigned int cpuCycles);

    void WRITE(Byte address, Byte data) override;
    Byte READ(Byte address) override;

    SN79489& GetSound () { return mSound; }
    VDP& GetVDP () { return mVDP; }
private:
    VDP mVDP;
    SN79489 mSound;
    
    lyra::InputManager& mInputManager;

    Byte mInputPort1 = 0xFF;
    Byte mInputPort2 = 0xFF;
};
