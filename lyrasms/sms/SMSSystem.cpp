#include <sms/SMSSystem.h>
#include <System/Log.h>

#include <Profiler/Profiler.h>

namespace
{
    const Word kVDPInterruptRoutine = 0x38;
    const Word kZ80NMIRoutine = 0x66;
}

void SMSSystem::Initialize(lyra::VulkanTextureManager& textureManager, lyra::InputManager& inputManager)
{
    mMemory = std::make_shared<SMSMemory>();

    mIO = std::make_shared<SMSIO>(inputManager);
    mZ80 = std::make_shared<z80>(mMemory, mIO);
    mZ80->Reset();
    mTVOut = std::make_shared<ColorOutputVulkanTexture>(textureManager);
    mTVOut->SetResolution(mIO->GetVDP().GetWidth(), mIO->GetVDP().GetHeight());
    mIO->GetVDP().SetColorOutput(mTVOut);
    mIO->GetVDP().ResetScreen();

    mHalted = false;
    mPaused = false;
}

void SMSSystem::Shutdown()
{
}

void SMSSystem::LoadRom(const lyra::Vector<char>& data)
{
    mMemory->LoadRom(data);
    lyraLogInfo("System: ROM loaded");
}

void SMSSystem::UpdateSystem(float /*_deltaTime*/)
{
    ProfileScoped;

    if(mZ80 && !mPaused && !mHalted)
    {
        mLastFrameInstructions.clear();

        unsigned int curScanlineCycles = 0;

        // 228 cycles per scanline

        const unsigned int currentScanlines = 262;//NTSC
        const unsigned int cyclesPerScanline = 228;

        for(unsigned int scanline = 0; scanline < currentScanlines; scanline++)
        {
            
            while(curScanlineCycles < cyclesPerScanline)
            {
                const auto lastInstructionCycles = mZ80->FetchAndExecuteInstruction();

                if (lastInstructionCycles == 0)
                {
                    mHalted = true;
                    break;
                }
                curScanlineCycles += lastInstructionCycles;
            }
            mIO->Update(curScanlineCycles);
            curScanlineCycles -= cyclesPerScanline;
            mIO->GetVDP().ProcessLine(scanline);
        }

        ManageInterrupts();
        
        if (mIO->GetVDP().IsScreenEnabled())
        {
            mTVOut->Flip();
        }
    }
}

void SMSSystem::ManageInterrupts()
{
    if (mZ80)
    {
        if (mIO->GetVDP().IsRequestingInterupt())
        {
            mZ80->RequestInterrupt(kVDPInterruptRoutine);
        }
    }
}
