#pragma once
#include <common/ColorOutputVulkanTexture.h>
#include <Math/Functions.h>

#include <Profiler/Profiler.h>

const std::string ColorOutputVulkanTexture::kTextureName = "TVOUT";

ColorOutputVulkanTexture::ColorOutputVulkanTexture(lyra::VulkanTextureManager& textureManager) : mWidth(0), mHeight(0),
	mTextureWidth(0),
	mTextureHeight(0),
	mTextureManager(textureManager)
{
}

void ColorOutputVulkanTexture::SetResolution(Word x, Word y)
{
	mWidth = x;
	mHeight = y;

	mTextureWidth = lyra::Math::NextPowerOf2(mWidth);
	mTextureHeight = lyra::Math::NextPowerOf2(mHeight);

	mBufferVector.resize(mTextureWidth * mTextureHeight * 4);
    mBuffer = mBufferVector.data();
	for (auto i = 0; i < mTextureHeight; ++i)
	{
		for (auto j = 0; j < mTextureWidth; ++j)
		{
			mBuffer[i * mTextureWidth * 4 + j * 4 + 0] = 0;
			mBuffer[i * mTextureWidth * 4 + j * 4 + 1] = 0;
			mBuffer[i * mTextureWidth * 4 + j * 4 + 2] = 255;
			mBuffer[i * mTextureWidth * 4 + j * 4 + 3] = 255;
		}
	}

	lyra::VulkanTexture::CreationInfo textureCreationInfo
	{
        {mWidth, mHeight},
        1,
        1,
        lyra::TextureType::Texture,
        VK_FORMAT_R8G8B8A8_UNORM,
        VK_IMAGE_TILING_OPTIMAL
    };
	mTextureManager.CreateTexture("Black", textureCreationInfo);
	lyra::VulkanTextureManager::BlitInfo blitInfo
	{
		textureCreationInfo.mDimensions,
	    textureCreationInfo.mFormat,
	    (char*)mBuffer
	};
	
	mTextureManager.BlitToTexture("Black", blitInfo);
	/*
	auto texture = mTextureManager->GetTexture(kTextureName);
	if (!texture)
		mTextureManager->CreateTexture(kTextureName, mTextureWidth, mTextureHeight, 4, lyra::ImageFormat::R8G8B8A8, (char*)mBuffer, VK_FILTER_NEAREST, VK_FILTER_NEAREST);
	else
		Flip();
		*/
}

void ColorOutputVulkanTexture::Flip()
{
	ProfileScoped;

// TODO refresh texture	mTextureManager->UpdateTextureData(kTextureName, mTextureWidth, mTextureHeight, 4, lyra::ImageFormat::R8G8B8A8, (char*)mBuffer);
	lyra::VulkanTextureManager::BlitInfo blitInfo
	{
			{mWidth, mHeight},
	    VK_FORMAT_R8G8B8A8_UNORM,
	    reinterpret_cast<char*>(mBuffer)
	};
	
	mTextureManager.BlitToTexture("Black", blitInfo);
}
