#pragma once
#include <System/Types.h>

#define TEST_BIT(a,b) TestMask(a, 1 << b)

#define BIT_GET_VAL(a, b) (TEST_BIT(a, b) ? 1 : 0)


inline bool TestMask(Byte byte, Byte bitMask)
{
    return byte & bitMask;
}

inline bool TestMask(Word word, Word bitMask)
{
    return word & bitMask;
}


inline Byte SetBit(Byte byte, Byte bit)
{
    return byte | (1 << bit);
}

inline Word SetBit(Word word, Byte bit)
{
    return word | (1 << bit);
}

inline Byte ResetBit(Byte byte, Byte bit)
{
    return byte & ~(1 << bit);
}

inline Word ResetBit(Word word, Byte bit)
{
    return word & ~(1 << bit);
}

inline Byte CountHiBits(Byte byte)
{
    Byte bitCount = 0;
    for (auto i = 0; i < 8; ++i)
    {
        if (TEST_BIT(byte, i)) bitCount++;
    }
    return bitCount;
}

inline Byte CountHiBits(Word word)
{
    Byte bitCount = 0;
    for (auto i = 0; i < 16; ++i)
    {
        if (TEST_BIT(word, i)) bitCount++;
    }
    return bitCount;
}


inline Byte GetBitValue(Word word, Byte bit)
{
	return TEST_BIT(word, bit) ? 1 : 0;
}

inline Byte GetBitValue(Byte byte, Byte bit)
{
    return TEST_BIT(byte, bit) ? 1 : 0;
}