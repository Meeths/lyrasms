#pragma once
#include <System/Types.h>

class IColorOutput
{
public:
	virtual ~IColorOutput() = default;
	virtual void SetResolution(Word X, Word y) = 0;
	virtual void WriteColor(const Byte* rgb, Word x, Word y) = 0;
	virtual void ReadColor(Word x, Word y, Byte* rgbOut) = 0;
};
