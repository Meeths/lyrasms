#pragma once
#include <common/IColorOutput.h>
#include <Renderer/Vulkan/VulkanTextureManager.h>
#include <System/Types.h>

class ColorOutputVulkanTexture : public IColorOutput
{
public:
	ColorOutputVulkanTexture(lyra::VulkanTextureManager& textureManager);
	virtual void SetResolution(Word x, Word y) override;
	inline virtual void WriteColor(const Byte* rgb, Word x, Word y) override;
	inline virtual void ReadColor(Word x, Word y, Byte* rgbOut) override;

	Word GetWidth() const { return mWidth; }
	Word GetHeight() const { return mHeight; }

	float GetUVHeight() const { return (mHeight - 1) / (float)(mTextureHeight); }
	float GetUVWidth() const { return mWidth / (float)mTextureWidth; }

	void Flip();

	static const std::string kTextureName;
private:
	Word mWidth;
	Word mHeight;

	Word mTextureWidth;
	Word mTextureHeight;

	lyra::VulkanTextureManager& mTextureManager;
	lyra::Vector<unsigned char> mBufferVector;
    unsigned char* mBuffer = nullptr;
};


void ColorOutputVulkanTexture::WriteColor(const Byte * rgb, Word x, Word y)
{
	Byte* dst = &mBuffer[y * mTextureWidth * 4 + x * 4];
    *(dst + 0) = rgb[0];
    *(dst + 1) = rgb[1];
    *(dst + 2) = rgb[2];
}

void ColorOutputVulkanTexture::ReadColor(Word x, Word y, Byte* rgbOut)
{
	Byte* src = &mBuffer[y * mTextureWidth * 4 + x * 4];
	rgbOut[0] = *(src + 0);
    rgbOut[1] = *(src + 1);
    rgbOut[2] = *(src + 2);
}
